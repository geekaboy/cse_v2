<?php
//------------[Model File name : Web_gallery_model.php ]--------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Work_time_range_model extends CI_Model
{
    private $table = "cse_v2.work_time_range tr";
    public function __construct()
    {
        parent::__construct();
    }

    public function get_list()
    {
        $sql = "SELECT * FROM {$this->table} ORDER BY id";
        return $this->db->query($sql)->result();
    }


}
