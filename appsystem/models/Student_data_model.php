<?php
//------------[Model File name : Web_gallery_model.php ]--------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Student_data_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    private $table = 'cse_v2.student_data';

    public function get_detail($std_id = '', $citizen_id = '')
    {
        if(is_null($std_id) || is_null($citizen_id)){return NULL;exit();}

        $sql = "SELECT *, CONCAT(std.prefix, std.fullname) AS fullname
                FROM {$this->table} std
                WHERE std.std_id = {$this->db->escape($std_id)}
                    AND std.citizen_id = {$this->db->escape($citizen_id)}";
        return $this->db->query($sql)->row();
    }

    public function get_list_all()
    {
        $sql = "SELECT row_number() OVER () AS num_rows, std_id,
                    CONCAT(prefix_name, std_fname, ' ', std_lname) AS std_name,
                    maj_name, fac_name, date_graduated, std_email
                FROM cse_v2.work_students ws
                WHERE ws.std_email LIKE '%_@__%.__%'";
        return $this->db->query($sql)->result_array();
    }

    public function get_faculty_list()
    {
        $sql = "SELECT s.fac_name
                FROM {$this->table} s
                GROUP BY s.fac_name
                ORDER BY s.fac_name";
        return $this->db->query($sql)->result();

    }

    public function get_major_by_fac($fac = NULL)
    {
        if($fac == NULL) return NULL;

        $sql = "SELECT maj_name, fac_name
                FROM {$this->table}
                WHERE fac_name LIKE '%{$fac}%'
                GROUP BY maj_name, fac_name
                ORDER BY maj_name";
        return $this->db->query($sql)->result();
    }

}//End class
