<?php
//------------[Model File name : Web_gallery_model.php ]--------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Answer_general_model extends CI_Model
{
    private $table = 'cse_v2.answer_general';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_comment_note($year_graduation = NULL, $faculy = NULL, $maj_name = NULL)
    {
        $where = '';
        if($year_graduation != NULL){
            $where .= " AND s.year_graduated = {$this->db->escape($year_graduation)}";
        }
        if($faculy != NULL){
            $where .= " AND s.fac_name LIKE '%{$faculy}%'";
        }
        if($maj_name != NULL){
            $where .= " AND s.maj_name LIKE '%{$maj_name}%'";
        }

        $sql = "SELECT ag.comment_note, s.fac_name, s.maj_name
                FROM cse_v2.answer_general ag
                LEFT JOIN cse_v2.student_data s
                	ON ag.std_id = s.std_id AND ag.citizen_id = s.citizen_id
                WHERE ag.comment_note <> '' AND ag.comment_note <> '-'
                    AND ag.comment_note <> 'ไม่มี' {$where}
                GROUP BY ag.comment_note, s.fac_name, s.maj_name
                ORDER BY s.maj_name, s.fac_name";
        return $this->db->query($sql)->result();
    }

}//END CLASS
