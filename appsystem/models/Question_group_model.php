<?php
//------------[Model File name : Web_gallery_model.php ]--------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Question_group_model extends CI_Model
{
    private $table = 'cse_v2.question_group';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_detail($id=NULL)
    {
        if($id == NULL) return NULL;
        $sql = "SELECT * FROM {$this->table}";
        return $this->db->query($sql)->row();

    }

    public function get_list()
    {
        $sql = "SELECT * FROM {$this->table}
                WHERE id <> 6
                ORDER BY id";
        return $this->db->query($sql)->result();
    }

}//END CLASS
