<?php
//------------[Model File name : Web_gallery_model.php ]--------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Survey_report_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_organiz($year_graduated = '', $fac_name = NULL, $maj_name = NULL)
    {
        $where = '';
        $where .= ($fac_name != NULL)?" AND std.fac_name LIKE '%{$fac_name}%'":'';
        $where .= ($maj_name != NULL)?" AND std.maj_name LIKE '%{$maj_name}%'":'';
        $where .= ($year_graduated != '')?" AND std.year_graduated LIKE '%{$year_graduated}%'":'';

        $sql = "SELECT r.id, r.title AS org_title,
                	(
                		SELECT COUNT(DISTINCT a.citizen_id) AS count_org

                		FROM cse_v2.answer_general a

                		LEFT JOIN cse_v2.student_workplace w
                			ON a.citizen_id = w.citizen_id AND a.std_id = w.std_id

                		LEFT JOIN cse_v2.student_data std
                			ON a.citizen_id = std.citizen_id AND a.std_id = std.std_id

                		WHERE a.org_type_id = r.id
                			{$where}

                		GROUP BY a.org_type_id
                	)
                FROM cse_v2.org_type r

                ORDER BY r.id DESC";
        return $this->db->query($sql)->result();
    }

    public function get_director_type($year_graduated = '', $fac_name = NULL, $maj_name = NULL)
    {
        $where = '';
        $where .= ($fac_name != NULL)?" AND std.fac_name LIKE '%{$fac_name}%'":'';
        $where .= ($maj_name != NULL)?" AND std.maj_name LIKE '%{$maj_name}%'":'';
        $where .= ($year_graduated != '')?" AND std.year_graduated = '{$year_graduated}'":'';

        $sql = "SELECT d.id, d.title AS director_title,
                	(
                		SELECT COUNT(DISTINCT a.citizen_id) AS count_director

                		FROM cse_v2.answer_general a

                		LEFT JOIN cse_v2.student_workplace w
                			ON a.citizen_id = w.citizen_id AND a.std_id = w.std_id

                		LEFT JOIN cse_v2.student_data std
                			ON a.citizen_id = std.citizen_id AND a.std_id = std.std_id

                		WHERE a.director_type_id = d.id
                			{$where}
                	)
                FROM cse_v2.director_type d

                ORDER BY d.id DESC";
        return $this->db->query($sql)->result();
    }


}//END CLASS
