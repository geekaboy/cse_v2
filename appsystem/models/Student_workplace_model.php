<?php
//------------[Model File name : Web_gallery_model.php ]--------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Student_workplace_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_detail($id=0){

        $sql = "SELECT * FROM cse_v2.student_workplace w
                WHERE w.id = {$this->db->escape($id)}";
        return $this->db->query($sql)->row();
    }


}//End class
