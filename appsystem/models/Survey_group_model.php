<?php
//------------[Model File name : Web_gallery_model.php ]--------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Survey_group_model extends CI_Model
{
    private $table = "cse_v2.survey_group";
    public function __construct()
    {
        parent::__construct();
    }

    public function get_list()
    {
        $sql = "SELECT * FROM {$this->table} ORDER BY year_graduated DESC";
        return $this->db->query($sql)->result();
    }

    public function get_detail($year_graduated = '')
    {
        // echo $year_graduated;
        if($year_graduated == ''){
            return NULL;
            exit();
        };

        $sql = "SELECT * FROM {$this->table}
                WHERE year_graduated = {$this->db->escape($year_graduated)}";
        return $this->db->query($sql)->row();


    }

}//END CLASS
