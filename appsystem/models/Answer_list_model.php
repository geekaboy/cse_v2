<?php
//------------[Model File name : Web_gallery_model.php ]--------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Answer_list_model extends CI_Model
{
    private $table = 'cse_v2.answer_list';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_sum_score($question_id = NULL, $year_graduation = NULL, $faculy = NULL, $maj_name = NULL)
    {
        if($question_id == NULL) return NULL;

        $where = '';
        if($year_graduation != NULL){
            $where .= " AND s.year_graduated = {$this->db->escape($year_graduation)}";
        }
        if($faculy != NULL){
            $where .= " AND s.fac_name LIKE '%{$faculy}%'";
        }
        if($maj_name != NULL){
            $where .= " AND s.maj_name LIKE '%{$maj_name}%'";
        }

        $sql = "SELECT SUM( al.answer_value ) AS sum_score,
                	COUNT ( al.question_group_id ) AS count_survey
                FROM cse_v2.answer_list al
                LEFT JOIN cse_v2.student_data s
                	ON al.std_id = s.std_id AND al.citizen_id = s.citizen_id
                WHERE al.question_group_id = {$question_id} {$where}";
        return $this->db->query($sql)->row();
    }

    public function get_qlist_score($question_id = NULL, $year_graduation = NULL, $faculy = NULL, $maj_name = NULL)
    {
        if($question_id == NULL) return NULL;

        $where = '';
        if($year_graduation != NULL){
            $where .= " AND s.year_graduated = {$this->db->escape($year_graduation)}";
        }
        if($faculy != NULL){
            $where .= " AND s.fac_name LIKE '%{$faculy}%'";
        }
        if($maj_name != NULL){
            $where .= " AND s.maj_name LIKE '%{$maj_name}%'";
        }
        $sql = "SELECT ql.order_num, al.question_list_id, ql.title, SUM( al.answer_value ) AS sum_score,
                	COUNT ( al.question_group_id ) AS count_survey
                FROM cse_v2.answer_list al
                LEFT JOIN cse_v2.question_list ql
                	ON ql.id = al.question_list_id
                LEFT JOIN cse_v2.student_data s
                	ON al.std_id = s.std_id AND al.citizen_id = s.citizen_id
                WHERE al.question_group_id = {$question_id} {$where}
                GROUP BY al.question_list_id, ql.title, ql.order_num
                ORDER BY ql.order_num";
        return $this->db->query($sql)->result();
    }


}//END CLASS
