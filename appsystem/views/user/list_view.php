
<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Laos lotto</li>
    <li class="breadcrumb-item">ผู้ใช้งาน</li>
    <li class="breadcrumb-item active">รายการผู้ใช้งาน</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">

      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="card">
            <div class="card-body">

              <div class="row">
                <table class="table table-bordered table-responsive-sm table-hover">
                  <thead>
                    <tr class="bg-info">
                      <th width="30">#</th>
                      <th>ชื่อ-สกุล</th>
                      <th>Username</th>
                      <th width="180">จัดการ</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i_row = 0;
                    foreach ($user_list as $user):
                      $i_row++;
                    ?>
                    <tr>
                      <td><?php echo $i_row; ?></td>
                      <td><?php echo $user->fullname ?></td>
                      <td><?php echo $user->username ?></td>
                      
                      <td>
                        <div class="btn-group">
                          <a href="<?php echo site_url('user/edit_view/'.$user->username); ?>" class="btn btn-warning">
                            <i class="fa fa-pencil"></i> แก้ไข
                          </a>
                          <button type="button" class="btn btn-danger btn-del" data-username="<?php echo $user->username ?>">
                            <i class="fa fa-trash-o"></i> ลบ
                          </button>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; ?>

                  </tbody>
                </table>
              </div>

            </div><!--end card body -->
          </div><!-- end card -->
        </div>

      </div><!-- end row -->
    </div>
  </div>
</main>
