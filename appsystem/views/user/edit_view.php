<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Backend</li>
    <li class="breadcrumb-item">ผู้ใช้งาน</li>
    <li class="breadcrumb-item active">แก้ไขผู้ใช้งาน</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="card">
            <div class="card-body">
              <form id="frm_user">
                <h5>แก้ไขบัญชีผู้ใช้งาน</h5>
                <div class="row">
                  <div class='form-group col-md-6'>
                    <label class='control-label' for='fullname'>ชื่อ-นามสกุล </label>
                    <input type="text" class="form-control" id='fullname' name='fullname' value="<?php echo $user->fullname ?>">
                  </div>
                  <div class='form-group col-md-6'>
                    <label class='control-label' for='username'>USERNAME</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                      </div>
                      <input type="text" class="form-control"  value="<?php echo $user->username ?>" id='username' name='username' readonly>
                    </div>
                  </div>
                  <div class='form-group col-md-6'>
                    <label class='control-label' for='password'>PASSWORD</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-key"></i></span>
                      </div>
                      <input type="password" class="form-control" placeholder="Password"  id='password' name='password'>
                    </div>
                  </div>
                  <div class='form-group col-md-6'>
                    <label class='control-label' for='confirm_password'>CONFIRM PASSWORD</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-key"></i></span>
                      </div>
                      <input type="password" class="form-control" placeholder="Confirm Password"  id='confirm_password' name='confirm_password'>
                    </div>
                  </div>
                </div>
                <div class='form-group text-center mt-5'>
                  <div class="btn-group" role="group">
                    <button type="button" class="btn btn-warning btn-backward" data-url="user/list_view">
                      <i class="fa fa-arrow-left"></i> กลับ
                    </button>
                    <button type="button"  class='btn btn-success'  id='btn_save'>
                      <span class='fa fa-save'></span> บันทึก
                    </button>
                  </div>

                </div>
              </form>
            </div><!--end card body -->
          </div><!-- end card -->
        </div>
      </div><!-- end row -->
    </div>


  </div>
</main>
