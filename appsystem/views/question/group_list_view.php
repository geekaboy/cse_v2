<?php $sess=$this->session->userdata(); ?>

<div class="table-responsive">
    <table class="table table-bordered table-hover table-condensed">
        <thead>
            <tr class="bg-info">
                <th width="10">#</th>
                <th class="text-center">รายการ</th>
                <?php if (isset($sess['is_login'])): ?>
                <th width="40" class="text-center">จัดการ</th>
                <?php endif; ?>

            </tr>
        </thead>
        <tbody>
            <?php
            $i_row = 0;
            foreach ($rsdata as $row) {
                $id = $row['id'];
                $i_row++;
                ?>
                <tr>
                    <td><?php echo $i_row; ?>.</td>
                    <td><?php echo $row['title']; ?></td>
                    <?php if (isset($sess['is_login'])): ?>
                    <td class="text-center">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-warning" onclick="edit_group('<?php echo $id ?>')">
                                <i class="fa fa-edit"></i> EDIT
                            </button>
                            <button type="button" class="btn btn-danger" disabled onclick="group_delete(<?php echo $id ?>)">
                                <i class="fa fa-trash-o"></i>
                                DELETE
                            </button>
                        </div>
                    </td>

                    <?php endif; ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
