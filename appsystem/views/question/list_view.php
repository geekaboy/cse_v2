<?php $sess=$this->session->userdata(); ?>

<div class="table-responsive">
  <table class="table table-bordered table-hover table-condensed">
    <thead>
      <tr class="bg-info">
        <th width="10">#</th>
        <th class="text-center">รายการ</th>
        <?php if (isset($sess['is_login'])): ?>
            <th width="40" class="text-center">จัดการ</th>
        <?php endif; ?>
      </tr>
    </thead>
    <tbody>
      <?php
      $i_grow = 0;
      $i_row = 0;
      $g_id = 0;
      foreach ($rsdata as $row) {


        $id = $row['id'];

        if($g_id != $row['question_group_id']){
          $i_row=0;
          $i_grow ++;
          $i_row ++;
          ?>
          <tr>
            <td><?php echo $i_grow; ?>.</td>
            <td><strong><?php echo $row['group_title']; ?></strong></td>
            <?php if (isset($sess['is_login'])): ?>
            <td>
            </td>
            <?php endif; ?>

          </tr>
          <tr>
            <td></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $i_row.') '.$row['title']; ?></td>
            <?php if (isset($sess['is_login'])): ?>
                <td class="text-center">
                  <div class="btn-group" role="group">
                    <a class="btn btn-warning" href="<?php echo site_url('question/edit_view?id='.$id);?>">
                      <i class="fa fa-edit"></i> EDIT
                    </a>
                    <button type="button" disabled class="btn btn-danger">
                      <i class="fa fa-trash-o"></i> DELETE
                    </button>
                  </div>
                </td>
            <?php endif; ?>

          </tr>
      <?php
          $g_id = $row['question_group_id'];
        }else{
          $i_row ++;
      ?>
      <tr>
        <td></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $i_row.') '.$row['title']; ?></td>
        <?php if (isset($sess['is_login'])): ?>
            <td class="text-center">
              <div class="btn-group" role="group">
                <a class="btn btn-warning" href="<?php echo site_url('question/edit_view?id='.$id);?>">
                  <i class="fa fa-edit"></i> EDIT
                </a>
                <button type="button" disabled class="btn btn-danger">
                  <i class="fa fa-trash-o"></i> DELETE
                </button>
              </div>
            </td>
        <?php endif; ?>

      </tr>

      <?php
        }
      }
      ?>
    </tbody>
  </table>
</div>
