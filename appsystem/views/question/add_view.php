<?php $sess=$this->session->userdata(); ?>

<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">CRRU CSE</li>
        <li class="breadcrumb-item active">เพิ่มคำถาม</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-body">
                        <?php if (isset($sess['is_login'])): ?>
                            <h4 class="card-title">
                                <i class="fa fa-question-circle-o"></i> เพิ่มคำถาม
                            </h4>
                            <!-- <h6 class="card-subtitle"> All bootstrap element classies </h6> -->
                            <div class="row justify-content-center">
                                <form class="form-horizontal m-t-20 col-md-8">
                                    <div class="form-group">
                                        <label> <i class="fa fa-dot-circle-o"></i> คุณลักษณะของบัณฑิต</label>
                                        <select class="form-control" id="question_group" name="question_group">
                                            <option value="0">เลือก</option>
                                            <?php
                                            $i_row = 0;
                                            foreach ($rsdata as $row) {
                                                $i_row++;
                                            ?>
                                                <option value="<?php echo $row['id']; ?>">
                                                    <?php echo $i_row . '. ' . $row['title']; ?>
                                                </option>
                                            <?php
                                            }
                                            ?>
                                        </select>

                                        <!-- <input type="text" class="form-control" name="group_name" id="group_name" placeholder="Ex. ด้านคุณธรรม จริยธรรม"> -->
                                    </div>
                                    <div class="form-group">
                                        <label><i class="fa fa-question-circle-o"></i> คำถาม </label>
                                        <textarea class="form-control" name="question_text" id="question_text" rows="8" cols="80" placeholder="โปรดกรอก คำถาม..."></textarea>

                                    </div>
                                    <div class="form-group text-center">
                                        <button type="button" class="btn btn-primary mt-3" id="btn_save">
                                            <i class="fa fa-save"></i> บันทึก
                                        </button>
                                    </div>
                                </form>

                            </div>
                            <hr>
                        <?php endif; ?>

                        <div id="div_list">
                            <!-- load list to div  -->

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
