<?php $sess=$this->session->userdata(); ?>
<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">CRRU CSE</li>
        <li class="breadcrumb-item active">Home</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-body">
                        <?php if (isset($sess['is_login'])): ?>
                        <h4 class="card-title">
                            <span class="ti-layout-media-overlay-alt-2"></span> เพิ่มคุณลักษณะของบัณฑิตแต่ละด้าน
                        </h4>
                        <!-- <h6 class="card-subtitle"> All bootstrap element classies </h6> -->
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <form class="form-horizontal mt-5">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="question_group_name" id="question_group_name" placeholder="Ex. ด้านคุณธรรม จริยธรรม">
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="button" class="btn btn-lg btn-primary mt-3" id="btn_save">
                                            <i class="fa fa-save"></i> บันทึก
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr>
                        <?php endif; ?>

                        <div id="div_group">
                            <!-- load list to div  -->

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php if (isset($sess['is_login'])): ?>
    <div class="modal fade" id="edit_group_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width:100%">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                        <i class="fa fa-edit"></i> แก้ไข
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" id="edit_group_form">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <form class="form-horizontal mt-5">
                                <div class="form-group">
                                    <label for="edit_title">คุณลักษณะของบัณฑิต</label>
                                    <input type="text" class="form-control form-control-lg" name="edit_title" id="edit_title" placeholder="Ex. ด้านคุณธรรม จริยธรรม">
                                    <input type="hidden"name="edit_id" id="edit_id">
                                </div>
                                <div class="form-group text-center">
                                    <button type="button" class="btn btn-lg btn-warning mt-3" id="btn_edit">
                                        <i class="fa fa-save"></i> แก้ไข
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div><!--modal body-->
            </div><!--modal-content-->
        </div><!--modal-dialog --->
    </div><!--modal-->

<?php endif; ?>
