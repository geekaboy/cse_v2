<div class="row justify-content-center mt-5">
    <?php echo $this->pagination->create_links(); ?>

</div>

<i class="fa fa-search"></i> <b>ค้นหา: </b> <?php echo ($search_text != '')?$search_text:'-'; ?>
<b> จบปีการศึกษา: </b> <?php echo $year_graduated; ?>
<b> จำนวนทั้งหมด: </b> <?php echo $total_row; ?> <b>รายการ</b>

<table class="table table-hover table-bordered table-condensed">
    <thead>
        <tr class="bg-info">
            <th width="20">#</th>
            <th width="20">
                <input type="checkbox" name="check_all" id="check_all" />
            </th>

            <th>สถานประกอบการ</th>
            <th>E-Mail</th>
            <th>โปรแกรมวิชา - คณะ/สำนัก</th>
            <th>สำรวจ(ครั้ง)</th>
            <th width="90">ตอบกลับ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i_row = $start;
        if (count($employer_list) == 0) {
            echo '<tr><td colspan="6" class="text-center"><h4 class="text-info">*** ไม่พบข้อมูล ***</h4></td></tr>';
        }
        foreach ($employer_list as $key => $employer) {
            $i_row++;
            ?>
            <tr>
                <td><?php echo $i_row; ?>. </td>
                <td>
                    <input type="checkbox" name="employer[]" value="<?php echo $employer->id; ?>"
                        data-std-id="<?php echo $employer->std_id; ?>"
                        data-citizen-id="<?php echo $employer->citizen_id; ?>"
                        data-email="<?php echo $employer->director_email; ?>"/>
                </td>
                <td><?php echo $employer->organiz_name; ?></td>
                <td><?php echo $employer->director_email; ?></td>
                <td><?php echo $employer->maj_name.' - '.$employer->fac_name; ?></td>
                <td class="text-center"><?php echo $employer->count_survey; ?></td>
                <td class="text-center"><?php echo $employer->is_answer; ?></td>
            </tr>
        <?php
        }
        ?>

    </tbody>
</table>
<div class="row justify-content-center">
    <div class="col-md-3">
        <div class="form-group">
          <label for="">เลือก E-Mail</label>
          <select class="form-control" name="cse_email" id="cse_email">
              <?php foreach ($email_list as $key => $email): ?>
                  <option value="<?php echo $email->id; ?>"><?php echo $email->email; ?></option>
              <?php endforeach; ?>
          </select>
        </div>
    </div>
    <div class="col-md-12 text-center mt-3">
        <button class="btn btn-lg btn-primary" type="button" onclick="send_survey(this)">
           <i class="fa fa-send"></i> ส่งสำรวจ
        </button>
    </div>

</div>

<script>
    $(document).ready(function() {

        //iCheck
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_minimal-green',
            radioClass: 'iradio_minimal-green',
            increaseArea: '20%' // optional
        });

        $('#check_all').on('ifChecked', function(event) {
            $('input[name="employer[]"]').iCheck('check');
        })

        $('#check_all').on('ifUnchecked', function(event) {
            $('input[name="employer[]"]').iCheck('uncheck');

        })
        //End iCheck

        $('.page-link > a').click(function(e) {
            e.preventDefault();
            var url = site_url + "send_survey/get_employer_list?" +
                'page=' + $(this).data('ci-pagination-page') +
                '&search_text=' + $('#search_text').val() +
                '&is_answer='+$('#is_answer').val()+
                '&year_graduated=' + $('#year_graduated').val();
            $('#div_table').load(url, function(response, status, request) {
                $('body').scrollTop(0);
                hide_preload();

            });

        });
    }); //end ready
</script>
