<style>
tbody td {
    font-size: 14px;
}
</style>
<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">CRRU CSE</li>
        <li class="breadcrumb-item active">สถานะการทำงานของบัณฑิต</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-body">
                        <h4 class="card-title text-center">
                            <i class="fa fa-envelope"></i> ส่งสถานะการทำงานของบัณฑิต
                        </h4>
                        <!-- <h6 class="card-subtitle"> All bootstrap element classies </h6> -->
                        <div class="row justify-content-center mt-5">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input id="search_text" name="search_text" class="form-control" type="text"
                                        placeholder="ค้นหา รหัสนักศึกษา, โปรแกรมวิชา, คณะ/สำนัก">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select id="year_graduated" class="form-control" name="year_graduated">
                                    <?php foreach ($group_list as $key => $group): ?>
                                        <option value="<?php echo $group->year_graduated ?>">
                                            จบปีการศึกษา <?php echo $group->year_graduated ?>
                                        </option>
                                    <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <select id="is_answer" class="form-control" name="is_answer">
                                        <option value="">ทั้งหมด</option>
                                        <option value="0">ยังไม่ตอบกลับ</option>
                                        <option value="1">ตอบกลับ</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="button" id="btn_search">
                                        <i class="fa fa-search"></i> ค้นหา
                                    </button>
                                    <button class="btn btn-warning" type="button" id="btn_clear">
                                        <i class="fa fa-eraser"></i> เคลียร์
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" id="div_table">

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
