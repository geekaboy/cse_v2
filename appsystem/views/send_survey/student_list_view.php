<div class="row justify-content-center mt-5">
    <?php echo $this->pagination->create_links(); ?>

</div>

<i class="fa fa-search"></i> <b>ค้นหา: </b> <?php echo ($search_text != '')?$search_text:'-'; ?>
<b> จบปีการศึกษา: </b> <?php echo $year_graduated; ?>
<b> จำนวนทั้งหมด: </b> <?php echo $total_row; ?> <b>รายการ</b>

<table class="table table-hover table-bordered table-condensed">
    <thead>
        <tr class="bg-info">
            <th width="20">#</th>
            <th width="20">
                <input type="checkbox" name="check_all" id="check_all" />
            </th>
            <th>รหัสนักศึกษา</th>
            <th>ชื่อ-สกุล</th>
            <th>E-Mail</th>
            <th>โปรแกรมวิชา</th>
            <th>คณะ/สำนัก</th>
            <th class="text-center">การตอบกลับ</th>
            <th>สำรวจ(ครั้ง)</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i_row = $start;
        if (count($student_list) == 0) {
            echo '<tr><td colspan="10" class="text-center"><h4 class="text-info">*** ไม่พบข้อมูล ***</h4></td></tr>';
        }
        foreach ($student_list as $key => $student) {
            $i_row++;
            ?>
            <tr>
                <td><?php echo $i_row; ?>. </td>
                <td>
                    <input type="checkbox" name="student[]" value="<?php echo $student->id; ?>"
                        data-std-id="<?php echo $student->std_id; ?>"
                        data-citizen-id="<?php echo $student->citizen_id; ?>"
                        data-email="<?php echo $student->email; ?>"/>
                </td>
                <td><?php echo $student->std_id; ?></td>
                <td><?php echo $student->prefix.$student->fullname; ?></td>
                <td><?php echo $student->email; ?></td>
                <td><?php echo $student->maj_name; ?></td>
                <td><?php echo $student->fac_name; ?></td>
                <td class="text-center">
                    <?php
                    echo ($student->is_answer == '1')?'<i class="fa fa-dot-circle-o text-success" aria-hidden="true"></i>':
                    '<i class="fa fa-dot-circle-o text-danger" aria-hidden="true"></i>';
                    ?>
                </td>
                <td class="text-center"><?php echo $student->count_survey; ?></td>
            </tr>
        <?php
        }
        ?>

    </tbody>
</table>
<div class="row justify-content-center">
    <div class="col-md-3">
        <div class="form-group">
          <label for="">เลือก E-Mail</label>
          <select class="form-control" name="cse_email" id="cse_email">
              <?php foreach ($email_list as $key => $email): ?>
                  <option value="<?php echo $email->id; ?>"><?php echo $email->email; ?></option>
              <?php endforeach; ?>
          </select>
        </div>
    </div>
    <div class="col-md-12 text-center mt-3">
        <button class="btn btn-lg btn-primary" type="button" onclick="send_survey(this)">
           <i class="fa fa-send"></i> ส่งสำรวจ
        </button>
    </div>

</div>

<script>
    $(document).ready(function() {

        //iCheck
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_minimal-green',
            radioClass: 'iradio_minimal-green',
            increaseArea: '20%' // optional
        });

        $('#check_all').on('ifChecked', function(event) {
            $('input[name="student[]"]').iCheck('check');
        })

        $('#check_all').on('ifUnchecked', function(event) {
            $('input[name="student[]"]').iCheck('uncheck');

        })
        //End iCheck

        $('.page-link > a').click(function(e) {
            e.preventDefault();
            var url = site_url + "send_survey/get_student_list?" +
                'page=' + $(this).data('ci-pagination-page') +
                '&search_text=' + $('#search_text').val() +
                '&is_answer='+$('#is_answer').val()+
                '&year_graduated=' + $('#year_graduated').val();
            $('#div_table').load(url, function(response, status, request) {
                $('body').scrollTop(0);
                hide_preload();

            });

        });
    }); //end ready
</script>
