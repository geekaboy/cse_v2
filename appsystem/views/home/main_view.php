<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">CRRU CSE</li>
        <li class="breadcrumb-item active">Home</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body text-center">
                            <img src="<?php echo base_url('assets/images/sys_logo.png'); ?>" class="img" width="250" />
                            <h2 class="mt-3" style="color:#F36F21;">Customer Satisfaction Evaluation</h2>
                        
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>