<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">CRRU CSE</li>
        <li class="breadcrumb-item active">Home</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body text-center">

                            <h2 class="mt-3" style="color:#F36F21;">รายงาน(Reports)</h2>
                            <div class="row mt-5">
                              <div class="col-md-6">
                                <button class="btn btn-primary btn-lg btn-block" type="button">
                                    รายงานการตอบกลับของบัณฑิต และใช้งานบัณฑิต
                                </button>
                              </div>
                              <div class="col-md-6">
                                <button class="btn btn-primary btn-lg btn-block" type="button">
                                    รายงานความพึงพอใจของผู้ใช้งานบันฑิต
                                </button>
                              </div>
                            </div>
                            <br><br><br>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
