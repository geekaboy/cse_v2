<?php
$sess = $this->session->userdata();

?>
<style>
.fa{font-family:"fontawesome";}
table, th, td{
  border: 1px solid black;
  border-collapse: collapse;
}
th, td{
    padding:3px 5px;
    font-size: 16px !important;
}
.bg-info {
    background-color: #63c2de !important;
    color: #fff;
}

.bg-info-light {
    background-color: #87d2e8 !important;
    color: #000000;
}

.bg-success {
    background-color: #4dbd74 !important;
    color: #fff;
}

.bg-success-light {
    background-color: rgb(212, 252, 208) !important;
    color: #000000;
}

</style>
<?php
$facname = '';
if($fac_name != '')$facname = $fac_name;

?>
<h2 style="text-align:center;">รายงานการสำรวจ <?php echo $fac_name; ?>  ประจำปีการศึกษา <?php echo $year; ?></h2>
<table style="border:0px;">
    <tr>
        <td style="text-align:center; border:0px;">
            <h2>การตอบกลับสำรวจสถานะการมีงานทำขอบัณฑิต</h2>
        </td>
        <td style="text-align:center; border:0px;">
            <h2>การตอบกลับสำรวจความพึงพอใจของนายจ้าง</h2>
        </td>
    </tr>
    <tr>
        <td style="border:0px;">
            <img src="<?php echo $sess['facStudentChart']; ?>" width="100%">
        </td>
        <td style="border:0px;">
            <img src="<?php echo $sess['facEmployerChart']; ?>" width="100%">
        </td>
    </tr>
</table>
<br><br>
<h3>รายละเอียด</h3>
<table>
    <thead>
        <tr>
            <th  class="bg-light" rowspan="2" style="vertical-align: middle;">ลำดับ</th>
            <th class="bg-light text-center text-bold" width="400" rowspan="2" style="vertical-align: middle;">
                โปรแกรมวิชา
            </th>
            <th colspan="3" class="text-center bg-info">สำรวจสถานะการมีงานทำของบัณฑิต</th>
            <th colspan="2" class="text-center bg-success">สำรวจความพึงพอใจของผู้ใช้บัณฑิต</th>
        </tr>
        <tr>

            <th class="text-center bg-info">บัณฑิต</th>
            <th class="text-center bg-info">มีงานทำ</th>
            <th class="text-center bg-info">คิดเป็นร้อยละ(%)</th>
            <th class="text-center bg-success">ตอบแบบสำรวจ</th>
            <th class="text-center bg-success">คิดเป็นร้อยละ(%)</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $i_row = 0;
    $total_std = 0;
    $total_std_work = 0;
    $total_employer_answer = 0;

    foreach ($maj_list as $key => $maj):
        $i_row++;
        $avg_std = ($maj->total_std_work != 0)?($maj->total_std_work*100)/$maj->total_std:0;
        $avg_employer = ($maj->total_std_work != 0)?($maj->total_employer_answer*100)/$maj->total_std_work:0;
        //==========SUMMARY ===========//
        $total_std+=$maj->total_std;
        $total_std_work+=$maj->total_std_work;
        $avg_std_work = ($total_std_work*100)/$total_std;

        $total_employer_answer+=$maj->total_employer_answer;
        $avg_employer_answer = @(($total_employer_answer*100)/$total_std_work);

    ?>

        <tr>
            <td><?php echo $i_row; ?>.</td>
            <td><strong><?php echo $maj->maj_name; ?></strong></td>
            <td class="bg-info-light" style="text-align:center;"><?php echo $maj->total_std; ?></td>
            <td class="bg-info-light" style="text-align:center;"><?php echo $maj->total_std_work; ?></td>
            <td class="bg-info-light" style="text-align:center;"><?php echo number_format($avg_std, 2); ?>%</td>
            <td class="bg-success-light" style="text-align:center;"><?php echo $maj->total_employer_answer; ?></td>
            <td class="bg-success-light" style="text-align:center;"><?php echo number_format($avg_employer, 2); ?>%</td>

        </tr>
    <?php endforeach; ?>

        <tr class="bg-light">
            <td colspan="2" style="text-align:center;"><b>รวม</b></td>
            <td class="bg-info-light" style="text-align:center;">
                <b><?php echo number_format($total_std); ?></b>
            </td>
            <td class="bg-info-light" style="text-align:center;">
                <b><?php echo number_format($total_std_work); ?></b>
            </td>
            <td class="bg-info-light" style="text-align:center;">
                <b><?php echo number_format($avg_std_work, 2); ?>%</b>
            </td>
            <td class="bg-success-light" style="text-align:center;">
                <b><?php echo number_format($total_employer_answer); ?></b>
            </td>
            <td class="bg-success-light" style="text-align:center;">
                <b><?php echo number_format($avg_employer_answer, 2) ?>%</b>
            </td>
        </tr>
    </tbody>
</table>
