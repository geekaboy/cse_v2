<div class="col-md-12 mt-3">
    <h5><i class="fa fa-commenting" aria-hidden="true"></i> ข้อความเห็นเพิ่มเติม</h5>
    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead>
                <tr class="bg-info">
                    <th>#</th>
                    <th class="text-center">ข้อความเห็น</th>
                    <th class="text-center" width="400">โปรแกรมวิชา</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $i_row = 0;
                if(count($comment_list) == 0){
                    echo '<tr><td class="text-center text-warning" colspan="3">*** ไม่มีข้อมูล ***</td></tr>';
                }
                foreach ($comment_list as $key => $comment):
                $i_row++;
                ?>
                <tr>
                    <td><?php echo $i_row; ?>.</td>
                    <td><?php echo $comment->comment_note ?></td>
                    <td width="400">
                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo $comment->maj_name; ?><br />
                        <i class="fa fa-university" aria-hidden="true"></i> <?php echo $comment->fac_name; ?>
                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>

</div>
