<style>
tbody td {
    font-size: 14px;
}
.info{
    background-color: rgb(208, 242, 252);
}
.success{
    background-color: rgb(212, 252, 208);
}
.tgCollapse{
    cursor: pointer;
}
.question-list>tbody>tr>td{
    font-size: 14px;
    /* font-style: italic; */
}
</style>
<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">CRRU CSE</li>
        <li class="breadcrumb-item active">ความพึงพอใจผู้ใช้บัณฑิต</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-body">
                        <h4 class="card-title text-center">
                            <i class="fa fa-commenting"></i> ความพึงพอใจผู้ใช้บัณฑิต
                        </h4>
                        <!-- <h6 class="card-subtitle"> All bootstrap element classies </h6> -->
                        <div class="row justify-content-center mt-5">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                คณะ/สำนักวิชา
                                            </span>
                                        </div>
                                        <select class="form-control" name="fac_name" id="fac_name">
                                            <option value="">ทั้งหมด</option>
                                        <?php foreach ($fac_list as $key => $fac): ?>
                                            <option value="<?php echo $fac->fac_name; ?>"><?php echo $fac->fac_name; ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                โปรแกรมวิชา
                                            </span>
                                        </div>
                                        <select class="form-control" name="maj_name" id="maj_name">
                                            <option value="">ทั้งหมด</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <select id="year_graduated" class="form-control" name="year_graduated">
                                        <?php foreach ($survey_group as $key => $row): ?>
                                            <option value="<?php echo $row->year_graduated ?>">
                                                จบปีการศึกษา <?php echo $row->year_graduated ?>
                                            </option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>&nbsp;&nbsp; เริ่ม
                                            </span>
                                        </div>
                                        <input class="form-control" id="date_start" type="text" name="date_start" readonly>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>&nbsp;&nbsp; ถึง
                                            </span>
                                        </div>
                                        <input class="form-control" id="date_end" type="text" name="date_end" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="button" id="btn_search">
                                        <i class="fa fa-search"></i> ดูรายงาน
                                    </button>
                                    <button class="btn btn-warning" type="button" id="btn_clear">
                                        <i class="fa fa-eraser"></i> เคลียร์
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center" id="div_detail">

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
