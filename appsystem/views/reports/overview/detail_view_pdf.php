<style>
.fa{font-family:"fontawesome";}
table, th, td{
  border: 1px solid black;
  border-collapse: collapse;
}
th, td{
    padding:3px 5px;
    font-size: 16px !important;
}
.bg-info {
    background-color: #63c2de !important;
    color: #fff;
}
.bg-info-light {
    background-color: #87d2e8 !important;
    color: #fff;
}

</style>

<h2 style="text-align: center; margin:3px 5px;">
    รายงานสถิติประเมินความพึงพอใจของผู้ใช้บัณฑิต
</h2>
<h3 style="text-align: center; margin:3px 5px;">
    <?php echo ($fac_name != NULL)? $fac_name:'คณะ/สำนักวิชา ทั้งหมด'; ?>
    <?php echo ($maj_name != NULL)? ' โปรแกรมวิชา'.$maj_name:'โปรแกรมวิชา ทั้งหมด'; ?>
</h3>
<br>
<img src="<?php echo $this->session->userdata('radar_chart'); ?>" alt="" class="img" width="100%">
<?php
$data_set = array();
$total_avg_score = 0;
foreach ($question_group as $key => $qg):
    $answer = $this->answer_list->get_sum_score($qg->id, $year_graduated, $fac_name, $maj_name);

    $avg_score = ($answer->sum_score != 0)?number_format($answer->sum_score/$answer->count_survey, 2):0;

    $data_set[] = array(
        'question_group'=>$qg->title,
        'avg_score'=>$avg_score
    );
    if($answer->sum_score == 0){
        $data_set = [];
        echo '<tr><td colspan="5" class="text-center text-warning">
            *** ไม่มีข้อมูลในการสำรวจ ***
        </td> </tr>';
        break;
    }
    ?>
<?php
endforeach;
?>

<br><br>
<table style="width:100%;">
    <thead>
        <tr>
            <th class="bg-info" colspan="5">ผลการประเมินคุณภาพบัณฑิตตามกรอบมาตรฐาน TQF</th>
        </tr>
        <tr>
            <th class="bg-info-light">มาตรฐานแต่ละด้าน</th>
            <th class="bg-info-light" style="width:12%;">คะแนนรวม</th>
            <th class="bg-info-light" style="width:12%;">จำนวนคำตอบ</th>
            <th class="bg-info-light" style="width:12%;">คะแนนเฉลี่ย</th>
            <th class="bg-info-light" style="width:12%;">ระดับคุณภาพ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $data_set = array();
        $total_avg_score = 0;
        foreach ($question_group as $key => $qg):
            $answer = $this->answer_list->get_sum_score($qg->id, $year_graduated, $fac_name, $maj_name);

            $avg_score = ($answer->sum_score != 0)?number_format($answer->sum_score/$answer->count_survey, 2):0;
            $total_avg_score += $avg_score;
            $level_txt = '';
            switch ($avg_score) {
                case $avg_score>=4.51:
                $level_txt = '<span class="text-success">มากที่สุด</span>';
                break;
                case $avg_score>=3.51 && $avg_score<=4.50:
                $level_txt = '<span class="text-success">มาก</span>';
                break;
                case $avg_score>=2.51  && $avg_score<=3.50:
                $level_txt = '<span class="text-primary">พอใช้</span>';
                break;
                case $avg_score>=1.51  && $avg_score<=2.50:
                $level_txt = '<span class="text-warning">น้อย</span>';
                break;
                case $avg_score<=1.50:
                $level_txt = '<span class="text-danger">น้อยที่สุด</span>';
                break;
            }
            $data_set[] = array(
                'question_group'=>$qg->title,
                'avg_score'=>$avg_score
            );
            if($answer->sum_score == 0){
                $data_set = [];
                echo '<tr><td colspan="5" class="text-center text-warning">
                    *** ไม่มีข้อมูลในการสำรวจ ***
                </td> </tr>';
                break;
            }
            ?>
            <tr style="background-color:#e6e6e6;" data-qg-id="<?php echo $qg->id; ?>">
                <td><strong><?php echo $qg->order_num.'. '.$qg->title; ?></strong></td>
                <td style="text-align:center;"><strong><?php echo number_format($answer->sum_score); ?></strong></td>
                <td style="text-align:center;"><strong><?php echo number_format($answer->count_survey); ?></strong></td>
                <td style="text-align:center;"><strong><?php echo $avg_score; ?></strong></td>
                <td style="text-align:center;"><strong><?php echo $level_txt; ?></strong></td>
            </tr>

            <?php
            //Detail
            $answer_list_score = $this->answer_list->get_qlist_score($qg->id, $year_graduated, $fac_name, $maj_name);
            foreach ($answer_list_score as $key => $qlist):
                $avg_score = ($qlist->sum_score != 0)?number_format($qlist->sum_score/$qlist->count_survey, 2):0;
                $level_txt = '';
                switch ($avg_score) {
                    case $avg_score>=4.51:
                    $level_txt = '<span class="text-success">มากที่สุด</span>';
                    break;
                    case $avg_score>=3.51 && $avg_score<=4.50:
                    $level_txt = '<span class="text-success">มาก</span>';
                    break;
                    case $avg_score>=2.51  && $avg_score<=3.50:
                    $level_txt = '<span class="text-primary">พอใช้</span>';
                    break;
                    case $avg_score>=1.51  && $avg_score<=2.50:
                    $level_txt = '<span class="text-warning">น้อย</span>';
                    break;
                    case $avg_score<=1.50:
                    $level_txt = '<span class="text-danger">น้อยที่สุด</span>';
                    break;
                }
            ?>
            <tr class="tgCollapse">
                <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $qlist->order_num.'. '.$qlist->title; ?></td>
                <td style="text-align:center;"><?php echo number_format($qlist->sum_score); ?></td>
                <td style="text-align:center;"><?php echo number_format($qlist->count_survey); ?></td>
                <td style="text-align:center;"><?php echo $avg_score; ?></td>
                <td style="text-align:center;"><?php echo $level_txt; ?></td>
            </tr>
            <?php endforeach; ?>
            <tr class="collapseDetail" id="collapse_<?php echo $qg->id; ?>" style="display:none;">
                <td colspan="5" id="tdCollapse_<?php echo $qg->id; ?>" style="padding:0px;"></td>
            </tr>
        <?php
        endforeach;
        ?>
        <?php
        $avg_score_summary = ($answer->sum_score != 0)?$total_avg_score/count($question_group):0;
        $level_txt = '';
        switch ($avg_score_summary) {
            case $avg_score_summary>=4.51:
            $level_txt = '<span class="text-success">มากที่สุด</span>';
            break;
            case $avg_score_summary>=3.51 && $avg_score_summary<=4.50:
            $level_txt = '<span class="text-success">มาก</span>';
            break;
            case $avg_score_summary>=2.51  && $avg_score_summary<=3.50:
            $level_txt = '<span class="text-primary">พอใช้</span>';
            break;
            case $avg_score_summary>=1.51  && $avg_score_summary<=2.50:
            $level_txt = '<span class="text-warning">น้อย</span>';
            break;
            case $avg_score_summary<=1.50:
            $level_txt = '<span class="text-danger">น้อยที่สุด</span>';
            break;
        }

        ?>
        <tr  class="bg-light">
            <th colspan="3" class="text-center"><strong>สรุป</strong></th>
            <th class="text-center"><?php echo number_format($avg_score_summary, 2); ?></th>
            <th class="text-center"><?php echo $level_txt; ?></th>
        </tr>

    </tbody>
</table>

<table style="width:100%;border:0px;margin-top:25px;">
    <tr>
        <td style="border:0px;padding:0px;">
            <table style="width:100%;margin-right:10px;">
                <thead>
                    <tr>
                        <th class="bg-info" colspan="3" style="text-align:center;">
                            ลักษณะของหน่วยงาน
                        </th>
                    </tr>
                    <tr>
                        <th class="bg-info-light">ประเภทหน่วยงาน</th>
                        <th class="bg-info-light">จำนวน</th>
                        <th class="bg-info-light">คิดเป็นร้อยละ</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $sum_count_org = 0;
                foreach ($get_oragiz as $key => $oragiz) {
                    $sum_count_org+=$oragiz->count_org;
                }
                $sum_avg_org = 0;
                foreach ($get_oragiz as $key => $org):
                    $count_org = ($org->count_org != NULL)?$org->count_org:0;
                    $avg_org = ($count_org*100)/$sum_count_org;
                    $sum_avg_org += $avg_org
                ?>
                    <tr>
                        <td><?php echo $org->org_title; ?></td>
                        <td style="text-align:center;"><?php echo $count_org; ?></td>
                        <td style="text-align:center;"><?php echo number_format($avg_org, 2); ?>%</td>
                    </tr>
                <?php endforeach; ?>
                    <tr>
                        <th>รวมสรุป</th>
                        <th><?php echo $sum_count_org; ?></th>
                        <th><?php echo $sum_avg_org; ?>%</th>
                    </tr>

                </tbody>

            </table>
        </td>
        <td style="border:0px;padding:0px;">
            <table style="width:100%;margin-left:10px;">
                <thead>
                    <tr>
                        <th class="bg-info" colspan="3" style="text-align:center;">
                            ระดับตำแหน่งในหน่วยงาน
                        </th>
                    </tr>
                    <tr>
                        <th class="bg-info-light">ระดับตำแหน่ง</th>
                        <th class="bg-info-light">จำนวน</th>
                        <th class="bg-info-light">คิดเป็นร้อยละ</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                $sum_count = 0;
                foreach ($director_type as $key => $director) {
                    $sum_count+=$director->count_director;
                }
                $sum_avg_director = 0;
                foreach ($director_type as $key => $director):
                    $count_director = ($director->count_director != NULL)?$director->count_director:0;
                    $avg_director = ($count_director*100)/$sum_count;
                    $sum_avg_director += $avg_director;
                ?>
                    <tr>
                        <td><?php echo $director->director_title; ?></td>
                        <td style="text-align:center;"><?php echo $count_director; ?></td>
                        <td style="text-align:center;"><?php echo number_format($avg_director, 2); ?>%</td>
                    </tr>
                <?php endforeach; ?>
                    <tr>
                        <th>รวมสรุป</th>
                        <th><?php echo $sum_count; ?></th>
                        <th><?php echo $sum_avg_director; ?>%</th>
                    </tr>
                </tbody>

            </table>
        </td>
    </tr>
</table>

<br><br><br>
<h2 style="text-align:center;">ความเห็นเพิ่มเติมจากผู้ใช้งานบัณฑิต</h2>

<table style="width:100%;border:0px;margin-top:10px;overflow: wrap;autosize:2.4;" autosize="2.4">
    <thead>
        <tr>
            <th class="bg-info" width="30">#</th>
            <th class="bg-info">ข้อความเห็น</th>
            <th class="bg-info" width="200">โปรแกรมวิชา</th>
        </tr>
    </thead>
    <tbody>
        <?php if (empty($comment_list)): ?>
            <tr><td colspan="3" class="text-center" style="text-align:center;"> *** ไม่มีข้อมูล ***</td></tr>
        <?php else:
            $i_row = 0;
            foreach ($comment_list as $key => $comment){
                $i_row++;
        ?>
            <tr>
                <td><?php echo $i_row; ?>.</td>
                <td><?php echo $comment->comment_note ?></td>
                <td>
                    <?php echo $comment->maj_name; ?> -
                    <?php echo $comment->fac_name; ?>
                </td>
            </tr>
        <?php
            }
        endif;
        ?>

    </tbody>
</table>
