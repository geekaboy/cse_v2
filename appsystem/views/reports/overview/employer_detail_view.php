<div class="col-md-6 mt-3">
    <h5><i class="fa fa-building-o" aria-hidden="true"></i> ลักษณะของหน่วยงาน</h5>
    <div class="table-responsive">
        <table class="table table-hover table-bordered ">
            <thead>
                <tr class="bg-info">
                    <th>ประเภทหน่วยงาน</th>
                    <th class="text-center" width="150">จำนวน</th>
                    <th class="text-center" width="150">คิดเป็นร้อยละ</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $sum_count_org = 0;
            foreach ($get_oragiz as $key => $oragiz) {
                $sum_count_org+=$oragiz->count_org;
            }
            $sum_avg_org = 0;
            foreach ($get_oragiz as $key => $org):
                $count_org = ($org->count_org != NULL)?$org->count_org:0;
                $avg_org = @(($count_org*100)/$sum_count_org);
                $sum_avg_org += $avg_org
            ?>
                <tr>
                    <td><?php echo $org->org_title; ?></td>
                    <td class="text-center"><?php echo $count_org; ?></td>
                    <td class="text-center"><?php echo number_format($avg_org, 2); ?>%</td>
                </tr>
            <?php endforeach; ?>
                <tr  class="bg-light">
                    <th class="text-center">รวมสรุป</th>
                    <th class="text-center"><?php echo $sum_count_org; ?></th>
                    <th class="text-center"><?php echo $sum_avg_org; ?>%</th>
                </tr>

            </tbody>

        </table>
    </div>

</div>

<div class="col-md-6 mt-3">
    <h5><i class="fa fa-briefcase" aria-hidden="true"></i> ระดับตำแหน่งในหน่วยงาน</h5>
    <div class="table-responsive">
        <table class="table table-hover table-bordered ">
            <thead>
                <tr class="bg-info">
                    <th>ระดับตำแหน่ง</th>
                    <th class="text-center" width="150">จำนวน</th>
                    <th class="text-center" width="150">คิดเป็นร้อยละ</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $sum_count = 0;
            foreach ($director_type as $key => $director) {
                $sum_count+=$director->count_director;
            }
            $sum_avg_director = 0;
            foreach ($director_type as $key => $director):
                $count_director = ($director->count_director != NULL)?$director->count_director:0;
                $avg_director = @(($count_director*100)/$sum_count);
                $sum_avg_director += $avg_director;
            ?>
                <tr>
                    <td><?php echo $director->director_title; ?></td>
                    <td class="text-center"><?php echo $count_director; ?></td>
                    <td class="text-center"><?php echo number_format($avg_director, 2); ?>%</td>
                </tr>
            <?php endforeach; ?>
                <tr  class="bg-light">
                    <th class="text-center">รวมสรุป</th>
                    <th class="text-center"><?php echo $sum_count; ?></th>
                    <th class="text-center"><?php echo $sum_avg_director; ?>%</th>
                </tr>
            </tbody>

        </table>
    </div>

</div>
