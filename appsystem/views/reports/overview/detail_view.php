<div class="col-md-12 mt-3">
    <div class="row">
        <div class="col-md-12 mb-3">
          <button type="button" class="btn btn-secondary pull-right" onclick="print(this)">
              <i class="fa fa-print" aria-hidden="true"></i> พิมพ์
          </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <h5>ผลการประเมินคุณภาพบัณฑิตตามกรอบมาตรฐาน TQF</h5>
            <strong>รายงานตาม <?php echo ($fac_name != NULL)? $fac_name:'คณะ/สำนักวิชา ทั้งหมด'; ?>
            <?php echo ($maj_name != NULL)? ' โปรแกรมวิชา'.$maj_name:'โปรแกรมวิชา ทั้งหมด'; ?></strong>
        </div>
    </div>

    <div class="row justify-content-center mt-5">
        <div class="col-md-10" id="div_chart" style="display:none;">
            <canvas id="chart_overview"></canvas>
        </div>
        <div class="col-md-12 mt-3">

            <div class="table-responsive">
                <table class="table table-hover table-bordered question-group">
                    <thead>
                        <tr class="bg-info">
                            <th class="text-center">มาตรฐานแต่ละด้าน</th>
                            <th class="text-center" width="150">คะแนนรวม</th>
                            <th class="text-center" width="150">จำนวนคำตอบ</th>
                            <th class="text-center" width="150">คะแนนเฉลี่ย</th>
                            <th class="text-center" width="150">ระดับคุณภาพ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $data_set = array();
                        $total_avg_score = 0;
                        foreach ($question_group as $key => $qg):
                            $answer = $this->answer_list->get_sum_score($qg->id, $year_graduated, $fac_name, $maj_name);

                            $avg_score = ($answer->sum_score != 0)?number_format($answer->sum_score/$answer->count_survey, 2):0;
                            $total_avg_score += $avg_score;
                            $level_txt = '';
                            switch ($avg_score) {
                                case $avg_score>=4.51:
                                $level_txt = '<span class="text-success">มากที่สุด</span>';
                                break;
                                case $avg_score>=3.51 && $avg_score<=4.50:
                                $level_txt = '<span class="text-success">มาก</span>';
                                break;
                                case $avg_score>=2.51  && $avg_score<=3.50:
                                $level_txt = '<span class="text-primary">พอใช้</span>';
                                break;
                                case $avg_score>=1.51  && $avg_score<=2.50:
                                $level_txt = '<span class="text-warning">น้อย</span>';
                                break;
                                case $avg_score<=1.50:
                                $level_txt = '<span class="text-danger">น้อยที่สุด</span>';
                                break;
                            }
                            $data_set[] = array(
                                'question_group'=>$qg->title,
                                'avg_score'=>$avg_score
                            );
                            if($answer->sum_score == 0){
                                $data_set = [];
                                echo '<tr><td colspan="5" class="text-center text-warning">
                                    *** ไม่มีข้อมูลในการสำรวจ ***
                                </td> </tr>';
                                break;
                            }
                            ?>
                            <tr class="tgCollapse" data-qg-id="<?php echo $qg->id; ?>">
                                <td data-toggle="tooltip" data-placement="top" title="คลิกเพื่อแสดงรายละเอียด"><strong><?php echo $qg->order_num.'. '.$qg->title; ?></strong></td>
                                <td class="text-center"><?php echo number_format($answer->sum_score); ?></td>
                                <td class="text-center"><?php echo number_format($answer->count_survey); ?></td>
                                <td class="text-center"><?php echo $avg_score; ?></td>
                                <td class="text-center"><?php echo $level_txt; ?></td>
                            </tr>
                            <tr class="collapseDetail" id="collapse_<?php echo $qg->id; ?>" style="display:none;">
                                <td colspan="5" id="tdCollapse_<?php echo $qg->id; ?>" style="padding:0px;"></td>
                            </tr>
                        <?php
                        endforeach;
                        ?>
                        <?php
                        $avg_score_summary = ($answer->sum_score != 0)?$total_avg_score/count($question_group):0;
                        $level_txt = '';
                        switch ($avg_score_summary) {
                            case $avg_score_summary>=4.51:
                            $level_txt = '<span class="text-success">มากที่สุด</span>';
                            break;
                            case $avg_score_summary>=3.51 && $avg_score_summary<=4.50:
                            $level_txt = '<span class="text-success">มาก</span>';
                            break;
                            case $avg_score_summary>=2.51  && $avg_score_summary<=3.50:
                            $level_txt = '<span class="text-primary">พอใช้</span>';
                            break;
                            case $avg_score_summary>=1.51  && $avg_score_summary<=2.50:
                            $level_txt = '<span class="text-warning">น้อย</span>';
                            break;
                            case $avg_score_summary<=1.50:
                            $level_txt = '<span class="text-danger">น้อยที่สุด</span>';
                            break;
                        }

                        ?>
                        <tr  class="bg-light">
                            <th colspan="3" class="text-center"><strong>สรุป</strong></th>
                            <th class="text-center"><?php echo number_format($avg_score_summary, 2); ?></th>
                            <th class="text-center"><?php echo $level_txt; ?></th>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div id="chart_data" style="display:none;">
                <?php echo json_encode($data_set); ?>
            </div>

        </div>
    </div>
</div>

<?php $this->load->view('reports/overview/employer_detail_view.php'); ?>

<?php $this->load->view('reports/overview/comment_view.php'); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('.tgCollapse').click(get_qlist_data);
    });
</script>
