<?php
$total_std = 0;
$total_std_work = 0;
$total_employer_answer = 0;
foreach ($faculty_list as $key => $faculty) {
    //==========SUMMARY ===========//
    $total_std+=$faculty->total_std;
    $total_std_work+=$faculty->total_std_work;
    $total_employer_answer+=$faculty->total_employer_answer;

}
    $avg_std_work = @(($total_std_work*100)/$total_std);
    $avg_std = @((($total_std - $total_std_work)*100) /$total_std);

    $avg_employer_suvey = @((($total_std_work - $total_employer_answer) * 100) / $total_std_work);
    $avg_employer_answer = @(($total_employer_answer*100)/ $total_std_work);


?>

<input type="hidden" name="avg_employer_answer" id="avg_employer_answer" value="<?php echo number_format($avg_employer_answer,2); ?>">
<input type="hidden" name="avg_employer_suvey" id="avg_employer_suvey" value="<?php echo number_format($avg_employer_suvey,2); ?>">

<input type="hidden" name="avg_std" id="avg_std" value="<?php echo number_format($avg_std,2); ?>">
<input type="hidden" name="avg_std_work" id="avg_std_work" value="<?php echo number_format($avg_std_work,2); ?>">
<div class="col-md-12 mb-3 mt-5">
  <button type="button" class="btn btn-secondary pull-right" onclick="print()">
      <i class="fa fa-print" aria-hidden="true"></i> พิมพ์
  </button>
</div>
<div class="col-md-6">
    <div class="card">
        <div class="card-header">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i> การตอบกลับสำรวจสถานะการมีงานทำขอบัณฑิต
        </div>
        <div class="card-body">
            <canvas id="student-chart-area"></canvas>
            <table class="table table-bordered mt-3">
                <thead>
                    <tr class="bg-info">
                        <th class="text-center">จำนวนบัณฑิต</th>
                        <th class="text-center">จำนวนที่มีงานทำ</th>
                        <th class="text-center">คิดเป็นร้อยละ(%)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center"><b><?php echo number_format($total_std); ?></b></td>
                        <td class="text-center"><b><?php echo number_format($total_std_work); ?></b></td>
                        <td class="text-center text-primary"><b><?php echo number_format($avg_std_work, 2); ?>%</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="card">
        <div class="card-header">
            <i class="fa fa-handshake-o" aria-hidden="true"></i> การตอบกลับสำรวจความพึงพอใจของนายจ้าง
        </div>
        <div class="card-body">
            <canvas id="employer-chart-area"></canvas>
            <table class="table table-bordered mt-3">
                <thead>
                    <tr class="bg-info">
                        <th class="text-center">จำนวนสำรวจ</th>
                        <th class="text-center">จำนวนตอบกลับ</th>
                        <th class="text-center">คิดเป็นร้อยละ(%)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center"><b><?php echo number_format($total_std_work); ?></b></td>
                        <td class="text-center"><b><?php echo number_format($total_employer_answer); ?></b></td>
                        <td class="text-center text-primary"><b><?php echo number_format($avg_employer_answer, 2); ?>%</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>

<div class="col-md-12">
    <h4><i class="fa fa-info-circle"></i> รายละเอียด</h4>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th  class="bg-light" rowspan="2" style="vertical-align: middle;">ลำดับ</th>
                    <th class="bg-light text-center text-bold" width="400" rowspan="2" style="vertical-align: middle;">
                        คณะ/สำนัก
                    </th>
                    <th colspan="3" class="text-center bg-info">สำรวจสถานะการมีงานทำของบัณฑิต</th>
                    <th colspan="2" class="text-center bg-success">สำรวจความพึงพอใจของผู้ใช้บัณฑิต</th>
                    <th rowspan="2" class="text-center bg-warning" style="vertical-align: middle;">เพิ่มเติม</th>
                </tr>
                <tr>

                    <th class="text-center bg-info">บัณฑิต</th>
                    <th class="text-center bg-info">มีงานทำ</th>
                    <th class="text-center bg-info">คิดเป็นร้อยละ(%)</th>
                    <th class="text-center bg-success">ตอบแบบสำรวจ</th>
                    <th class="text-center bg-success">คิดเป็นร้อยละ(%)</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $i_row = 0;
            $total_std = 0;
            $total_std_work = 0;
            $total_employer_answer = 0;

            foreach ($faculty_list as $key => $faculty):
                $i_row++;

                $avg_std = ($faculty->total_std_work*100)/$faculty->total_std;
                $avg_employer = @(($faculty->total_employer_answer*100)/$faculty->total_std_work);
                //==========SUMMARY ===========//
                $total_std+=$faculty->total_std;
                $total_std_work+=$faculty->total_std_work;
                $avg_std_work = ($total_std_work*100)/$total_std;

                $total_employer_answer+=$faculty->total_employer_answer;
                $avg_employer_answer = @(($total_employer_answer*100)/$total_std_work);

            ?>

                <tr>
                    <td><?php echo $i_row; ?>.</td>
                    <td><strong><?php echo $faculty->fac_name; ?></strong></td>
                    <td class="text-center info"><?php echo $faculty->total_std; ?></td>
                    <td class="text-center info"><?php echo $faculty->total_std_work; ?></td>
                    <td class="text-center info"><?php echo number_format($avg_std, 2); ?>%</td>
                    <td class="text-center success"><?php echo $faculty->total_employer_answer; ?></td>
                    <td class="text-center success"><?php echo number_format($avg_employer, 2); ?>%</td>
                    <td class="text-center">
                        <a href="<?php echo site_url('reports/faculty').'?fac_name='.$faculty->fac_name.'&year='.$year; ?>" class="btn btn-primary" target="_blank">
                            <i class="fa fa-info-circle" aria-hidden="true"></i> เพิ่มเติม
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>

                <tr class="bg-light">
                    <td colspan="2" class="text-center"><b>รวม</b></td>
                    <td class="text-center info"><b><?php echo number_format($total_std); ?></b></td>
                    <td class="text-center info"><b><?php echo number_format($total_std_work); ?></b></td>
                    <td class="text-center info"><b><?php echo number_format($avg_std_work, 2); ?>%</b></td>
                    <td class="text-center success"><b><?php echo number_format($total_employer_answer); ?></b></td>
                    <td class="text-center success"><b><?php echo number_format($avg_employer_answer, 2) ?>%</b></td>
                    <td class="text-center"></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
