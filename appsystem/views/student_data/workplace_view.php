<style>
tbody td {
    font-size: 14px;
}
</style>
<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">CRRU CSE</li>
        <li class="breadcrumb-item active">ข้อมูลสถานทีทำงานบัณฑิต</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-body">
                        <h4 class="card-title text-center">
                            <i class="fa fa-database"></i> ข้อมูลสถานทีทำงานบัณฑิต
                        </h4>
                        <!-- <h6 class="card-subtitle"> All bootstrap element classies </h6> -->
                        <div class="row justify-content-center mt-5">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input id="search_text" name="search_text" class="form-control" type="text"
                                        placeholder="ค้นหา รหัสนักศึกษา, ชื่อบัณฑิต, โปรแกรมวิชา, คณะ/สำนัก, สถานประกอบการ">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select id="year_graduated" class="form-control" name="year_graduated">
                                    <?php foreach ($survey_group as $key => $row): ?>
                                        <option value="<?php echo $row->year_graduated ?>">
                                            จบปีการศึกษา <?php echo $row->year_graduated ?>
                                        </option>
                                    <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="button" id="btn_search">
                                        <i class="fa fa-search"></i> ค้นหา
                                    </button>
                                    <button class="btn btn-warning" type="button" id="btn_clear">
                                        <i class="fa fa-eraser"></i> เคลียร์
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" id="div_table">

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
