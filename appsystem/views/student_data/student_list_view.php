<div class="row justify-content-center mt-5">
    <?php echo $this->pagination->create_links(); ?>

</div>
<i class="fa fa-search"></i> <b>ค้นหา: </b> <?php echo ($search_text != '')?$search_text:'-'; ?>
<b> จบปีการศึกษา: </b> <?php echo $year_graduated; ?>
<b> จำนวนทั้งหมด: </b> <?php echo $total_row; ?> <b>รายการ</b>

<table class="table table-hover table-bordered table-condensed">
    <thead>
        <tr class="bg-info">
            <th width="20">#</th>
            <th>รหัสนักศึกษา</th>
            <th>ชื่อ-สกุล</th>
            <th>โปรแกรมวิชา</th>
            <th>คณะ/สำนัก</th>
            <th>E-Mail</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i_row = $start;
        if (count($student_list) == 0) {
            echo '<tr><td colspan="7" class="text-center"><h4 class="text-info">*** ไม่พบข้อมูล ***</h4></td></tr>';
        }
        foreach ($student_list as $key => $student) {
            $i_row++;
            ?>
            <tr>
                <td><?php echo $i_row; ?>. </td>
                <td><?php echo $student->std_id; ?></td>
                <td><?php echo $student->prefix.$student->fullname; ?></td>
                <td><?php echo $student->maj_name; ?></td>
                <td><?php echo $student->fac_name; ?></td>
                <td><?php echo $student->email; ?></td>
            </tr>
        <?php
        }
        ?>

    </tbody>
</table>
<div class="row justify-content-center mt-5">
    <?php echo $this->pagination->create_links(); ?>

</div>

<script>
    $(document).ready(function() {
        $('.page-link > a').click(function(e) {
            e.preventDefault();
            show_preload();
            var url = site_url + "student_data/get_list?" +
                'page=' + $(this).data('ci-pagination-page') +
                '&search_text=' + $('#search_text').val() +
                '&year_graduated=' + $('#year_graduated').val();
            $('#div_table').load(url, function(response, status, request) {
                $('body').scrollTop(0);
                hide_preload();

            });

        });
    }); //end ready
</script>
