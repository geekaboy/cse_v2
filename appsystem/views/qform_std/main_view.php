<html lang="th">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <meta name="site_url" content="<?php echo site_url(); ?>">
    <meta name="base_url" content="<?php echo base_url(); ?>">

    <title>CSE: Customer Satisfaction Evaluation</title>
    <!-- Icons-->
    <link href="<?php echo base_url(); ?>assets/theme/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Main styles for this application-->
    <link href="<?php echo base_url(); ?>assets/theme/css/style.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>assets/theme/vendors/jquery/js/jquery.min.js"></script>

    <!--JQueryUI -->
    <script src="<?php echo base_url(); ?>assets/plugins/JQuery/jquery-ui/jquery-ui.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/plugins/JQuery/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
    <style media="screen">
    body {
        font-family: 'Sarabun', serif;
        letter-spacing: 0.1px;
        background-color: #e8e8e8;

    }
    .help-block>.badge {
        font-size: 12px;
    }
    .form-control {
        color: #000;
    }
    </style>
    <script src="<?php echo base_url('assets/plugins/breadcrumbs/js/modernizr.js'); ?>" charset="utf-8"></script>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed">
    <main class="main">
        <div class="container-fluid mt-5">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h5><i class="fa fa-commenting" aria-hidden="true"></i> แบบสอบถามสถานะการมีงานทำของบัณฑิตมหาวิทยาลัยราชภัฏเชียงราย</h5>
                        <div class="card border-info">
                            <div class="card-header"><span class="ti-user"></span> ข้อมูลส่วนตัวบัณฑิต</div>
                            <div class="card-body" >
                                <div class="col-md-12">

                                    <dl class="row">
                                        <dt class="col-sm-2"><strong>ชื่อ - นามสกุล: </strong></dt>
                                        <dd class="col-sm-9"><?php echo $std_data->prefix.$std_data->fullname; ?></dd>

                                        <dt class="col-sm-2"><strong>รหัสนักศึกษา: </strong></dt>
                                        <dd class="col-sm-9"><?php echo $std_data->std_id; ?></dd>

                                        <dt class="col-sm-2"><strong>โปรแกรมวิชา: </strong></dt>
                                        <dd class="col-sm-9"><?php echo $std_data->maj_name; ?></dd>

                                        <dt class="col-sm-2"><strong>คณะ/สำนัก: </strong></dt>
                                        <dd class="col-sm-9"><?php echo $std_data->fac_name; ?></dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 mt-3 mb-3">
                        <div class="card border-info">
                            <div class="card-header"><span class="ti-clipboard"></span> แบบสอบถาม</div>
                            <div class="card-body" >
                                <p class="help-block">
                                    <strong><u>คำจี้แจง</u></strong> &nbsp;&nbsp;โปรดเติมข้อความในช่องว่างให้สมบูรณ์ และช่องที่มีเครื่องหมาย <span class="text-danger">* ดอกจัน</span>  จำเป็นต้องกรอกข้อมูล
                                </p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="<?php echo site_url('survey/save_std'); ?>" method="post" name="frm_survey" id="frm_survey">
                                            <input type="hidden" name="std_id" id="std_id" value="<?php echo $std_data->std_id; ?>">
                                            <input type="hidden" name="citizen_id" id="citizen_id" value="<?php echo $std_data->citizen_id; ?>">
                                            <input type="hidden" name="token_code" id="token_code" value="<?php echo $token_code; ?>">
                                            <strong><i class="ti-briefcase"></i> สถานะการทำงาน</strong>
                                            <fieldset class="form-group">
                                                <input type="hidden" name="work_status" >
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input" name="work_status" value="0">
                                                        ยังไม่มีงานทำ
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input" name="work_status" value="1" checked>
                                                        มีงานทำ/อาชีพอิสระ/เจ้าของกิจการ
                                                    </label>
                                                </div>


                                            </fieldset>
                                            <div class="text-center" style="display:none;" id="div_no_work">
                                                <button type="button" class="btn btn-primary" id="btn_send_no_work">
                                                    <i class="fa fa-paper-plane" aria-hidden="true"></i> ส่งแบบสอบถาม
                                                </button>
                                            </div>
                                            <hr>
                                            <div id="div_have_work">
                                                <fieldset class="form-group">

                                                    <label>เบอร์โทรศัพท์บัณฑิต <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control col-md-4" id="std_mobile_number" name="std_mobile_number" required>

                                                </fieldset>
                                                <strong><i class="ti-info-alt"></i> ข้อมูลการทำงาน</strong><br>

                                                <label>ลักษณะงานที่ทำตรงกับสาขาที่ท่านได้สำเร็จการศึกษาหรือไม่ <span class="text-danger">*</span></label>
                                                <div class="form-row">
                                                    <div class="form-group col-md-2">
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                <input type="hidden" name="work_match" value="">
                                                                <input type="radio" class="form-check-input" name="work_match" value="1" required>
                                                                ตรง
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" name="work_match" value="0" required>
                                                                ไม่ตรง
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <label>ระยะเวลาที่ท่านทำงานอยู่กับ บริษัท/องค์กร ของท่าน <span class="text-danger">*</span></label>
                                                <div class="form-row">
                                                    <?php
                                                    foreach ($time_range_list as $key => $time_range): ?>

                                                    <div class="form-group col-md-2">
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" name="work_time_range" id="work_time_range" value="<?php echo $time_range->id; ?>" required>
                                                                <?php echo $time_range->name; ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>

                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-5">
                                                    <label>ชื่อหน่วยงาน/สถานประกอบกอบ <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="organization_name" name="organization_name" placeholder="โปรดกรอก ชื่อหน่วยงาน/สถานประกอบกอบ Ex. มหาวิทยาลัยราชภัฏเชียงราย" required>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="work_position">ตำแหน่งงาน <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="work_position" name="work_position" required>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="salary">เงินเดือน (บาท)<span class="text-danger">*</span></label>
                                                    <input type="number" class="form-control" id="salary" name="salary" required>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="addr">ที่อยู่สถานที่ทำงาน <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="addr" name="addr" placeholder="Ex. เลขที่ 80 อาคารอธิการบดี ถนนพหลโยธิน" required>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="province">จังหวัด <span class="text-danger">*</span></label>
                                                    <select class="form-control" name="province" id="province" required>
                                                        <option value="">เลือกจังหวัด</option>
                                                        <?php
                                                        foreach ($provice_list as $provice) {
                                                            ?>
                                                            <option value="<?php echo $provice->name_th; ?>" data-code="<?php echo $provice->id; ?>">
                                                                <?php echo $provice->name_th.' ( '.$provice->name_en.' )'; ?>
                                                            </option>

                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <label for="amphures">แขวง/เขต/อำเภอ <span class="text-danger">*</span></label>
                                                    <select class="form-control" name="amphures" id="amphures" required>
                                                        <option value="">เลือกอำเภอ</option>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <label for="tambon">ตำบล/แขวง <span class="text-danger">*</span></label>
                                                    <select class="form-control" name="tambon" id="tambon" required>
                                                        <option value="">เลือกตำบล</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="zipcode">รหัสไปรษณีย์ <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="zipcode" name="zipcode" required>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="province">ประเทศ <span class="text-danger">*</span></label>
                                                    <select class="form-control" name="country" id="country" required>
                                                        <option value="">เลือกประเทศ</option>
                                                        <?php
                                                        foreach ($country_list as $country) {
                                                            ?>
                                                            <option value="<?php echo $country->name_th; ?>" data-code="<?php echo $country->id; ?>" <?php echo ($country->id == 'TH')?'selected':''; ?>>
                                                                <?php echo $country->name_th.' ( '.$country->name_en.' )'; ?>
                                                            </option>

                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label>ชื่อหัวหน้างาน <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="director_name" name="director_name" placeholder="โปรดกรอก ชื่อหัวหน้างาน" required>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>E-Mail หัวหน้างาน <span class="text-danger">*</span></label>
                                                    <input type="email" class="form-control" id="director_email" name="director_email" placeholder="โปรดกรอก E-Mail หัวหน้างาน" required>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="tel">เบอร์โทรศัพท์หัวหน้างาน <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control"  id="tel" name="tel" required>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="fax">เบอร์แฟกซ์ </label>
                                                    <input type="text" class="form-control"  id="fax" name="fax">
                                                </div>


                                            </div>

                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary" id="btn_sendForm">
                                                    <i class="fa fa-send" aria-hidden="true"></i> ส่งแบบฟอร์ม
                                                </button>
                                            </div>
                                        </div>

                                    </form>

                                </div><!-- End col-md-12 -->
                            </div> <!-- End row -->

                        </div><!--End card-body-->

                    </div><!-- End card -->

                </div>

            </div>
        </div>
    </div>
</main>
