<link rel="stylesheet" href="<?php echo base_url('assets/plugins/breadcrumbs/css/style.css'); ?>">
<link href="https://fonts.googleapis.com/css?family=Taviraj" rel="stylesheet">
<!-- Preload  -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/preload/_css/Icomoon/style.css'); ?>">
<style media="screen">
    body {
        font-family: 'Taviraj', serif;
        letter-spacing: 0.1px;
        background-color: #e8e8e8;
    }

    .help-block>.badge {
        font-size: 12px;
    }
</style>
<script src="<?php echo base_url('assets/plugins/breadcrumbs/js/modernizr.js'); ?>" charset="utf-8"></script>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">CSE SYSTEM</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
            aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

    </nav>

    <div class="container">

        <?php $this->load->view('qform/advice_view.php'); ?>

        <div class="row justify-content-center">
            <div class="col-md-7">
                <nav class="mx-auto" style="width:86%;">
                    <ol class="cd-breadcrumb triangle">
                        <li class="current" id="bc_step1"><a href="#" onclick="to_step_01()"><span
                                    class="ti-user"></span> ขั้นตอนที่ 1 ข้อมูลทั่วไป</a></li>
                        <li id="bc_step2"><a href="#" onclick="to_step_02()"><span class="ti-pencil-alt"></span>
                                ขั้นตอนที่ 2 ตอบแบบสอบถาม</a></li>
                        <li id="bc_step3"><a href="#"><span class="ti-check-box"></span> เสร็จสิ้น</a></li>
                    </ol>
                </nav>

            </div>
        </div>
        <!--Preload -->
        <div class="row mt-3 mb-3" id="div_preload" style="display:none;">
            <div class="col-md-12">
                <div id="loading">
                    <div id="loading-center">
                        <div id="loading-center-absolute">
                            <div class="object" id="object_one"></div>
                            <div class="object" id="object_two"></div>
                            <div class="object" id="object_three"></div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-md-12">
                <div class="card border-info">
                    <div class="card-header bg-primary text-white"><span class="ti-user"></span> ข้อมูลส่วนตัวบัณฑิต
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <dl class="row mb-0">
                                <dt class="col-sm-2"><strong>ชื่อ - นามสกุล: </strong></dt>
                                <dd class="col-sm-9">
                                    <?php echo $work_data['prefix_name'].$work_data['std_fname'].'  '.$work_data['std_lname']; ?>
                                </dd>

                                <dt class="col-sm-2"><strong>รหัสนักศึกษา: </strong></dt>
                                <dd class="col-sm-9"><?php echo $work_data['std_id']; ?></dd>

                                <dt class="col-sm-2"><strong>โปรแกรมวิชา: </strong></dt>
                                <dd class="col-sm-9"><?php echo $work_data['maj_name']; ?></dd>

                                <dt class="col-sm-2"><strong>คณะ/สำนัก: </strong></dt>
                                <dd class="col-sm-9"><?php echo $work_data['fac_name']; ?></dd>

                                <dt class="col-sm-2"><strong>สถานะการทำงาน: </strong></dt>
                                <dd class="col-sm-9">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="work_status" checked
                                            value="1">
                                        <label class="form-check-label">ปัจุบันทำงานที่นี่</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="work_status" value="0">
                                        <label class="form-check-label">ไม่ได้ทำงานที่นี่แล้ว</label>
                                    </div>
                                    <div class="form-group" id="div_cause" style="display:none;">
                                        <label for=""><i class="ti-comment-alt"></i>
                                            ความคิดเห็นหรือข้อเสนอแนะเพื่อพัฒนาคุณภาบัณฑิต</label>
                                        <textarea class="form-control" name="work_cause" id="work_cause" rows="8"
                                            cols="80" placeholder="โปรดระบุความคิดเห็น..."></textarea>
                                        <button type="button" class="btn btn-outline-primary float-right mt-3"
                                            id="btn_work_cause">
                                            <span class="ti-check-box"></span> ส่งแบบสอบถาม
                                        </button>
                                    </div>

                                </dd>
                            </dl>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- model_preload-->
        <div id="model_preload" class="modal bs-example-modal-lg fade" tabindex="-1" role="dialog" aria-labelledby=""
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="border:0px; background-color: #fff0">
                    <div class="modal-body">

                        <div class="text-center text-white fa-5x">
                            <i class="fa fa-cog fa-spin"></i>
                            <h4>โปรดรอสักครู่ระบบกำลังดำเนินการ</h4>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <form id="q_form">
            <!-- Step 1 ขั้นตอนที่ 1  -->
            <?php $this->load->view('qform/step01_view.php'); ?>

            <!-- Step 2 ขั้นตอนที่ 2 -->
            <?php $this->load->view('qform/step02_view.php'); ?>
        </form>

    </div><!-- content -->
    <script src="<?php echo base_Url('assets/plugins/jquery-validation/dist/jquery.validate.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-validation/dist/additional-methods.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/page-script/form_question_custum.js'); ?>" charset="utf-8">
    </script>

    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/PNotify/pnotify.min.css'); ?>">
    <script src="<?php echo base_url('assets/plugins/PNotify/pnotify.min.js'); ?>" charset="utf-8"></script>


    <script type="text/javascript">
        $(document).ready(function () {

            $('#btn_work_cause').click(function (e) {
                e.preventDefault();
                $('#model_preload').modal('show');
                $('#btn_work_cause').attr("disabled", true);

                var url = "<?php echo site_url('website/qform/save_no_workplace'); ?>";
                var param = {
                    work_status: $('input[name="work_status"]:checked').val(),
                    work_cause :$('#work_cause').val(),
                    std_id: '<?php echo $work_data['std_id']; ?>',
                    token_id: '<?php echo $token_id; ?>',
                    citizen_id: '<?php echo $work_data['citizen_id']; ?>',
                };
                $.post(url, param,
                    function (data, textStatus, jqXHR) {
                        if (data.is_success) {

                            window.location.href = "<?php echo site_url('website/qform/success_view'); ?>";

                        }else{
                            $('#btn_work_cause').attr("disabled", false);
                            $('#model_preload').modal('hide');

                            new PNotify({
                                title: 'แจ้งเตือน',
                                text: 'กรุณาตรวจสแบอีกครั้ง',
                                type: 'warning'
                            });
                        }
                    },"json").fail(function(data){
                        $('#btn_work_cause').attr("disabled", false);
                        $('#model_preload').modal('hide');

                        new PNotify({
                            title: 'แจ้งเตือน',
                            text: 'กรุณาตรวจสแบอีกครั้ง',
                            type: 'warning'
                        });

                    });

            });

            $('input[name="work_status"]').change(function (event) {
                if ($(this).val() == 0) {
                    $('#div_cause').fadeIn();
                    $('#q_form').fadeOut();
                    $('#work_cause').focus();
                } else {
                    $('#div_cause').fadeOut();
                    $('#q_form').fadeIn();

                }
            }); //End event

            $('#btn_sendForm').click(function (event) {

                if (form.valid()) {
                    send_form();

                }

            });


            $('.rd_group').find('input[type=radio]').change(function (event) {
                /* Act on the event */
                var tr_el = $(this).parents().eq(1);
                tr_el.css({
                    "color": "#222",
                    "background-color": "#fff"
                });

            });
        });//END READY

        function send_form() {
            $('#model_preload').modal('show');
            $("#btn_sendForm").attr("disabled", true);
            var q_answer = [];
            var ch_data = answer_valid();
            if (ch_data.is_success) {

                q_answer = ch_data.q_answer;

            } else {

                q_answer = [];
                setTimeout(function () {
                    $('#model_preload').modal('hide');
                }, 500);
                $("#btn_sendForm").attr("disabled", false);

                return false;

            }

            var param = {
                work_status: $('input[name="work_status"]:checked').val(),
                organization_name: $('#organization_name').val(),
                organiz_type: $("input[name='organiz_type']:checked").val(),
                organiz_type_other: $("#other_organiz_type").val(),
                director_lev: $("input[name='director_lev']:checked").val(),
                director_lev_other: $("#other_director_lev").val(),
                education_lev: $("input[name='education_lev']:checked").val(),
                std_id: '<?php echo $work_data['std_id']; ?>',
                token_id: '<?php echo $token_id; ?>',
                citizen_id: '<?php echo $work_data['citizen_id']; ?>',
                std_name: $("#std_name").val(),
                std_position: $("#std_position").val(),
                std_major: $("#std_major").val(),
                std_faculty: $("#std_faculty").val(),
                work_year: $("#work_year").val(),
                work_month: $("#work_month").val(),
                q_answer: q_answer,
                comment_note: $('#comment_note').val()
            }

            url = "<?php echo site_url('website/qform/form_save'); ?>";
            $.post(url, param, function (data, textStatus, xhr) {
                // console.log(data);
                if (data.is_success) {

                    window.location.href = "<?php echo site_url('website/qform/success_view'); ?>";

                }
            }, 'json').fail(function(){


                $('#btn_sendForm').attr("disabled", false);


            });


        } //end function

        function answer_valid() {
            var q_answer = [];
            var not_answer = [];
            var anser_group = $('.rd_group');
            $.each(anser_group, function (index, el) {
                var radio_checked = $(el).find('input[type=radio]:checked');
                if (radio_checked.length > 0) {
                    // console.log($(radio_checked).val());
                    q_answer.push({
                        question_list_id: $(radio_checked).attr('list-id'),
                        question_group_id: $(radio_checked).attr('group-id'),
                        answer: $(radio_checked).val(),
                    });
                } else {
                    not_answer.push({
                        question_list: $(el).attr('list')
                    });
                }

            });

            if (not_answer.length > 0) {
                $('html, body').animate({
                    scrollTop: ($('tr[list=' + not_answer[0].question_list + ']').offset().top)
                }, 500);

                $.each(not_answer, function (index, arr) {
                    $('tr[list=' + arr.question_list + ']').css({
                        'color': '#fb6730',
                        'background-color': '#fcf8e3'
                    });
                });
                var notice = new PNotify({
                    title: 'แจ้งเตือน',
                    text: 'โปรดตอบแบบสอบถามให้ครบ',
                    type: 'warning'
                });
                notice.get().click(function () {
                    notice.remove();
                });
                var res = {
                    is_success: false,
                    q_answer: q_answer,
                    not_answer: not_answer,
                }
                return res;

            } else {
                var res = {
                    is_success: true,
                    q_answer: q_answer,
                    not_answer: not_answer,
                }
                return res;
            }
        }
    </script>
</body>

<?php $this->load->view('website/include/footer_view.php'); ?>
