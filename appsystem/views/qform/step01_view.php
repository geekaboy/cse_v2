<div class="row" id="div_step1">
    <div class="col-md-12">
        <div class="card border-info mb-3">
            <div class="card-header bg-primary text-white">
                <h5>ตอนที่ 1 ข้อมูลทั่วไปของผู้ตอบแบบสอบถาม</h5>
            </div>
            <div class="card-body">
                <p class="help-block"><strong><u>คำจี้แจง</u></strong> &nbsp;&nbsp;โปรดเติมข้อความในช่องว่างให้สมบูรณ์ และช่องที่มีเครื่องหมาย <span class="text-danger">* ดอกจัน</span>  จำเป็นต้องกรอกข้อมูล</p>

                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="" class="label-head">1. ชื่อหน่วยงาน/สถานประกอบการ <span class="text-danger">*</span> </label>
                        <input type="text" class="form-control" id="organization_name"
                            name="organization_name"  placeholder="ex. บริษัท เวอร์คพอยท์ เอ็นเตอร์เทนเมนท์ จำกัด(มหาชน)" value="<?php echo $work_data->organiz_name; ?>">
                        <input type="hidden" id="student_workplace_id" name="student_workplace_id" value="<?php echo $work_data->wid; ?>">
                    </div>
                </div>
                <label for="" class="label-head">2. ลักษณะหน่วยงาน <span class="text-danger">*</span></label>
                <div class="form-group">
                    <?php
                    foreach ($org_data as $org) {

                        ?>
                        <div class="form-check form-check-inline mr-2">
                            <input class="form-check-input" type="radio" id="organiz_type" name="organiz_type" value="<?php echo $org->id; ?>" required>
                            <label class="form-check-label"><?php echo $org->title; ?></label>
                        </div>

                        <?php
                    }
                    ?>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="organiz_type" name="organiz_type" value="0">
                        <label class="form-check-label mr-1">อื่นๆ(โปรดระบุ)</label>
                        <div class="form-group m-0">
                            <input type="text" class="form-control" id="organiz_type_other" name="organiz_type_other" placeholder="">
                        </div>
                    </div>

                </div>

                <!--  -->
                <label for="" class="label-head">3. ระดับตำแหน่งของทานในองค์กร <span class="text-danger">*</span></label>
                <div class="form-group">
                    <?php
                    foreach ($director_type as $director) {

                        ?>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="director_type" name="director_type" value="<?php echo $director->id; ?>" required>
                            <label class="form-check-label"><?php echo $director->title; ?></label>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="director_type" name="director_type" value="0">
                        <label class="form-check-label mr-1">อื่นๆ(โปรดระบุ)</label>
                        <div class="form-group m-0">
                            <input type="text" class="form-control" id="director_type_other" name="director_type_other" placeholder="">
                        </div>
                    </div>
                </div>

                <label for="" class="label-head">4. บัณฑิตที่ทำงานอยู่กับท่าน</label>
                <div class="form-row">
                    <div class="form-group col-md-4 mb-2">
                        <label for="">ชื่อ - สกุล <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="std_name"
                            name="std_name" value="<?php echo $work_data->prefix.$work_data->fullname;?>" required>
                        <input type="hidden" id="citizen_id" name="citizen_id" value="<?php echo $work_data->citizen_id; ?>" required>
                        <input type="hidden" id="std_id" name="std_id" value="<?php echo $work_data->std_id; ?>" required>
                        <input type="hidden" id="token_code" name="token_code" value="<?php echo $token_code; ?>" required>

                    </div>
                    <div class="form-group col-md-4 mb-2">
                        <label for="">ตำแหน่ง <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="work_position"
                            name="work_position" value="<?php echo $work_data->work_position; ?>" required>

                    </div>

                </div>

                <div class="form-row">
                    <div class="col-md-4">
                        <div class="form-group mb-2">
                            <label for="">สาขาวิชาเอก <span class="text-danger">*</span> </label>
                            <input type="text" id="maj_name" name="maj_name" class="form-control"
                                value="<?php echo $work_data->maj_name; ?>"  required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group mb-2">
                            <label for="">คณะ <span class="text-danger">*</span></label>
                            <input type="text"  id="fac_name" name="fac_name" class="form-control"
                                value="<?php echo $work_data->fac_name; ?>" required>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">สำเร็จการศึกษาระดับ <span class="text-danger">*</span> </label>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" checked type="radio" name="education_lev" value="1">
                                    <label class="form-check-label">ปริญาตรี</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="education_lev" value="2">
                                    <label class="form-check-label">ปริญาโท</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="education_lev" value="3">
                                    <label class="form-check-label">ปริญาเอก</label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- End card body-->
        </div>
        <!-- End card-->
    </div>

</div>
