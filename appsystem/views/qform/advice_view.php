<div class="row">
  <div class="col-md-12 mt-3">
    <h4>
        <i class="fa fa-commenting" aria-hidden="true"></i> แบบสอบถามประเมินความพึงพอใจที่มีต่อบัณฑิตมหาวิทยาลัยราชภัฏเชียงราย
    </h4>
    <div class="card border-info">
      <div class="card-body" >
        <b>คำชี้แจง</b>
        <p>1. แบบสอบถามฉบับนี้มีวัตถุประสงค์เพื่อสอบถามความพึงพอใจของท่านที่มีต่อผู้ใต้บังคับบัญชาที่สำเร็จการศึกษาจาก มหาวิทยาลัยราชภัฏเชียงราย โดยมีรายละเอียดของแบบสอบถามประกอบด้วย</p>
        <p>&nbsp; &nbsp; ตอนที่ 1 ข้อมูลทั่วไปของผู้ตอบแบบสอบถาม</p>
        <p>&nbsp; &nbsp; ตอนที่ 2 ความพึงพอใจของผู้ใช้บัณฑิตที่มีต่อบัณฑิตมหาวิทยาลัยราชภัฏเชียงรายตามคุณลักษณะของบัณฑิต 5 ด้าน ประกอบด้วย</p>
        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 1) ด้านคุณธรรม จริยธรรม</p>
        <p class="collapse-text">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2) ด้านความรู้</p>
        <p class="collapse-text">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 3) ด้านทักษะทางปัญญา</p>
        <p class="collapse-text">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 4) ด้านทักษะความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบ</p>
        <p class="collapse-text">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 5) ด้านทักษะการวิเคราะห์เชิงตัวเลข การสื่อสาร และการใข้เทคโนโลยีสารสนเทศ</p>
        <p class="collapse-text">&nbsp; &nbsp; ตอนที่ 3 ความคิดเห็นและข้อเสนอแนะต่อการพัฒนาคุณภาพบัณฑิต</p>
        <p class="collapse-text">2. โปรดตอบคำถามทุกข้อตามความเป็นจริง เพื่อประโยชน์ในการพัฒนาคุณภาพบัณฑิตและการจัดการเรียนสอนของมหาวิทยาลัยต่อไป</p>
        <button type="button" class="btn btn-primary float-right" id="btn_showmore"> ย่อหน้าต่าง</button>
        <!-- <button type="button" class="btn btn-outline-primary float-right" id="btn_test"> TEST</button> -->
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#btn_showmore').click(function(event) {
      var has_visible = $('.collapse-text').is(":visible");
      if(has_visible){
        $('.collapse-text').hide();
        $(this).html('อ่านเพิ่มเติม');
      }else{
        $('.collapse-text').show();
        $(this).html('ย่อหน้าต่าง');

      }
    });
  });
</script>
