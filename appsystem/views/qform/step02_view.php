<div class="row" id="div_step2"><!--style="display:none;"-->
    <div class="col-md-12">
        <div class="card border-info mb-3">
            <div class="card-header bg-primary text-white">
                <h5>ตอนที่ 2 ความพึงพอใจของผู้ใช้บัณฑิตมหาวิทยาลัยราชภัฏเชียงรายตามคุณลักษนะบัณฑิต 5 ด้าน</h5>
            </div>
            <div class="card-body">
                <p class="help-block"><strong><u>คำจี้แจง</u></strong>
                    &nbsp;&nbsp;โปรดเลือกระดับความพึงพอใจตามความเป็นจริง โดยมีเกณฑ์ประเมินดังนี้
                    <span class="badge badge-pill bg-success">5</span> = มากที่สุด
                    <span class="badge badge-pill bg-info">4</span> = มาก
                    <span class="badge badge-pill bg-light">3</span> = ปานกลาง
                    <span class="badge badge-pill bg-warning">2</span> = น้อย
                    <span class="badge badge-pill bg-danger">1</span> = ควรปรับปรุง
                </p>
                <div class="row">
                    <div class='table-responsive'>
                        <table class='table table-bordered table-hover'>
                            <thead>
                                <tr>
                                    <th rowspan="2" class="align-middle text-center w-100 pt-1 pb-1">คุณลักษณะ</th>
                                    <th colspan="5" class="text-center pt-1 pb-1">ระดับความคิดเห็น</th>
                                </tr>
                                <tr class="text-center">
                                    <th class="pt-1 pb-1 bg-success">5</th>
                                    <th class="pt-1 pb-1 bg-info">4</th>
                                    <th class="pt-1 pb-1 bg-light">3</th>
                                    <th class="pt-1 pb-1 bg-warning">2</th>
                                    <th class="pt-1 pb-1 bg-danger">1</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i_grow = 0;
                                $i_row = 0;
                                $gid= 0;
                                foreach ($qlist as $question) {
                                    if($gid != $question['qgroup_id']){
                                        $i_row=0;
                                        $i_grow++;
                                        $i_row++;


                                        ?>
                                        <tr class="bg-secondary">
                                            <th colspan="6"><?php echo $i_grow.'. '.$question['qgroup_title']; ?></th>
                                        </tr>
                                        <tr class="rd_group" list="<?php echo $question['qlist_id']; ?>">
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $i_row.') '.$question['qlist_title']; ?></td>
                                            <td class="rbg-success">
                                                <input type="radio" class="answer"  data-list-id="<?php echo $question['qlist_id']; ?>" data-group-id="<?php echo $question['qgroup_id']; ?>" name="<?php echo 'qg'.$question['qgroup_id'].'_ql'.$question['qlist_id']; ?>" value="5" required/>
                                            </td>
                                            <td class="rbg-info">
                                                <input type="radio" class="answer" data-list-id="<?php echo $question['qlist_id']; ?>" data-group-id="<?php echo $question['qgroup_id']; ?>" name="<?php echo 'qg'.$question['qgroup_id'].'_ql'.$question['qlist_id']; ?>" value="4" required/>
                                            </td>
                                            <td class="rbg-light">
                                                <input type="radio" class="answer" data-list-id="<?php echo $question['qlist_id']; ?>" data-group-id="<?php echo $question['qgroup_id']; ?>" name="<?php echo 'qg'.$question['qgroup_id'].'_ql'.$question['qlist_id']; ?>" value="3" required/>
                                            </td>
                                            <td class="rbg-warning">
                                                <input type="radio" class="answer" data-list-id="<?php echo $question['qlist_id']; ?>" data-group-id="<?php echo $question['qgroup_id']; ?>" name="<?php echo 'qg'.$question['qgroup_id'].'_ql'.$question['qlist_id']; ?>" value="2" required/>
                                            </td>
                                            <td class="rbg-danger">
                                                <input type="radio" class="answer" data-list-id="<?php echo $question['qlist_id']; ?>" data-group-id="<?php echo $question['qgroup_id']; ?>" name="<?php echo 'qg'.$question['qgroup_id'].'_ql'.$question['qlist_id']; ?>" value="1" required/>
                                            </td>
                                        </tr>
                                        <?php
                                        $gid = $question['qgroup_id'];
                                    }else{
                                        $i_row++;

                                        ?>

                                        <tr class="rd_group" list="<?php echo $question['qlist_id']; ?>">
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $i_row.') '.$question['qlist_title']; ?></td>
                                            <td class="rbg-success">
                                                <input type="radio" class="answer" data-list-id="<?php echo $question['qlist_id']; ?>" data-group-id="<?php echo $question['qgroup_id']; ?>" name="<?php echo 'qg'.$question['qgroup_id'].'_ql'.$question['qlist_id']; ?>" value="5" required/>
                                            </td>
                                            <td class="rbg-info">
                                                <input type="radio" class="answer" data-list-id="<?php echo $question['qlist_id']; ?>" data-group-id="<?php echo $question['qgroup_id']; ?>" name="<?php echo 'qg'.$question['qgroup_id'].'_ql'.$question['qlist_id']; ?>" value="4" required/>
                                            </td>
                                            <td class="rbg-light">
                                                <input type="radio" class="answer" data-list-id="<?php echo $question['qlist_id']; ?>" data-group-id="<?php echo $question['qgroup_id']; ?>" name="<?php echo 'qg'.$question['qgroup_id'].'_ql'.$question['qlist_id']; ?>" value="3" required/>
                                            </td>
                                            <td class="rbg-warning">
                                                <input type="radio" class="answer" data-list-id="<?php echo $question['qlist_id']; ?>" data-group-id="<?php echo $question['qgroup_id']; ?>" name="<?php echo 'qg'.$question['qgroup_id'].'_ql'.$question['qlist_id']; ?>" value="2" required/>
                                            </td>
                                            <td class="rbg-danger">
                                                <input type="radio" class="answer" data-list-id="<?php echo $question['qlist_id']; ?>" data-group-id="<?php echo $question['qgroup_id']; ?>" name="<?php echo 'qg'.$question['qgroup_id'].'_ql'.$question['qlist_id']; ?>" value="1" required/>
                                            </td>
                                        </tr>

                                        <?php
                                    }
                                }

                                ?>


                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- End card body-->
        </div>
        <!-- End card-->
        <div class="card border-info mb-3">
            <div class="card-header bg-primary text-white">
                <h5>ตอนที่ 3 โปรดแสดงความคิดเห็นเพิ่มเติมเพื่อประโยชน์การปรับปรุงหลักสูตรให้สอดคล้องกับผู้ใช้</h5>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <textarea name="comment_note" id="comment_note" class="form-control" rows="8" cols="80" placeholder="โปรดแสดงความคิดเห็นและข้อเสนอแนะ..."></textarea>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success float-right" id="btn_sendForm">
                            <i class="fa fa-paper-plane" aria-hidden="true"></i>  ส่งแบบสอบถาม
                        </button>
                    </div>
                </div>
            </div>
            <!-- End card body-->
        </div>
        <!-- End card-->
    </div>
</div>
