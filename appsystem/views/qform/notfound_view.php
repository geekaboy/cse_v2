<?php $this->load->view('website/include/header_view.php'); ?>
<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/web_theme/plugins/boostrap/custum_style.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/breadcrumbs/css/style.css'); ?>">
<link href="https://fonts.googleapis.com/css?family=Taviraj" rel="stylesheet">
<!-- Preload  -->
<style media="screen">
body{
  font-family: 'Taviraj', serif;
  letter-spacing: 0.1px;
}
</style>
<script src="<?php echo base_url('assets/plugins/breadcrumbs/js/modernizr.js'); ?>" charset="utf-8"></script>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">CSE SYSTEM</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

  </nav>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-7 text-center pt-5">
        <h1 style="font-size:4rem;" class="mb-3">
          <span class="ti ti-alert text-danger"></span>
        </h1>
        <h3>ไม่พบข้อมูล</h3>
      </div>
    </div>



  </div><!-- content -->
  <script type="text/javascript">

  </script>
</body>

<?php $this->load->view('website/include/footer_view.php'); ?>
