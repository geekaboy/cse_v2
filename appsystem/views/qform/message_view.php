<html lang="th">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <meta name="site_url" content="<?php echo site_url(); ?>">
    <meta name="base_url" content="<?php echo base_url(); ?>">

    <title>CSE: Customer Satisfaction Evaluation</title>
    <!-- Icons-->
    <link href="<?php echo base_url(); ?>assets/theme/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Main styles for this application-->
    <link href="<?php echo base_url(); ?>assets/theme/css/style.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>assets/theme/vendors/jquery/js/jquery.min.js"></script>

    <!--JQueryUI -->
    <script src="<?php echo base_url(); ?>assets/plugins/JQuery/jquery-ui/jquery-ui.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/plugins/JQuery/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
    <style media="screen">
        body {
            font-family: 'Sarabun', serif;
            letter-spacing: 0.1px;
            background-color: #e8e8e8;
        }

        .help-block>.badge {
            font-size: 12px;
        }
        .form-control {
            color: #000;
        }
    </style>
    <script src="<?php echo base_url('assets/plugins/breadcrumbs/js/modernizr.js'); ?>" charset="utf-8"></script>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed">
    <main class="main">

        <div class="container-fluid mt-5">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 style="font-size:4rem;" class="mb-3">

                          <?php
                          if($is_success){
                            echo '<i class="fa fa-check-square-o text-success" aria-hidden="true"></i>';
                          }else{
                            echo '<i class="fa fa-exclamation-triangle text-warning" aria-hidden="true"></i>';
                          }

                          ?>
                        </h1><br>
                        <h3><?php echo $message; ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </main>
