<html lang="th">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <meta name="site_url" content="<?php echo site_url(); ?>">
    <meta name="base_url" content="<?php echo base_url(); ?>">

    <title>CSE: Customer Satisfaction Evaluation</title>
    <!-- Icons-->
    <link href="<?php echo base_url(); ?>assets/theme/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Main styles for this application-->
    <link href="<?php echo base_url(); ?>assets/theme/css/style.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>assets/theme/vendors/jquery/js/jquery.min.js"></script>

    <!--JQueryUI -->
    <script src="<?php echo base_url(); ?>assets/plugins/JQuery/jquery-ui/jquery-ui.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/plugins/JQuery/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
    <style media="screen">
        body {
            font-family: 'Sarabun', serif;
            letter-spacing: 0.1px;
            background-color: #e8e8e8;

        }
        .help-block>.badge {
            font-size: 12px;
        }
        .form-control {
            color: #000;
        }
    </style>
    <script src="<?php echo base_url('assets/plugins/breadcrumbs/js/modernizr.js'); ?>" charset="utf-8"></script>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed">
    <main class="main">

        <div class="container-fluid mt-5">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container">

                            <?php $this->load->view('qform/advice_view.php'); ?>

                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <div class="card border-info">
                                        <div class="card-header bg-primary text-white">
                                            <h5>
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> ข้อมูลบัณฑิต
                                            </h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="col-md-12">
                                                <dl class="row mb-0">
                                                    <dt class="col-sm-2"><strong>ชื่อ - นามสกุล: </strong></dt>
                                                    <dd class="col-sm-9">
                                                        <?php echo $work_data->prefix.$work_data->fullname; ?>
                                                    </dd>

                                                    <dt class="col-sm-2"><strong>รหัสนักศึกษา: </strong></dt>
                                                    <dd class="col-sm-9"><?php echo $work_data->std_id; ?></dd>

                                                    <dt class="col-sm-2"><strong>โปรแกรมวิชา: </strong></dt>
                                                    <dd class="col-sm-9"><?php echo $work_data->maj_name; ?></dd>

                                                    <dt class="col-sm-2"><strong>คณะ/สำนัก: </strong></dt>
                                                    <dd class="col-sm-9"><?php echo $work_data->fac_name; ?></dd>

                                                    <dt class="col-sm-2"><strong>สถานะการทำงาน: </strong></dt>
                                                    <dd class="col-sm-9">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="work_status" checked
                                                                value="1">
                                                            <label class="form-check-label">ปัจุบันทำงานที่นี่</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="work_status" value="0">
                                                            <label class="form-check-label">ไม่ได้ทำงานที่นี่แล้ว</label>
                                                        </div>
                                                        <div class="form-group" id="div_cause" style="display:none;">
                                                            <label for="">
                                                                <i class="ti-comment-alt"></i> ความคิดเห็นหรือข้อเสนอแนะเพื่อพัฒนาคุณภาบัณฑิต
                                                            </label>
                                                            <textarea class="form-control" name="work_cause" id="work_cause" rows="8"
                                                                cols="80" placeholder="โปรดระบุความคิดเห็น..."></textarea>
                                                            <button type="button" class="btn btn-primary float-right mt-3"
                                                                id="btn_work_cause">
                                                                <i class="fa fa-paper-plane" aria-hidden="true"></i> ส่งแบบสอบถาม
                                                            </button>
                                                        </div>

                                                    </dd>
                                                </dl>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <form id="q_form">
                                <!-- Step 1 ขั้นตอนที่ 1  -->
                                <?php $this->load->view('qform/step01_view.php'); ?>

                                <!-- Step 2 ขั้นตอนที่ 2 -->
                                <?php $this->load->view('qform/step02_view.php'); ?>
                            </form>

                        </div><!-- content -->
                    </div>
                </div>
            </div>
        </div>
    </main>
