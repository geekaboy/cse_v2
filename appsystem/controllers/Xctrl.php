<?php
//------------[Controller File name : Web_link.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Xctrl extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index(){

    }
    public function generate(){
        $get = $this->input->get(null, true);
        $user_id = $get['user_id'];
        $checkType = strtoupper($get['check_type']);
        $finger = $this->load->database('finger', true);
        $day = date('d');
        $month = date('m');
        $year = date('Y');

        $sql = "SELECT TOP 1 *
        FROM dbo.CHECKINOUT
        WHERE CHECKTYPE = '{$checkType}'
        	AND DAY(CHECKTIME) = {$day}
        	AND MONTH(CHECKTIME) = {$month}
        	AND YEAR(CHECKTIME) = {$year}
        ORDER BY REC_NO DESC";
        $q = $finger->query($sql);

        if($q->num_rows() > 0){

            $sql = "SELECT TOP 1 *
            FROM dbo.CHECKINOUT
            WHERE USERID = {$user_id}
            	AND CHECKTYPE = '{$checkType}'
            	AND DAY(CHECKTIME) = {$day}
            	AND MONTH(CHECKTIME) = {$month}
            	AND YEAR(CHECKTIME) = {$year}
            ORDER BY REC_NO DESC";
            $q = $finger->query($sql);
            if($q->num_rows() == 0){
                if($checkType == 'I'){
                  $h = 8;
                  $m = rand(15, 28);
                  $s = rand(0,59);  
                } else {
                    $h = 16;
                    $m = rand(35, 40);
                    $s = rand(0,59);  
                }

                $checktime = date('Y-m-d').' '.$h.':'.$m.':'.$s;
                $data_insert = array(
                    'USERID'=>$user_id,
                    'CHECKTIME'=>$checktime,
                    'CHECKTYPE'=> $checkType,
                    'VERIFYCODE'=>'1',
                    'SENSORID'=>20,
                    'WorkCode'=>0,
                    'UserExtFmt'=>0
                );
                $finger->insert('CHECKINOUT', $data_insert);
            }

            // echo "<pre>".date('Y-m-d h:m:i');
            // print_r($q->result());
        }



    }

    public function notify_crru()
    {
        $crru = $this->load->database('crru', true);
        $sql = "SELECT * FROM web_contents c
                WHERE c.is_approve = '0' AND c.is_publish = '1'";
        $q = $crru->query($sql);
        $result = $q->result();
        $num_rows = $q->num_rows();


        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        date_default_timezone_set("Asia/Bangkok");

        $sToken = "4aeHV5Fu3uNkhcSnnVxjF1hRiW0F5vFJcFvhGfs29u6";
        $sMessage = "เนื้อหาที่รอการอนุมัติ ".$num_rows." รายการ ที่ http://www.crru.ac.th/2020/backend/web_contents/approve_list?token=eyJ1c2VyIjoiYWRtaW4iLCJwYXNzIjoiMzEzMzI2MzM5Y3JydTIwMTkifQ==";


        $chOne = curl_init();
        curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $chOne, CURLOPT_POST, 1);
        curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=".$sMessage);
        $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$sToken.'', );
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec( $chOne );

        //Result error
        if(curl_error($chOne))
        {
            echo 'error:' . curl_error($chOne);
        }
        else {
            $result_ = json_decode($result, true);
            echo "status : ".$result_['status']; echo "message : ". $result_['message'];
        }
        curl_close( $chOne );
    }

    public function find_student_number()
    {
        $studentList = [591705180,
        601792077,
        601792093,
        611794001,
        611794003,
        611794006,
        611794007,
        611794009,
        611794010,
        611794011,
        611794014,
        611794015,
        611794016,
        611794018,
        611794019,
        611794022,
        611794023,
        611794024,
        611794025,
        611794026,
        621894001,
        621894003,
        621894004,
        621894007,
        621894008,
        621894009,
        621894011,
        621894013,
        621894014,
        591795052,
        621794001,
        621794002,
        621794003,
        621794004,
        621794005,
        621794006,
        621794008,
        621794009,
        621794010,
        621794011,
        621794012,
        621794013,
        621794014,
        621794017,
        621794019,
        621794020,
        621794021,
        621794022,
        621794024,
        621794025,
        621794026,
        621794027,
        621794028,
        621794029,
        621794030,
        621794031,
        621794032];

        foreach ($studentList as $key => $studentID) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://orasis.crru.ac.th/crru_api/api_class/student/student_login",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_SSL_VERIFYPEER=> false,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "{\"sis_username\":\"{$studentID}\",\"sis_password\":\"crotondev\"}",
              CURLOPT_HTTPHEADER => array(
                "authorization: Basic Y3JydV9hcGlfYWRtaW46MjEyMjI0MjM2",
                "cache-control: no-cache",
                "content-type: application/json",
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
              // redirect('student/login/page_login_failed');

            } else {
              // echo $response;
              $res = json_decode($response);
              //echo ($key+1).'] '.$res->std_id.' -'.$res->full_name.'--> '.$res->mobile.'<br>';
              echo $res->mobile.'<br>';
          }

      }//END FOREACH


    }
}//END CLASS
