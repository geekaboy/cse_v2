<?php
//------------[Controller File name : Web_link.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Address extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    { }

    public function get_amphures()
    {
        $post = $this->input->post(NULL, TRUE);
        $sql = "SELECT * FROM cse_v2.addr_amphures a
                WHERE a.province_id = {$post['province_id']}";
        $qdata = $this->db->query($sql)->result();
        echo json_encode($qdata);
    }

    public function get_districts()
    {
        $post = $this->input->post(NULL, TRUE);
        $sql = "SELECT * FROM cse_v2.addr_districts d
                WHERE d.amphure_id = {$post['amphure_id']}";
        $qdata = $this->db->query($sql)->result();
        echo json_encode($qdata);
    }

}//END CLASS
