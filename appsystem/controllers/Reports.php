<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Amenadiel\JpGraph\Graph;
use Amenadiel\JpGraph\Plot;
class Reports extends CI_Controller
{

	//========================= PUBLIC=========================================//

	public function __construct()
	{
		parent::__construct();
        $this->load->model('student_data_model', 'student');
        $this->load->model('survey_group_model', 'survey_group');
        $this->load->model('survey_report_model', 'survey_report');
        $this->load->model('question_group_model', 'question_group');
        $this->load->model('answer_general_model', 'answer_general');
        $this->load->model('answer_list_model', 'answer_list');

	}

    public function index()
    {
      $this->main_view();
    }

    public function main_view()
    {
      //@Plugin & @Appjs
      $data['plugin'] = array();
      $data['appjs'] = array();

      //@VIEW
      $this->load->view('theme/header', $data);
      $this->load->view('reports/main_view');
      $this->load->view('theme/footer');

  	}

	public function summary()
    {
        //@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/JQuery/jquery-ui/datepicker-th.js',
			'assets/theme/vendors/chart.js/js/Chart.min.js',
			'assets/theme/vendors/chart.js/js/chartjs-plugin-labels.js'
        );
        $data['appjs'] = array(
            'appjs/reports/custom.css',
            'appjs/reports/summary/app.js'
        );

        $data['survey_group'] = $this->survey_group->get_list();
        $data['fac_list'] = $this->student->get_faculty_list();

        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('reports/summary/main_view');
        $this->load->view('theme/footer');
    }

    public function summary_detail()
    {
		$post = $this->input->post(NULL, TRUE);

		$where = '';
		$where .= ($post['fac_name'] != '')?" AND std.fac_name = {$this->db->escape($post['fac_name'])}":'';
		$sql = "SELECT std.fac_name, COUNT(std.*) AS total_std,
					(
						SELECT COUNT(DISTINCT w.citizen_id) AS total_std_work
						FROM cse_v2.student_workplace w
						LEFT JOIN cse_v2.student_data d
							ON w.std_id = d.std_id AND w.citizen_id = d.citizen_id
						WHERE d.year_graduated = std.year_graduated
							AND d.fac_name = std.fac_name
							AND d.date_record BETWEEN {$this->db->escape($post['date_start'])}
								AND {$this->db->escape($post['date_end'])}
					),
					(
						SELECT COUNT(DISTINCT ag.citizen_id) AS total_employer_answer
						FROM cse_v2.answer_general ag
						LEFT JOIN cse_v2.student_workplace w
							ON ag.student_workplace_id = w.id
						LEFT JOIN cse_v2.student_data d
							ON w.std_id = d.std_id AND w.citizen_id = d.citizen_id
						WHERE d.year_graduated = std.year_graduated
							AND d.fac_name = std.fac_name
							AND ag.date_record BETWEEN {$this->db->escape($post['date_start'])}
								AND {$this->db->escape($post['date_end'])}
					)
				FROM cse_v2.student_data std
				WHERE std.year_graduated = '{$post['year']}' {$where}
				GROUP BY std.fac_name, std.year_graduated
				ORDER BY std.fac_name";
		$data['faculty_list'] = $this->db->query($sql)->result();
		$data['year'] = $post['year'];
        $this->load->view('reports/summary/detail_view', $data);
    }

	public function summary_detail_pdf()
	{
		$get = $this->input->get(NULL, TRUE);

		$where = '';
		$where .= ($get['fac_name'] != '')?" AND std.fac_name = {$this->db->escape($get['fac_name'])}":'';
		$sql = "SELECT std.fac_name, COUNT(std.*) AS total_std,
					(
						SELECT COUNT(DISTINCT w.citizen_id) AS total_std_work
						FROM cse_v2.student_workplace w
						LEFT JOIN cse_v2.student_data d
							ON w.std_id = d.std_id AND w.citizen_id = d.citizen_id
						WHERE d.year_graduated = std.year_graduated
							AND d.fac_name = std.fac_name
							AND d.date_record BETWEEN {$this->db->escape($get['date_start'])}
								AND {$this->db->escape($get['date_end'])}
					),
					(
						SELECT COUNT(DISTINCT ag.citizen_id) AS total_employer_answer
						FROM cse_v2.answer_general ag
						LEFT JOIN cse_v2.student_workplace w
							ON ag.student_workplace_id = w.id
						LEFT JOIN cse_v2.student_data d
							ON w.std_id = d.std_id AND w.citizen_id = d.citizen_id
						WHERE d.year_graduated = std.year_graduated
							AND d.fac_name = std.fac_name
							AND ag.date_record BETWEEN {$this->db->escape($get['date_start'])}
								AND {$this->db->escape($get['date_end'])}
					)
				FROM cse_v2.student_data std
				WHERE std.year_graduated = '{$get['year']}' {$where}
				GROUP BY std.fac_name, std.year_graduated
				ORDER BY std.fac_name";
		$data['faculty_list'] = $this->db->query($sql)->result();
		$data['fac_name'] = $get['fac_name'];
		$data['year'] = $get['year'];

		//MPDF
		$mpdf = new \Mpdf\Mpdf();
		//custom font
		$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
		$fontDirs = $defaultConfig['fontDir'];

		$mpdf = new \Mpdf\Mpdf([
		    'fontDir' => array_merge($fontDirs, ['assets/fonts',]),
		    'fontdata' => [
		            'sarabun' => [
		                'R' => 'THSarabunNew.ttf',
		                'I' => 'THSarabunNew Italic.ttf',
		                'B' =>  'THSarabunNew Bold.ttf',
		            ],
		            'fontawesome' => [
		                'R' => 'fontawesome-webfont.ttf',
		            ]
		        ],
		]);
		$mpdf->SetTitle('CSE Customer Satisfaction Evaluation');
		$mpdf->shrink_tables_to_fit = 0;

        $html = $this->load->view('theme/css_include', true);
        $html .= $this->load->view('reports/summary/summary_pdf', $data, true);
		// echo $html;
        $mpdf->WriteHTML($html);
        $mpdf->Output(); // opens in browser

	}

	public function faculty()
    {
        //@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/JQuery/jquery-ui/datepicker-th.js',
			'assets/theme/vendors/chart.js/js/Chart.min.js',
			'assets/theme/vendors/chart.js/js/chartjs-plugin-labels.js'
        );
        $data['appjs'] = array(
            'appjs/reports/custom.css',
            'appjs/reports/faculty/app.js'
        );

		$get = $this->input->get(NULL, TRUE);
        $data['survey_group'] = $this->survey_group->get_list();
        $data['maj_list'] = $this->student->get_major_by_fac($get['fac_name']);
		$data['fac_name'] = $get['fac_name'];
		$data['year'] = $get['year'];

        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('reports/faculty/main_view');
        $this->load->view('theme/footer');
    }

    public function faculty_detail()
    {
		$post = $this->input->post(NULL, TRUE);
		$where = '';
		$where .= ($post['maj_name'] != '')?" AND std.maj_name = {$this->db->escape($post['maj_name'])}":'';
		$where .= ($post['fac_name'] != '')?" AND std.fac_name = {$this->db->escape($post['fac_name'])}":'';
		$sql = "SELECT std.maj_name, COUNT(std.*) AS total_std,
					(
						SELECT COUNT(DISTINCT w.citizen_id) AS total_std_work
						FROM cse_v2.student_workplace w
						LEFT JOIN cse_v2.student_data d
							ON w.std_id = d.std_id AND w.citizen_id = d.citizen_id
						WHERE d.year_graduated = std.year_graduated
							AND d.maj_name = std.maj_name
							AND d.date_record BETWEEN {$this->db->escape($post['date_start'])}
								AND {$this->db->escape($post['date_end'])}
					),
					(
						SELECT COUNT(DISTINCT ag.citizen_id) AS total_employer_answer
						FROM cse_v2.answer_general ag
						LEFT JOIN cse_v2.student_workplace w
							ON ag.student_workplace_id = w.id
						LEFT JOIN cse_v2.student_data d
							ON w.std_id = d.std_id AND w.citizen_id = d.citizen_id
						WHERE d.year_graduated = std.year_graduated
							AND d.maj_name = std.maj_name
							AND ag.date_record BETWEEN {$this->db->escape($post['date_start'])}
								AND {$this->db->escape($post['date_end'])}
					)
				FROM cse_v2.student_data std
				WHERE std.year_graduated = {$this->db->escape($post['year'])} {$where}
				GROUP BY std.maj_name, std.year_graduated
				ORDER BY std.maj_name";
		$data['faculty_list'] = $this->db->query($sql)->result();
        $this->load->view('reports/faculty/detail_view', $data);
    }

	public function faculty_detail_pdf()
    {
		$get = $this->input->get(NULL, TRUE);
		$where = '';

		$where .= ($get['fac_name'] != '')?" AND std.fac_name = {$this->db->escape($get['fac_name'])}":'';
		$sql = "SELECT std.maj_name, COUNT(std.*) AS total_std,
					(
						SELECT COUNT(DISTINCT w.citizen_id) AS total_std_work
						FROM cse_v2.student_workplace w
						LEFT JOIN cse_v2.student_data d
							ON w.std_id = d.std_id AND w.citizen_id = d.citizen_id
						WHERE d.year_graduated = std.year_graduated
							AND d.maj_name = std.maj_name
							AND d.date_record BETWEEN {$this->db->escape($get['date_start'])}
								AND {$this->db->escape($get['date_end'])}
					),
					(
						SELECT COUNT(DISTINCT ag.citizen_id) AS total_employer_answer
						FROM cse_v2.answer_general ag
						LEFT JOIN cse_v2.student_workplace w
							ON ag.student_workplace_id = w.id
						LEFT JOIN cse_v2.student_data d
							ON w.std_id = d.std_id AND w.citizen_id = d.citizen_id
						WHERE d.year_graduated = std.year_graduated
							AND d.maj_name = std.maj_name
							AND ag.date_record BETWEEN {$this->db->escape($get['date_start'])}
								AND {$this->db->escape($get['date_end'])}
					)
				FROM cse_v2.student_data std
				WHERE std.year_graduated = {$this->db->escape($get['year'])} {$where}
				GROUP BY std.maj_name, std.year_graduated
				ORDER BY std.maj_name";
		$data['maj_list'] = $this->db->query($sql)->result();
		$data['fac_name'] = $get['fac_name'];
		$data['year'] = $get['year'];
		//MPDF
		$mpdf = new \Mpdf\Mpdf();
		//custom font
		$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
		$fontDirs = $defaultConfig['fontDir'];

		$mpdf = new \Mpdf\Mpdf([
		    'fontDir' => array_merge($fontDirs, ['assets/fonts',]),
		    'fontdata' => [
		            'sarabun' => [
		                'R' => 'THSarabunNew.ttf',
		                'I' => 'THSarabunNew Italic.ttf',
		                'B' =>  'THSarabunNew Bold.ttf',
		            ],
		            'fontawesome' => [
		                'R' => 'fontawesome-webfont.ttf',
		            ]
		        ],
		]);
		$mpdf->SetTitle('CSE Customer Satisfaction Evaluation');
		$mpdf->shrink_tables_to_fit = 0;

        $html = $this->load->view('theme/css_include', true);
        $html .= $this->load->view('reports/faculty/faculty_detail_pdf', $data, true);
		// echo $html;
        $mpdf->WriteHTML($html);
        $mpdf->Output(); // opens in browser

		// $this->load->view('reports/faculty/detail_view', $data);


    }

	public function overview()
	{
		//@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/JQuery/jquery-ui/datepicker-th.js',
			'assets/theme/vendors/chart.js/js/Chart.min.js',
			'assets/plugins/numeralJS/numeral.min.js',
			// 'assets/plugins/canvasToBlob/canvasjs.min.js'
        );
        $data['appjs'] = array(
            'appjs/reports/custom.css',
            'appjs/reports/overview/app.js'
        );

        $data['survey_group'] = $this->survey_group->get_list();
		$data['fac_list'] = $this->student->get_faculty_list();

        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('reports/overview/main_view');
        $this->load->view('theme/footer');
	}

	public function overview_detail()
	{
		$get = $this->input->get(NULL, TRUE);
		$data['year_graduated'] = $get['year'];
		$data['question_group'] = $this->question_group->get_list();

		$maj_name = ($get['maj_name'] != '')? $get['maj_name']:NULL;
		$fac_name = ($get['fac_name'] != '')? $get['fac_name']:NULL;
		$data['fac_name'] = $fac_name;
		$data['maj_name'] = $maj_name;
		$data['comment_list'] = $this->answer_general->get_comment_note($get['year'], $fac_name, $maj_name);

		$data['get_oragiz'] = $this->survey_report->get_organiz($get['year'], $fac_name, $maj_name);
		$data['director_type'] = $this->survey_report->get_director_type($get['year'], $fac_name, $maj_name);

		$this->load->view('reports/overview/detail_view', $data);

	}

	public function overview_detail_pdf()
	{
		$get = $this->input->get(NULL, TRUE);
		$data['year_graduated'] = $get['year'];
		$data['question_group'] = $this->question_group->get_list();

		$maj_name = ($get['maj_name'] != '')? $get['maj_name']:NULL;
		$fac_name = ($get['fac_name'] != '')? $get['fac_name']:NULL;
		$data['fac_name'] = $fac_name;
		$data['maj_name'] = $maj_name;
		$data['comment_list'] = $this->answer_general->get_comment_note($get['year'], $fac_name, $maj_name);

		$data['get_oragiz'] = $this->survey_report->get_organiz($get['year'], $fac_name, $maj_name);
		$data['director_type'] = $this->survey_report->get_director_type($get['year'], $fac_name, $maj_name);

		// $this->load->view('theme/css_include', $data);
        // $this->load->view('reports/overview/detail_view_pdf');
		// return FALSE;

		$mpdf = new \Mpdf\Mpdf();
		//custom font
		$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
		$fontDirs = $defaultConfig['fontDir'];

		$mpdf = new \Mpdf\Mpdf([
		    'fontDir' => array_merge($fontDirs, ['assets/fonts',]),
		    'fontdata' => [
		            'sarabun' => [
		                'R' => 'THSarabunNew.ttf',
		                'I' => 'THSarabunNew Italic.ttf',
		                'B' =>  'THSarabunNew Bold.ttf',
		            ],
		            'fontawesome' => [
		                'R' => 'fontawesome-webfont.ttf',
		            ]
		        ],
		]);
		$mpdf->SetTitle('CSE Customer Satisfaction Evaluation');
		$mpdf->shrink_tables_to_fit = 0;

        $html = $this->load->view('theme/css_include', true);
        $html .= $this->load->view('reports/overview/detail_view_pdf', $data, true);
		// echo $html;
        $mpdf->WriteHTML($html);
        $mpdf->Output(); // opens in browser


	}

	public function overview_answer_list_score()
	{
		$post = $this->input->post(NULL, TRUE);
		$maj_name = ($post['maj_name'] != '')? $post['maj_name']:NULL;
		$fac_name = ($post['fac_name'] != '')? $post['fac_name']:NULL;

		$answer_list_score = $this->answer_list->get_qlist_score($post['qg_id'], $post['year'], $fac_name, $maj_name);

		echo json_encode($answer_list_score);
	}

	public function gen_radar_graph()
	{
        // print_r($data_set);exit();
		// Create the basic rtadar graph
		$__width  = 800;
		$__height = 400;
		$graph    = new Graph\RadarGraph($__width, $__height);

		// Set background color and shadow
		$graph->SetColor('white');

		// Position the graph
		$graph->SetCenter(0.4, 0.55);

		// Setup the axis formatting
		$graph->axis->SetFont(FF_USERFONT1, FS_NORMAL);
		$graph->axis->SetWeight(2);

		// Setup the grid lines
		$graph->grid->SetLineStyle('longdashed');
		$graph->grid->SetColor('#9f9f9f');
		$graph->grid->Show();
		$graph->HideTickMarks();

		$graph->axis->title->SetFont(FF_USERFONT1, FS_NORMAL, 18);
		$graph->SetTitles(['ด้านคุณธรรม จริยธรรม', 'ด้านความรู้', 'ด้านทักษะทางปัญญา',
		 "ด้านทักษะความสัมพันธ์ระหว่างบุคคล\nและความรับผิดชอบ", "ด้านทักษะการคิดวิเคราะห์เชิงตัวเลข \nการสื่อสาร และการใช้เทคโนโลยี"]);

		// Create the second radar plot
		$plot = new Plot\RadarPlot([4.3, 3.8, 4.6, 4.4, 4.8]);
		$plot->SetColor('#ff8fbe', '#ff8fbe@0.3');

		// Add the plots to the graph
		$graph->Add($plot);

		// And output the graph
		$graph->Stroke();
	}

	public function set_print_chart()
	{
		$post = $this->input->post(NULL, TRUE);
		$data_chart = 'data:image/png;base64,'.str_replace('[removed]','',$post['chart_data']);
		$sess = array('radar_chart'=>$data_chart);
		$this->session->set_userdata($sess);


	}

	public function set_chart_summary()
	{
		$post = $this->input->post(NULL, TRUE);
		$studentChart = 'data:image/png;base64,'.str_replace('[removed]','',$post['studentChart']);
		$employerChart = 'data:image/png;base64,'.str_replace('[removed]','',$post['employerChart']);
		$sess = array('studentChart'=>$studentChart, 'employerChart'=>$employerChart);
		$this->session->set_userdata($sess);
	}

	public function set_chart_faculty()
	{
		$post = $this->input->post(NULL, TRUE);
		$studentChart = 'data:image/png;base64,'.str_replace('[removed]','',$post['studentChart']);
		$employerChart = 'data:image/png;base64,'.str_replace('[removed]','',$post['employerChart']);
		$sess = array('facStudentChart'=>$studentChart, 'facEmployerChart'=>$employerChart);
		$this->session->set_userdata($sess);
	}

}//END CLASS
