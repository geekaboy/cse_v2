<?php
//------------[Controller File name : Web_link.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $sess = $this->session->userdata();

        if(empty($sess['is_login'])){
            redirect('login');
        }
    }

    private  $limit = 30;

    public function index()
    {
        // echo "Access denied";
        $this->list_view();
    }

    public function add_view()
    {
        //@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/sweetalert/sweetalert_all.js',
            'assets/plugins/jquery.serializeJSON/jquery.serializejson.min.js'
        );
        $data['appjs'] = array(
            'appjs/user/add_view.js'
        );


        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('user/add_view.php');
        $this->load->view('theme/footer');
    }

    public function edit_view($username)
    {
        $sql = "SELECT * FROM user WHERE username = '{$username}'";
        $data['user'] = $this->db->query($sql)->row();
        //@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/sweetalert/sweetalert_all.js',
            'assets/plugins/jquery.serializeJSON/jquery.serializejson.min.js'
        );
        $data['appjs'] = array(
            'appjs/user/edit_view.js'
        );


        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('user/edit_view.php');
        $this->load->view('theme/footer');
    }

    public function save()
    {
        $this->load->library('form_validation');

        $frm = $this->form_validation;

        $frm->set_rules('fullname', 'ชื่อผู้ใช้งาน', 'trim|required');
        $frm->set_rules('username', 'USERNAME', 'trim|required');
        $frm->set_rules('password', 'PASSWORD', 'trim|required');
        $frm->set_rules('confirm_password', 'CONFIRM PASSWORD', 'trim|required');
        $frm->set_message('required', 'กรุณากรอก %s');

        if ($frm->run() == FALSE) {
            $message = '';
            $message .= form_error('username');
            $message .= form_error('password');
            $message .= form_error('confirm_password');

            echo json_encode(array(
                'is_successful' => FALSE,
                'message' => $message
            ));
        } else {

            $data_post = $this->input->post();

            if ($data_post['password'] != $data_post['confirm_password']) {
                echo json_encode(array(
                    'is_successful' => FALSE,
                    'message' => 'รหัสผ่านไม่ตรงกัน กรุณาตรวจสอบใหม่อีกครั้ง!'
                ));
                exit();
            }

            $sql = "SELECT * FROM user WHERE username = '{$data_post['username']}'";
            $user = $this->db->query($sql);
            if ($user->num_rows() > 0) {
                echo json_encode(array(
                    'is_successful' => FALSE,
                    'message' => 'ผู้ใช้งานระบบ '.$data_post['username'] . ' มีอยู่ในระบบแล้ว'
                ));
                exit();
            }

            $data = array(
                'fullname' => $data_post['fullname'],
                'username' => $data_post['username'],
                'password' => md5($data_post['password']),
            );

            if ($this->db->insert('user', $data)) {
                echo json_encode(array(
                    'is_successful' => TRUE,
                    'message' => 'บันทึกเรียบร้อย'
                ));
            } else {
                echo json_encode(array(
                    'is_successful' => FALSE,
                    'message' => 'ผิดพลาด'
                ));
            }
        } //End if
    }

    public function update()
    {
        $this->load->library('form_validation');

        $frm = $this->form_validation;

        $frm->set_rules('fullname', 'ชื่อ-นามสกุล', 'trim|required');
        $frm->set_rules('username', 'USERNAME', 'trim|required');
        $frm->set_rules('password', 'PASSWORD', 'trim|required');
        $frm->set_rules('confirm_password', 'CONFIRM PASSWORD', 'trim|required');
        $frm->set_message('required', 'กรุณากรอก %s');

        if ($frm->run() == FALSE) {
            $message = '';
            $message .= form_error('fullname');
            $message .= form_error('username');
            $message .= form_error('password');
            $message .= form_error('confirm_password');

            echo json_encode(array(
                'is_successful' => FALSE,
                'message' => $message
            ));
        } else {

            $data_post = $this->input->post();

            if ($data_post['password'] != $data_post['confirm_password']) {
                echo json_encode(array(
                    'is_successful' => FALSE,
                    'message' => 'รหัสผ่านไม่ตรงกัน กรุณาตรวจสอบใหม่อีกครั้ง!'
                ));
                exit();
            }

            $sql = "SELECT * FROM user WHERE username = '{$data_post['username']}'";
            $user = $this->db->query($sql);
            if ($user->num_rows() == 0) {
                echo json_encode(array(
                    'is_successful' => FALSE,
                    'message' => 'ไม่พบผู้ใช้งาน'
                ));
                exit();
            }
            $cond = array(
                'username' => $data_post['username']
            );
            $data = array(
                'fullname' => $data_post['fullname'],
                'password' => md5($data_post['password']),
            );

            if ($this->db->update('user', $data, $cond)) {
                echo json_encode(array(
                    'is_successful' => TRUE,
                    'message' => 'บันทึกเรียบร้อย'
                ));
            } else {
                echo json_encode(array(
                    'is_successful' => FALSE,
                    'message' => 'ผิดพลาด'
                ));
            }
        } //End if
    }

    public function del()
    {
        $data_post = $this->input->post();
        $sql = "SELECT * FROM user WHERE username = '{$data_post['username']}'";
        $user = $this->db->query($sql);
        if ($user->num_rows() > 0) {
            $cond = array(
                'username' => $data_post['username']
            );

            if ($this->db->delete('user', $cond)) {
                echo json_encode(array(
                    'is_successful' => TRUE,
                    'message' => 'ลบเรียบร้อย'
                ));
            }
        } else {
            echo json_encode(array(
                'is_successful' => FALSE,
                'message' => 'ไม่พบผู้ใช้งาน'
            ));
            exit();
        }
    }

    public function list_view()
    {
        $sql = "SELECT *
            FROM user u
            ORDER BY u.username, u.fullname";
        $data['user_list'] = $this->db->query($sql)->result();

        //@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/sweetalert/sweetalert_all.js',
        );
        $data['appjs'] = array(
            'appjs/user/list_view.js',
        );

        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('user/list_view');
        $this->load->view('theme/footer');
    }

}//end class
