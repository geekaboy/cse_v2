<?php
//------------[Controller File name : Web_link.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Generate extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $sess = $this->session->userdata();
        if (empty($sess['is_login'])) {
            redirect('login');
        }
    }

    public function index()
    { }

    public function import_student_workplace()
    {
        // echo "Warning!! ...import_student_workplace";
        // exit();
        $this->load->model('Student_data_model', 'student');

        $sql = "SELECT std_d.* FROM cse.work_students_detail std_d
            LEFT JOIN cse.work_students std
                ON std_d.std_id = std.std_id
                    AND std_d.citizen_id = std.citizen_id
            WHERE std_d.std_id IS NOT null
            ";
        $workplace_list = $this->db->query($sql)->result();

        foreach ($workplace_list as $key => $workplace) {
            $regex = '/[ก-๙\ ]/';
            $data_eng = preg_replace($regex, '', $workplace->director_email);
            $data_expert = str_replace(' ', '', mb_strimwidth($data_eng, 0, 250, "", "UTF-8"));
            //$data_expert= fn_filter_url(mb_strimwidth(fn_undat($data_eng),0,250,"", "UTF-8"));
            $email_str = strtolower($data_expert);

            // $std_data = $this->student->get_detail_by_std_id($workplace->std_id);

            $insert_data = array(
                'std_id' => $workplace->std_id,
                'citizen_id' => $workplace->citizen_id,
                'work_position' => $workplace->work_position,
                'director_email' => $email_str,
                'director_name' => $workplace->director_name,
                'organiz_name' => $workplace->organiz_name,
                'address' => $workplace->address,
                'tambon' => $workplace->tambon,
                'amphures' => $workplace->amphures,
                'province' => $workplace->province,
                'nation' => $workplace->nation,
                'zipcode' => $workplace->zipcode,
                'tel' => $workplace->tel,
                'is_current' => $workplace->is_current,
                'work_time_range_id' => $workplace->work_time_range_id,
                'salary' => $workplace->salary,
                'work_match' => $workplace->work_match,
                'note' => 'Import form old version'
            );

            $this->db->insert('cse_v2.student_workplace', $insert_data);
            echo $workplace->std_id . '<br>';
        }

        echo "Success";
    }

    public function import_student_workplace_orasis()
    {
        // echo "Warning!! ...import_student_workplace";
        // exit();
        $orasis = $this->load->database('orasis', true);

        $this->load->model('Student_data_model', 'student');

        $sql = "SELECT * FROM cse_v2.student_data s
                WHERE year_graduated = '2561'
            ";
        $student_list = $this->db->query($sql)->result();
        $i_row = 0;
        foreach ($student_list as $key => $student) {
            $sql = "SELECT * FROM cse_v2.student_workplace
                    WHERE std_id = '{$student->std_id}'
                    AND citizen_id = '{$student->citizen_id}'";
            $q = $this->db->query($sql);
            if($q->num_rows() == 0){
                $sql = "SELECT pundit.*
                        FROM pundit.view_student_workplace pundit
                        WHERE qn_work_email <> ''
                    		AND qn_work_email <> '-'
                    		AND qn_work_email IS NOT null
                    		AND qn_work_email LIKE '%_@__%.__%'
                            AND std_id = '{$student->std_id}'
                            AND citizen_id = '{$student->citizen_id}'";
                $workplace = $orasis->query($sql)->row();

                if(!is_null($workplace)){
                    $i_row++;
                    $regex = '/[ก-๙\ ]/';
                    $data_eng = preg_replace($regex, '', $workplace->qn_work_email);
                    $data_expert = str_replace(' ', '', mb_strimwidth($data_eng, 0, 250, "", "UTF-8"));
                    //$data_expert= fn_filter_url(mb_strimwidth(fn_undat($data_eng),0,250,"", "UTF-8"));
                    $email_str = strtolower($data_expert);

                    // $std_data = $this->student->get_detail_by_std_id($workplace->std_id);
                    $salary = str_replace( ',', '', $workplace->qn_salary);

                    $insert_data = array(
                        'std_id' => $workplace->std_id,
                        'citizen_id' => $workplace->citizen_id,
                        'work_position' => 'Unknow',
                        'director_email' => $email_str,
                        'director_name' => $workplace->ext_manager_name,
                        'organiz_name' => $workplace->qn_work_name,
                        'address' => strval($workplace->qn_work_add),
                        'tambon' => $workplace->qn_work_tambon,
                        'amphures' => $workplace->qn_work_amphur,
                        'province' => $workplace->province_name_th,
                        'nation' => 'ไทย',
                        'zipcode' => $workplace->qn_work_zipcode,
                        'tel' => $workplace->qn_work_tel,
                        'is_current' => '1',
                        'work_time_range_id' => 1,
                        'salary' => intval($salary),
                        'work_match' => '1',
                        'is_answer'=>'1',
                        'note' => 'Import from Orasis'
                    );
                    $this->db->insert('cse_v2.student_workplace', $insert_data);

                    $update_data = array(
                        'work_status'=>'1',
                        'is_answer'=>'1'
                    );
                    $cond = array('std_id'=>$workplace->std_id, 'citizen_id'=>$workplace->citizen_id);
                    $this->db->update('cse_v2.student_data', $update_data, $cond);
                    // echo "<pre>";
                    // print_r($insert_data);
                }

            }//END IF


        }//END FOREACH

        echo "Success => ".$i_row;
    }

    public function update_citizen_id()
    {
        echo "Warning!! ...update_citizen_id";
        exit();
        $sql = "SELECT * FROM cse_v2.student_data d
                WHERE d.std_id IN (
                SELECT std_id FROM cse_v2.student_workplace
                )";
        $qdata = $this->db->query($sql)->result();
        foreach ($qdata as $key => $std) {

            $data_update = array(
                'citizen_id' => $std->citizen_id
            );
            $cond = array(
                'std_id' => $std->std_id
            );
            $this->db->update('cse_v2.student_workplace', $data_update, $cond);
            echo $std->std_id . '<br>';
        }
        echo "Success";
    }

    public function import_student_data()
    {
        // echo "Warning!! ...import_student_data";
        // exit();
        $orasis = $this->load->database('orasis', true);
        $sql = "SELECT *
            FROM public.view_cse_pundit_student
            WHERE year_approve_finish = '2561'
                AND pundit_email <> ''
                AND pundit_email <> '-'
                AND pundit_email IS NOT null
                AND pundit_email LIKE '%_@__%.__%'
            ";

        $student_list = $orasis->query($sql)->result();

        foreach ($student_list as $key => $std) {
            $sql = "SELECT * FROM cse_v2.student_data s
                    WHERE s.std_id = '{$std->std_id}'
                        AND s.citizen_id = '{$std->citizen_id}'";
            $q = $this->db->query($sql);
            if($q->num_rows() == 0){
                $regex = '/[ก-๙\ ]/';
                $data_eng = preg_replace($regex, '', $std->pundit_email);
                $data_expert = str_replace(' ', '', mb_strimwidth($data_eng, 0, 250, "", "UTF-8"));
                //$data_expert= fn_filter_url(mb_strimwidth(fn_undat($data_eng),0,250,"", "UTF-8"));
                $email_str = strtolower($data_expert);
                $data  = array(
                    'std_id' => $std->std_id,
                    'citizen_id' => $std->citizen_id,
                    'date_graduated' => $std->date_approve_1,
                    'year_graduated' => $std->year_approve_finish,
                    'prefix' => $std->pname_th,
                    'fullname' => $std->std_fname_th . ' ' . $std->std_lname_th,
                    'gender' => $std->gender_id,
                    'maj_name' => $std->maj_name_th,
                    'fac_name' => $std->fac_name_th,
                    'mobile_number' => $std->pundit_mobile,
                    'email' => $email_str,
                    'degree_num' => $std->gpa,
                    'lev_name_th' => $std->lev_name_th

                );
                $this->db->insert('cse_v2.student_data', $data);
                echo $std->std_id . '<br>';
            }//END IF

        }

        echo "Success";
    }

    public function import_send_survey()
    {
        echo "Warning!! ...import_send_survey";
        exit();
        $sql = "SELECT * FROM cse_v2.student_workplace w
            LEFT JOIN cse_v2.student_data std
              ON w.std_id = std.std_id AND w.citizen_id = std.citizen_id";
        $qdata = $this->db->query($sql)->result();
        $i_row = 0;
        foreach ($qdata as $key => $row) {
            $sql = "SELECT *
              FROM cse.email_send_log esl
              WHERE esl.type = '2'
                 AND esl.is_answer = '1'
                 AND esl.std_id = '{$row->std_id}'";
            $rdata = $this->db->query($sql)->row();
            if (count($rdata) > 0) {

                $data_insert = array(
                    'std_id' => $row->std_id,
                    'citizen_id' => $row->citizen_id,
                    'std_email' => $rdata->std_email,
                    'director_email' => $rdata->director_email,
                    'survey_type' => '2',
                    'token_code' => $rdata->token_id,
                    'is_read' => '1',
                    'is_answer' => '1',
                    'answer_time' => $rdata->answer_time
                );
                $this->db->insert('cse_v2.send_survey', $data_insert);
                $i_row++;
                echo $row->std_id . '<br/>';
            }
        }

        echo 'Success';
    }

    public function import_answer()
    {
        $sql = "SELECT * FROM cse_v2.send_survey s";
        $rlist = $this->db->query($sql)->result();

        foreach ($rlist as $key => $row) {
            $sql = "SELECT *
                    FROM cse.question_gen g
                    WHERE g.std_id = '{$row->std_id}'
                        AND g.citizen_id = '{$row->citizen_id}'";
            $rdata = $this->db->query($sql)->row();
            $cond = array(
                'std_id'=>$row->std_id,
                'citizen_id'=>$row->citizen_id
            );
            if(count($rdata) > 0){
                // print_r($rdata);
                $work_status = '1';
                 //Insert answer_general
                $data_insert = array(
                    'std_id'=>$row->std_id,
                    'citizen_id'=>$row->citizen_id,
                    'org_name'=>$rdata->organization_name,
                    'org_type_id'=>$rdata->organization_type_id,
                    'org_type_other'=>$rdata->organization_type_other,
                    'director_type_id'=>$rdata->director_lev_type_id,
                    'director_type_other'=>$rdata->director_lev_type_other,
                    'comment_note'=>$rdata->comment_note,
                    'is_status'=>'1',
                    'work_status'=>$work_status,
                    'from_token_code'=>$row->token_code,
                );
            }else{
                // echo "Empty!!! ".$row->std_id."<br/>";
                $work_status = '0';
                 //Insert answer_general
                $data_insert = array(
                    'std_id'=>$row->std_id,
                    'citizen_id'=>$row->citizen_id,
                    'org_name'=>'',
                    'org_type_id'=>0,
                    'org_type_other'=>'',
                    'director_type_id'=>0,
                    'director_type_other'=>'',
                    'comment_note'=>'',
                    'is_status'=>'1',
                    'work_status'=>$work_status,
                    'from_token_code'=>$row->token_code,
                );

                //update student_data
                $this->db->update('cse_v2.student_data', array('work_status'=>$work_status), $cond);
            }
            $this->db->insert('cse_v2.answer_general', $data_insert);

            //update student_workplace
            $this->db->update('cse_v2.student_workplace', array('is_answer'=>'1'), $cond);
            // if($row->std_id == '551132001') print_r($data_insert);

            $ins_id_sql='SELECT LASTVAL() as ins_id';
            $qdata= $this->db->query($ins_id_sql)->row();
            $answer_general_id = $qdata->ins_id;

            if($work_status == '1'){
                //Insert answer_list
                $sql = "SELECT *
                FROM cse.question_answer a
                WHERE a.question_gen_id = '{$rdata->id}'";
                $xdata = $this->db->query($sql)->result();

                if(count($xdata) > 0){
                    foreach ($xdata as $key => $answer) {

                        $qlist = $this->db->get_where('cse_v2.question_list', array('old_id'=>$answer->question_list_id))->row();
                        // print_r($row);
                        $data_insert = array(
                            'std_id'=>$row->std_id,
                            'citizen_id'=>$row->citizen_id,
                            'question_list_id'=>$qlist->id,
                            'question_group_id'=>$answer->question_group_id,
                            'answer_general_id'=>$answer_general_id,
                            // 'answer_general_id'=>'',
                            'answer_value'=>$answer->answer_value,
                        );


                        $this->db->insert('cse_v2.answer_list', $data_insert);
                    }

                }else{
                    $this->db->update('cse_v2.answer_general',
                        array('work_status'=>'0'),
                        array('id'=>$answer_general_id));
                }
            }

            echo $row->std_id.'<br/>';

         }

         echo 'success';
    }

    public function import_question_list()
    {
        $sql = "SELECT * FROM cse.question_list l ORDER BY question_group_id";
        $list = $this->db->query($sql)->result();
        $order_num = 0;
        foreach ($list as $key => $row) {
            // title character varying(255) COLLATE pg_catalog."default",
            // question_group_id integer,
            // curriculum_id character varying COLLATE pg_catalog."default",
            // is_status character varying(1) COLLATE pg_catalog."default" NOT NULL DEFAULT 1,
            // order_num integer,
            // old_id integer,
            $order_num++;
            $data_insert = array(
                'title'=>$row->question_name,
                'question_group_id'=>$row->question_group_id,
                'order_num'=>$order_num,
                'old_id'=>$row->id
            );
            $this->db->insert('cse_v2.question_list', $data_insert);

            if($order_num == 5) $order_num=0;
        }

    }

    public function add_workplace_id_to_send_survey()
    {
        echo "Warning!! ...add_workplace_id_to_send_survey";
        exit();
        $sql = "SELECT * FROM cse_v2.send_survey s";
        $list = $this->db->query($sql)->result();
        foreach ($list as $key => $row) {
            $sql = "SELECT * FROM cse_v2.student_workplace w
                    WHERE w.std_id = '{$row->std_id}'
                        AND w.citizen_id = '{$row->citizen_id}' ";
            $workplace = $this->db->query($sql)->row();

            $data_update = array(
                'student_workplace_id'=>$workplace->id
            );
            $cond = array(
                'id'=>$row->id
            );
            $this->db->update('cse_v2.send_survey', $data_update, $cond);
            // code...
        }
    }

    public function update_answer_general()
    {
        echo "Warning!! ...update_answer_general";
        exit();
        $sql = "SELECT * FROM cse_v2.answer_general ag";
        $list = $this->db->query($sql)->result();
        foreach ($list as $key => $row) {
            $sql = "SELECT * FROM cse_v2.student_workplace w
                    WHERE w.std_id = '{$row->std_id}'
                        AND w.citizen_id = '{$row->citizen_id}' ";
            $workplace = $this->db->query($sql)->row();
            if(count($workplace) > 0){
                $data_update = array(
                    'student_workplace_id'=>$workplace->id
                );
                $cond = array(
                    'id'=>$row->id
                );
                $this->db->update('cse_v2.answer_general', $data_update, $cond);
            }

            // code...
        }
    }
}//End class
