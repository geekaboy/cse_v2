<?php
//------------[Controller File name : Web_link.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');


class Send_survey extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $sess = $this->session->userdata();
        if(empty($sess['is_login'])){
            redirect('login');
        }

        $this->load->model('student_data_model', 'student');
        $this->load->model('student_workplace_model', 'workplace');
        $this->load->model('email_list_model', 'email_list');
        $this->load->model('survey_group_model', 'survey_group');


    }
    public function index()
    {

    }

    public function employer_survey_view()
    {
        //@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/icheck/skins/minimal/green.css',
            'assets/plugins/icheck/icheck.min.js',
            'assets/plugins/sweetalert/sweetalert_all.js'
        );
        $data['appjs'] = array(
            'appjs/send_survey/employer_survey.js'
        );

        $data['group_list'] = $this->survey_group->get_list();


        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('send_survey/employer_survey_view');
        $this->load->view('theme/footer');
    }

    public function get_employer_list()
    {
        $data_get = $this->input->get();

        $where = "";
        if($data_get['search_text']!= ''){
            $where .= " AND (
                std.maj_name LIKE '%{$data_get['search_text']}%'
                OR std.fac_name LIKE '%{$data_get['search_text']}%'
                OR w.organiz_name LIKE '%{$data_get['search_text']}%'
                OR std.std_id LIKE '%{$data_get['search_text']}%'
            )";
        }
        $sql = "SELECT COUNT(w.std_id) AS total_row
        FROM cse_v2.student_workplace w
        LEFT JOIN cse_v2.student_data std
            ON w.std_id = std.std_id AND w.citizen_id = std.citizen_id
        WHERE std.year_graduated = '{$data_get['year_graduated']}'
            AND w.is_answer = '{$data_get['is_answer']}' $where";

        $q = $this->db->query($sql)->row();
        $total_row = $q->total_row;
        $page = (isset($data_get['page']))?$data_get['page']:1;

        $data['total_row'] = $total_row;
        $data['search_text'] = $data_get['search_text'];
        $data['year_graduated'] = $data_get['year_graduated'];

        $this->load->helper('pagination');
        $config['base_url'] = site_url('send_survey/get_employer_list');
        $config['total_row'] = $total_row;
        $config['per_page'] = 100;
        gen_pagination($config);

        $limit = $config['per_page'];
        $start = ($page - 1) * $limit;
        $data['start'] = $start;

        $sql = "SELECT w.*, std.maj_name, std.fac_name
        FROM cse_v2.student_workplace w
        LEFT JOIN cse_v2.student_data std
            ON w.std_id = std.std_id AND w.citizen_id = std.citizen_id
        WHERE std.year_graduated = '{$data_get['year_graduated']}'
            AND w.is_answer = '{$data_get['is_answer']}' $where
        ORDER BY w.count_survey, w.is_answer, std.maj_name, std.fac_name
        LIMIT $limit OFFSET $start";

        $data['employer_list'] = $this->db->query($sql)->result();
        $data['email_list'] = $this->email_list->get_list();

        $this->load->view('send_survey/employer_list_view', $data);

    }


    public function student_survey_view()
    {
        //@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/icheck/skins/minimal/green.css',
            'assets/plugins/icheck/icheck.min.js',
            'assets/plugins/sweetalert/sweetalert_all.js'
        );
        $data['appjs'] = array(
            'appjs/send_survey/student_survey.js'
        );

        $data['group_list'] = $this->survey_group->get_list();

        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('send_survey/student_survey_view');
        $this->load->view('theme/footer');
    }

    public function get_student_list()
    {
        $data_get = $this->input->get();

        $where = "";
        if($data_get['search_text']!= ''){
            $where .= " AND (
                s.maj_name LIKE '%{$data_get['search_text']}%'
                OR s.fac_name LIKE '%{$data_get['search_text']}%'
                OR s.std_id LIKE '%{$data_get['search_text']}%'
            )";
        }
        if($data_get['is_answer']!= ''){
            $where .= " AND s.is_answer = {$this->db->escape($data_get['is_answer'])}";
        }


        $sql = "SELECT COUNT(DISTINCT s.citizen_id) AS total_row
                FROM cse_v2.student_data s
                LEFT JOIN cse_v2.student_workplace w
                	ON s.std_id = w.std_id
                		AND s.citizen_id = w.citizen_id
                		AND w.is_current = '0'
                WHERE s.year_graduated = {$this->db->escape($data_get['year_graduated'])}
                    $where";

        $q = $this->db->query($sql)->row();
        $total_row = $q->total_row;
        $page = (isset($data_get['page']))?$data_get['page']:1;

        $data['total_row'] = $total_row;
        $data['search_text'] = $data_get['search_text'];
        $data['year_graduated'] = $data_get['year_graduated'];

        $this->load->helper('pagination');
        $config['base_url'] = site_url('send_survey/get_student_list');
        $config['total_row'] = $total_row;
        $config['per_page'] = 100;
        gen_pagination($config);

        $limit = $config['per_page'];
        $start = ($page - 1) * $limit;
        $data['start'] = $start;

        $sql = "SELECT DISTINCT s.citizen_id, s.id, s.std_id, s.prefix, s.fullname, s.email,
                        s.maj_name, s.fac_name, w.is_current, s.count_survey, s.is_answer
                FROM cse_v2.student_data s
                LEFT JOIN cse_v2.student_workplace w
                	ON s.std_id = w.std_id
                		AND s.citizen_id = w.citizen_id
                		AND w.is_current = '0'
                WHERE s.year_graduated = {$this->db->escape($data_get['year_graduated'])}
                    $where
                ORDER BY s.count_survey DESC, s.is_answer, w.is_current, s.fullname
                LIMIT $limit OFFSET $start";

        $data['student_list'] = $this->db->query($sql)->result();
        $data['email_list'] = $this->email_list->get_list();

        $this->load->view('send_survey/student_list_view', $data);

    }

    public function send_employer()
    {
        $post = $this->input->post(NULL, TRUE);

        $success_email_arr = array();
        $fail_email_arr = array();
        foreach ($post['list'] as $row) {

            $workplace = $this->workplace->get_detail($row['id']);
            $cse_email = $this->email_list->get_detail($post['cse_email']);

            $token_code = md5(date('Y-m-d H:i:s') . $row['std_id']);
            $is_success = $this->send_email($workplace, $token_code, $cse_email);

            //CHECK SUCCESS
            if ($is_success) {

                array_push($success_email_arr, $row);

            } else {
                array_push($fail_email_arr, $row);

                $cond = array(
                    'std_id'=>$row['std_id'],
                    'citizen_id'=>$workplace->citizen_id
                );
                $this->db->update('cse_v2.student_workplace', array('email_status'=>'false'), $cond);

            }//end if

            //===== INSERT SEND_SURVEY TRANSACTION ====//
            $data = array(
                'std_id' => $row['std_id'],
                'citizen_id' => $row['citizen_id'],
                'std_email' => '',
                'director_email' => $row['email'],
                'survey_type' => '2',
                'token_code' => $token_code,
                'student_workplace_id'=>$workplace->id
            );
            $this->db->insert('cse_v2.send_survey', $data);

            //====== UPDATE STUDENT_WORKPLACE =======//
            $data_update = array(
                'count_survey'=>intval($workplace->count_survey)+1
            );
            $cond = array(
                'std_id'=>$row['std_id'],
                'citizen_id'=>$row['citizen_id']
            );
            $this->db->update('cse_v2.student_workplace', $data_update, $cond);

        }//end foreach

        echo json_encode(array(
            'success_list' => $success_email_arr,
            'fail_list' => $fail_email_arr
        ));

    }

    public function send_student()
    {
        $post = $this->input->post(NULL, TRUE);

        $success_email_arr = array();
        $fail_email_arr = array();
        foreach ($post['list'] as $row) {

            $student = $this->student->get_detail($row['std_id'], $row['citizen_id']);
            $cse_email = $this->email_list->get_detail($post['cse_email']);

            $token_code = md5(date('Y-m-d H:i:s') . $row['std_id']);

            $is_success = $this->send_email_std($student, $token_code, $cse_email);

            //CHECK SUCCESS
            $email_status = 'true';
            if ($is_success) {

                array_push($success_email_arr, $row);

            } else {
                array_push($fail_email_arr, $row);
                $email_status = 'false';

            }//end if

            //===== INSERT SEND_SURVEY TRANSACTION ====//
            $data = array(
                'std_id' => $row['std_id'],
                'citizen_id' => $row['citizen_id'],
                'std_email' => $row['email'],
                'director_email' => '',
                'survey_type' => '1',
                'token_code' => $token_code
            );
            $this->db->insert('cse_v2.send_survey', $data);

            //====== UPDATE STUDENT_data =======//
            $data_update = array(
                'count_survey'=>intval($student->count_survey)+1,
                'email_status'=>$email_status
            );
            $cond = array(
                'std_id'=>$row['std_id'],
                'citizen_id'=>$row['citizen_id']
            );
            $this->db->update('cse_v2.student_data', $data_update, $cond);

        }//end foreach

        echo json_encode(array(
            'success_list' => $success_email_arr,
            'fail_list' => $fail_email_arr
        ));

    }


    //====================== PRIVATE FUNCTION =================================//
    private function send_email($workplace, $token_code, $cse_email)
    {

        $this->load->library('email');
        $post = $this->input->post(NULL, TRUE);
        $std_data = $this->student->get_detail($workplace->std_id, $workplace->citizen_id);

        // Get full html:
        $body =
            '<!DOCTYPE html>
			<html>
			<head>
			<meta charset="utf-8">
			<title>ขอความอนุเคราะห์ตอบแบบสอบถามสำรวจความพึงพอใจผู้ใช้บัณฑิต</title>
			<style media="screen">
			p{
				font-size: 12px;
			}
			</style>
			</head>
			<body>
			<div class="">
			<img src="https://lh3.googleusercontent.com/zY3Irqj4NEEhFr2R0MGIOBjeUyMKbzaoQAdKzD69STlNgYyCjW-ipFToNSj8CUqVKhqNhfbUF5igpaZm3tlDFFdrHUe6Y01k2LWSy7EnuuzTknnl_U5yrefrZZhVIZZHa115qtdkGkaFVY1yeNxpjhPQyKTP6aYJ3lIizoWfFWZuWyX7Zw1iWUQNheRw5a6ElZyzgcUNHwIhcH9gEI4bew9CauiA5r_deehA_meIzlkLtXH_jQPCCkrCxhg3jgpU07sHjcAOwVEei41OoZbgddtNx0MwhkSaJlf98e4XhFZoFAiDt46lG2fmQBx6umiQVBuvmn-jx3DE4LV0NvTzNLOJxY2cLO3nrMHD8m662xFj8QV-ES11qJlQAf7r6biGPsrDbhfqqC3Hvrn9hOb68Tb8RWeXBiLwQOxcDo4iwEnpborqLd4NVbB6qgGGNbwh6SMnUixq_vNcsTjcNVSJE6od3GrWImThAVy88gJ9vtb0NLq3BbcHp9-wGLOYwR_JMUNoywGvb6qte3qjIwNiUfVGrdsTndvY3PlVaQw-SaiT4tWAuIXGk62X8LkCDmPcMzxbWYYz6pw8aJJPR5MlhAXxZkPLws81GkK3bsc=s300-no" width="100" style="margin:0 auto;display: block;">
			</div>
			<p style="text-align:right;margin:0 auto;">
			มหาวิทยาลัยราชภัฏเชียงราย<br />
			ถนนพหลโยธิน  อำเภอเมือง<br />
			จังหวัดเชียงราย ๕๗๑๐๐<br />
			</p>
			<p style="text-align:center">' . thainumDigit(do_full_thai_date_style_1(date('Y-m-d'))) . '</p>
			<p><b>เรื่อง</b> ขอความอนุเคราะห์ตอบแบบสอบถามเพื่อสำรวจความพึงพอใจผู้ใช้บัณฑิตมหาวิทยาลัยราชภัฏเชียงราย</p>
			<p><b>เรียน</b> ผู้อำนวยการ/หัวหน้าฝ่ายงาน ของ  ' . $std_data->fullname . '</p>
			<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ด้วย มหาวิทยาลัยราชภัฏเชียงรายได้มีการสำรวจความพึงพอใจผู้ใช้บัณฑิตต่อการ
			ปฏิบัติงานของบัณฑิตที่สำเร็จการศึกษาจากมหาวิทยาลัยราชภัฏเชียงรายเพื่อให้ได้ข้อมูลที่มีคุณค่าอย่างยิ่งในการนําไปใช้ในการวางแผนการจัดการศึกษา การปรับปรุงหลักสูตร
			และกระบวนการจัดการเรียนการสอนของมหาวิทยาลัยให้เป็นไปอย่างมีประสิทธิภาพ รวมถึงการพัฒนาบัณฑิตให้มีคุณภาพยิ่งขึ้นและสอดคล้องกับความต้องการของผู้ใช้บัณฑิตในปัจจุบันและอนาคต
			</p>
			<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จึงใคร่ขอความอนุเคราะห์ท่านตอบแบบสอบถามเพื่อประเมินการปฏิบัติงานของบัณฑิตที่สำเร็จการศึกษาจากมหาวิทยาลัยราชภัฏเชียงรายผ่านแบบสอบถามประเมินความพึงพอใจผู้ใช้บัณฑิตมหาวิทยาลัยราชภัฏเชียงราย
			โดยท่านสามารถเข้าผ่านลิงค์ต่อไปนี้ <a href="https://cse.crru.ac.th/cse/survey/question_form?std_id=' . $workplace->std_id . '&token_code=' . $token_code . '">
			https://cse.crru.ac.th/cse/survey/question_form?std_id=' . $workplace->std_id . '&token_code=' . $token_code . '</a>
			หากมีข้อสงสัยในการกรอกแบบสอบถามหรือต้องการข้อมูลเพิ่มเติมโปรดติดต่อ cse@crru.ac.th หรือ กองมาตรฐานวิชาการและประกันคุณภาพ เบอร์โทรศัพท์ 053-776-136
			</p>
			<p style="text-align:center;">จึงเรียนมาเพื่อโปรดพิจารณาให้ความอนุเคราะห์ และใคร่ขอขอบคุณล่วงหน้า มา ณ โอกาสนี้</p>
			<br><br>
			<p style="text-align:center;">ขอแสดงความนับถือ</p>
			<div style="text-align:center;">
    	    <img src="https://lh3.googleusercontent.com/f6I9oTrnSlsZsB-iIHd8xp45DPnN0XcUpbd_bQSDBWQP7GHBnDjXXCyKyTLlFvFARx1QqWN-oIzmfP1FpwT-BMMjaN7YQPxVSfkv5zfnRtQZUFb0IPsexDX7JKwnL4IvfpkuAoOizQbLqXXlk4sqjORXAJP-wc0oER6AlUtk8Ryf4B8AwwCSs-FSM55TsK5FqBGePOtsNGgrtVBAKFq0Epo0_nWNkTgw776gfz_s6QARaLzDkDWpC_wqqaCJhQta9Umf_ZX9Ang0PhLHcC0SD3P4ivDbuvOad_OyTejLzjw-wniGdW93i3w9vZBWXxc9G-30LoKxcRSSitKpbkitBx64u1TCZx3cqDECgL3_RsA-VwLY0tGXrgv1FqtdBL--wCLlWugrgBn_xo9NG15KHO1fJmkfvTGA9tEvU3CqfNEwXrQKibE_2whjTLWnXU6swub9fon91MetH6mPcPENllBJv1H0NXQz5xUtKawCFlE1wB_NOdAz3EQaNyh-Gw8OXgdhpH07BWVFJKlQk9yauNlQIybVW2wX5o2CDK0tzPlBvDguEFggtug1M86HIXKmdPqA9o8qpwg1vfILABXfdMCFJbOftvwVu-t46xfynNqrUEy-00jkoweROT425dGaDdIgmrPu7U7t33b_Ftb_ytihF7cPGbw=w285-h132-no"
    	      width="150" alt="">
    	    </div>
    	    <br>
			<p style="text-align:center;">(ผู้ช่วยศาสตราจารย์ ดร.ศรชัย  มุ่งไธสง)</p>
			<p style="text-align:center;">อธิการบดีมหาวิทยาลัยราชภัฏเชียงราย</p>
			</body>
			</html>
			';
        // Also, for getting full html you may use the following internal method:
        $subject = 'ขอความอนุเคราะห์ตอบแบบสอบถามสำรวจความพึงพอใจผู้ใช้บัณฑิต';
        // $from_email = smtp_pass_mail;
        $from_email = $cse_email->email;
        $pass = $cse_email->password;

        $result = $this->email
            ->from($from_email, 'มหาวิทยาลัยราชภัฏเชียงราย')   // Optional, an account where a human being reads.
            ->set_smtp_pass($pass)
            ->to($workplace->director_email)
            ->subject($subject)
            ->message($body)
            ->send();
        // print_r($result);
        return $result;
    }

    private function send_email_std($student, $token_code, $cse_email)
    {

        $this->load->library('email');
        $post = $this->input->post(NULL, TRUE);
        $std_data = $this->student->get_detail($student->std_id, $student->citizen_id);

        // Get full html:
        $body =
            '<!DOCTYPE html>
			<html>
			<head>
			<meta charset="utf-8">
			<title></title>
			<style media="screen">
			p{
				font-size: 12px;
			}
			</style>
			</head>
			<body>
			<div class="">
			<img src="https://lh3.googleusercontent.com/zY3Irqj4NEEhFr2R0MGIOBjeUyMKbzaoQAdKzD69STlNgYyCjW-ipFToNSj8CUqVKhqNhfbUF5igpaZm3tlDFFdrHUe6Y01k2LWSy7EnuuzTknnl_U5yrefrZZhVIZZHa115qtdkGkaFVY1yeNxpjhPQyKTP6aYJ3lIizoWfFWZuWyX7Zw1iWUQNheRw5a6ElZyzgcUNHwIhcH9gEI4bew9CauiA5r_deehA_meIzlkLtXH_jQPCCkrCxhg3jgpU07sHjcAOwVEei41OoZbgddtNx0MwhkSaJlf98e4XhFZoFAiDt46lG2fmQBx6umiQVBuvmn-jx3DE4LV0NvTzNLOJxY2cLO3nrMHD8m662xFj8QV-ES11qJlQAf7r6biGPsrDbhfqqC3Hvrn9hOb68Tb8RWeXBiLwQOxcDo4iwEnpborqLd4NVbB6qgGGNbwh6SMnUixq_vNcsTjcNVSJE6od3GrWImThAVy88gJ9vtb0NLq3BbcHp9-wGLOYwR_JMUNoywGvb6qte3qjIwNiUfVGrdsTndvY3PlVaQw-SaiT4tWAuIXGk62X8LkCDmPcMzxbWYYz6pw8aJJPR5MlhAXxZkPLws81GkK3bsc=s300-no" width="100" style="margin:0 auto;display: block;">
			</div>
			<p style="text-align:right;margin:0 auto;">
			มหาวิทยาลัยราชภัฏเชียงราย<br />
			ถนนพหลโยธิน  อำเภอเมือง<br />
			จังหวัดเชียงราย ๕๗๑๐๐<br />
			</p>
			<p style="text-align:center">' . thainumDigit(do_full_thai_date_style_1(date('Y-m-d'))) . '</p>
			<p><b>เรื่อง</b> ขอความอนุเคราะห์ตอบแบบสอบถาม</p>
			<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ด้วย มหาวิทยาลัยราชภัฏเชียงราย
			ได้ดำเนินการสำรวจการมีงานทำบัณฑิตมหาวิทยาลัยราชภัฏเชียงราย เพื่อนำข้อมูลไปใช้เป็นแนวทางในการ
			ปรับปรุงหลักสูตรและการจัดการเรียนการสอนของมหาวิทยาลัยให้มีประสิทธิภาพ
			</p>
			<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ในการนี้จึงใคร่ขอความอนุเคราะห์ข้อมูลเกี่ยวกับสถานภาพการทำงานและข้อมูลติดต่อ
			หัวหน้างานของท่านเพื่อนำไปใช้ประโยชน์ในการสร้างเครือข่ายกับผู้ประกอบการที่ใช้บัณฑิต โดยท่านสามารถเข้าไปกรอกข้อมูลในแบบสอบถามสถานะการมีงานทำของบัณฑิตออนไลน์
			<a href="https://cse.crru.ac.th/cse/survey/question_form_std?std_id=' . $std_data->std_id . '&token_code=' . $token_code . '">
			https://cse.crru.ac.th/cse/survey/question_form_std?std_id=' . $std_data->std_id . '&token_code=' . $token_code . '
			</a>
			หากมีข้อสงสัยในการกรอกแบบสอบถามหรือต้องการข้อมูลเพิ่มเติมโปรดติดต่อ cse@crru.ac.th หรือกองมาตรฐานวิชาการและการประกันคุณภาพ เบอร์โทรศัพท์ 053-776-136
			</p>
			<p style="text-align:center;">จึงเรียนมาเพื่อโปรดพิจารณาให้ความอนุเคราะห์ และใคร่ขอขอบคุณล่วงหน้า มา ณ โอกาสนี้</p>
			<br><br>
			<p style="text-align:center;">ขอแสดงความนับถือ</p>
			<div style="text-align:center;">
    	      <img src="'.base_url('assets/images/president_sign.jpg').'" width="150" alt="">
    	    </div>
    	    <br>
			<p style="text-align:center;">(ผู้ช่วยศาสตราจารย์ ดร.ศรชัย  มุ่งไธสง)</p>
			<p style="text-align:center;">อธิการบดีมหาวิทยาลัยราชภัฏเชียงราย</p>
			</body>
			</html>

			';
        // Also, for getting full html you may use the following internal method:
        $subject = 'ขอความอนุเคราะห์ตอบแบบสอบถามสถานะการมีงานทำ';
        // $from_email = smtp_pass_mail;
        $from_email = $cse_email->email;
        $pass = $cse_email->password;

        $result = $this->email
            ->from($from_email, 'มหาวิทยาลัยราชภัฏเชียงราย')   // Optional, an account where a human being reads.
            ->set_smtp_pass($pass)
            ->to($student->email)
            ->subject($subject)
            ->message($body)
            ->send();
        // print_r($result);
        return $result;
    }


}//End class
