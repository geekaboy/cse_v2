<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Question extends CI_Controller
{

	//========================= PUBLIC=========================================//
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{ }

	public function add_view()
	{
		$sql = "SELECT * FROM cse_v2.question_group qg
		WHERE qg.is_status ='1'
		ORDER BY qg.order_num";
		$q = $this->db->query($sql);
		$data = array(
			'rsdata' => $q->result_array(),
			'num_row' => $q->num_rows()
		);

		//@Plugin & Appjs
		$data['plugin'] = array(
			'assets/plugins/sweetalert/sweetalert_all.js',
			'assets/plugins/PNotify/pnotify.min.js',
			'assets/plugins/PNotify/pnotify.min.css'
		);
		$data['appjs'] = array(
			'appjs/question/add_view.js'
		);

		//@VIEW
		$this->load->view('theme/header', $data);
		$this->load->view('question/add_view.php');
		$this->load->view('theme/footer');
	}

	public function edit_view()
	{	
		$data_get = $this->input->get();
		$sql = "SELECT * FROM cse_v2.question_group qg
		WHERE qg.is_status ='1'
		ORDER BY qg.id";
		$q = $this->db->query($sql);
		$data = array(
			'group_list' => $q->result_array(),
			'num_row' => $q->num_rows()
		);

		$sql = "SELECT * FROM cse_v2.question_list WHERE id = {$data_get['id']}";
		$data['question'] = $this->db->query($sql)->row();

		//@Plugin & Appjs
		$data['plugin'] = array(
			'assets/plugins/sweetalert/sweetalert_all.js',
			'assets/plugins/PNotify/pnotify.min.js',
			'assets/plugins/PNotify/pnotify.min.css'
		);
		$data['appjs'] = array(
			'appjs/question/edit_view.js'
		);

		//@VIEW
		$this->load->view('theme/header', $data);
		$this->load->view('question/edit_view.php');
		$this->load->view('theme/footer');
	}

	public function question_save()
	{
		$data_post = $this->input->post();

		$data = array(
			'question_group_id'=>$data_post['question_group'],
			'title'=>$data_post['question_name'],
		);

		$is_sucess = $this->db->insert('cse_v2.question_list', $data);
		if($is_sucess){
			echo json_encode(
				array(
					'is_success'=>TRUE,
					'msg'=>'บันทึกเรียบร้อย'
				)
			);
		}else{
			echo json_encode(
				array(
					'is_success'=>FALSE,
					'msg'=>'ผิดพลาด'
				)
			);
		}
	}//end function

	public function question_update(){
		$title = $this->input->post('title',true);
		$group_id = $this->input->post('group_id');
		$id = $this->input->post('id');

		$data = array(
			'question_group_id'=>$group_id,
			'title'=>$title
		);

		$condition = array(
			'id'=>$id
		);

		$is_sucess = $this->db->update('cse_v2.question_list', $data, $condition);
		if($is_sucess){
			echo json_encode(
				array(
					'is_success'=>TRUE,
					'msg'=>'แก้ไขข้อมูลเรียบร้อย'
				)
			);
		}else{
			echo json_encode(
				array(
					'is_success'=>FALSE,
					'msg'=>'ผิดพลาด'
				)
			);
		}

	}//end function


	public function list_view()
	{
		$sql = "SELECT
			ql.*,
			qg.title AS group_title,
			qg.id AS gid
		FROM cse_v2.question_list ql
		LEFT JOIN cse_v2.question_group qg ON ql.question_group_id = qg.id
		WHERE ql.is_status = '1'
		ORDER BY gid";
		$q = $this->db->query($sql);
		$data = array(
			'rsdata'=>$q->result_array(),
			'num_row'=>$q->num_rows()
		);
		$this->load->view('question/list_view.php',$data);

	}

	public function add_group_view()
	{
		//@Plugin & Appjs
		$data['plugin'] = array(
			'assets/plugins/sweetalert/sweetalert_all.js'
		);
		$data['appjs'] = array(
			'appjs/question/add_question_view.js'
		);

		//@VIEW
		//@VIEW
		$this->load->view('theme/header', $data);
		$this->load->view('question/add_group_view.php');
		$this->load->view('theme/footer');
	}

	public function group_list_view()
	{
		$sql = "SELECT * 
				FROM cse_v2.question_group qg 
				WHERE qg.is_status = '1' 
				ORDER BY order_num";
		$q = $this->db->query($sql);
		$data = array(
			'rsdata' => $q->result_array(),
			'num_row' => $q->num_rows()
		);
		$this->load->view('question/group_list_view.php', $data);
	}

	public function group_save()
	{
		$data_post = $this->input->post();

		$data = array(
			'title' => $data_post['title'],
			'group_type' => '0'

		);

		$is_sucess = $this->db->insert('cse_v2.question_group', $data);
		if ($is_sucess) {
			echo json_encode(
				array(
					'is_success' => TRUE,
					'msg' => 'บันทึกเรียบร้อย'
				)
			);
		} else {
			echo json_encode(
				array(
					'is_success' => FALSE,
					'msg' => 'ผิดพลาด'
				)
			);
		}
	}

	public function get_group_detail()
	{
		$data_post = $this->input->get();

		$cond = array(
			'id' => $data_post['id']

		);
		$qdata = $this->db->get_where('cse_v2.question_group', $cond)->row_array();
		echo json_encode($qdata);
	}

	public function group_update()
	{
		$title = $this->input->post('title', true);
		$id = $this->input->post('id');

		$data = array(
			'title' => $title
		);

		$condition = array(
			'id' => $id
		);

		$is_sucess = $this->db->update('cse_v2.question_group', $data, $condition);

		if ($is_sucess) {
			echo json_encode(
				array(
					'is_success' => TRUE,
					'msg' => 'แก้ไขข้อมูลเรียบร้อย'
				)
			);
		} else {
			echo json_encode(
				array(
					'is_success' => FALSE,
					'msg' => 'ผิดพลาด'
				)
			);
		}
	}
}//End class
