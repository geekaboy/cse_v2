<?php
//------------[Controller File name : Web_link.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Student_data extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_data_model', 'student');
        $this->load->model('survey_group_model', 'survey_group');


    }

    public function index()
    { }

    public function list_view()
    {
        //@Plugin & @Appjs
        $data['plugin'] = array();
        $data['appjs'] = array(
            'appjs/student_data/list_view.js'
        );

        $data['survey_group'] = $this->survey_group->get_list();


        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('student_data/student_data_view');
        $this->load->view('theme/footer');
    }

    public function get_list()
    {
        $data_get = $this->input->get();

        $where = "";
        if ($data_get['search_text'] != '') {
            $where .= " AND (
                    std.maj_name LIKE '%{$data_get['search_text']}%'
                    OR std.fac_name LIKE '%{$data_get['search_text']}%'
                    OR std.fullname LIKE '%{$data_get['search_text']}%'
                    OR std.std_id LIKE '%{$data_get['search_text']}%'
                    OR std.citizen_id LIKE '%{$data_get['search_text']}%'
                  )";
        }
        $sql = "SELECT COUNT(std.std_id) AS total_row
            FROM cse_v2.student_data std
            WHERE std.year_graduated = '{$data_get['year_graduated']}' $where";

        $q = $this->db->query($sql)->row();
        $total_row = $q->total_row;
        $page = (isset($data_get['page'])) ? $data_get['page'] : 1;

        $data['total_row'] = $total_row;
        $data['search_text'] = $data_get['search_text'];
        $data['year_graduated'] = $data_get['year_graduated'];

        $this->load->helper('pagination');
        $config['base_url'] = site_url('student_data/get_list');
        $config['total_row'] = $total_row;
        $config['per_page'] = 100;
        gen_pagination($config);

        $limit = $config['per_page'];
        $start = ($page - 1) * $limit;
        $data['start'] = $start;

        $sql = "SELECT std.*
            FROM cse_v2.student_data std
            WHERE std.year_graduated = '{$data_get['year_graduated']}' $where
            ORDER BY std.maj_name, std.fac_name, std.std_id, std.citizen_id
            LIMIT $limit OFFSET $start";

        $data['student_list'] = $this->db->query($sql)->result();

        $this->load->view('student_data/student_list_view', $data);
    }

    public function workplace_view()
    {
        //@Plugin & @Appjs
        $data['plugin'] = array();
        $data['appjs'] = array(
            'appjs/student_data/workplace_list_view.js'
        );

        $data['survey_group'] = $this->survey_group->get_list();

        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('student_data/workplace_view');
        $this->load->view('theme/footer');
    }

    public function get_workplace_list()
    {
        $data_get = $this->input->get();

        $where = "";
        if ($data_get['search_text'] != '') {
            $where .= " AND (
                    std.maj_name LIKE '%{$data_get['search_text']}%'
                    OR std.fac_name LIKE '%{$data_get['search_text']}%'
                    OR std.fullname LIKE '%{$data_get['search_text']}%'
                    OR std.std_id LIKE '%{$data_get['search_text']}%'
                    OR w.organiz_name LIKE '%{$data_get['search_text']}%'
                  )";
        }
        $sql = "SELECT COUNT(w.std_id) AS total_row FROM cse_v2.student_workplace w
            LEFT JOIN cse_v2.student_data std
              ON w.std_id = std.std_id
              AND w.citizen_id = std.citizen_id
            WHERE std.year_graduated = '{$data_get['year_graduated']}' $where";

        $q = $this->db->query($sql)->row();
        $total_row = $q->total_row;
        $page = (isset($data_get['page'])) ? $data_get['page'] : 1;

        $data['total_row'] = $total_row;
        $data['search_text'] = $data_get['search_text'];
        $data['year_graduated'] = $data_get['year_graduated'];

        $this->load->helper('pagination');
        $config['base_url'] = site_url('student_data/get_workplace_list');
        $config['total_row'] = $total_row;
        $config['per_page'] = 100;
        gen_pagination($config);

        $limit = $config['per_page'];
        $start = ($page - 1) * $limit;
        $data['start'] = $start;

        $sql = "SELECT w.*, std.prefix, std.fullname, std.maj_name, std.fac_name
            FROM cse_v2.student_workplace w
            LEFT JOIN cse_v2.student_data std
              ON w.std_id = std.std_id
              AND w.citizen_id = std.citizen_id
            WHERE std.year_graduated = '{$data_get['year_graduated']}' $where
            ORDER BY w.is_answer, std.maj_name, std.fac_name
            LIMIT $limit OFFSET $start";

        $data['student_list'] = $this->db->query($sql)->result();

        $this->load->view('student_data/workplace_list_view', $data);
    }

    public function get_maj_by_fac()
	{
		$post = $this->input->post(NULL, TRUE);
		$maj_list = $this->student->get_major_by_fac($post['fac_name']);

		echo json_encode($maj_list);
	}
}//End class
