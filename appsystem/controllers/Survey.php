<?php
//------------[Controller File name : Web_link.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Survey extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_data_model','student');
        $this->load->model('student_workplace_model','workplace');
        $this->load->model('work_time_range_model','time_range');


    }

    public function question_form()
    {
        $get = $this->input->get(NULL, TRUE);

        if(empty($get['std_id']) || empty($get['token_code'])){
            $res_data = array(
					'is_success' => FALSE,
					'message' => 'ไม่พบข้อมูลที่ท่านร้องขอ'
				);
            $this->load->view('qform/message_view', $res_data);
            return FALSE;
        }//END IF


        $std_id =$get['std_id'];;
        $token_code = $get['token_code'];

        $sql = "SELECT s.*
            FROM cse_v2.send_survey s
            WHERE s.std_id = {$this->db->escape($std_id)}
                AND s.token_code = {$this->db->escape($token_code)}";
        $survey_data = $this->db->query($sql)->row();

        if(COUNT($survey_data) > 0){// CHECK HAVE RECORD
            if($survey_data->is_answer == '0'){
                //ดึงข้อมูลการทำงาน
                $sql = "SELECT *, w.id AS wid
                    FROM cse_v2.student_workplace w
                    LEFT JOIN cse_v2.student_data std
                        ON w.std_id = std.std_id AND w.citizen_id = std.citizen_id
                    WHERE w.id = {$this->db->escape($survey_data->student_workplace_id)}
                        AND w.std_id = {$this->db->escape($std_id)}
                        AND w.citizen_id = {$this->db->escape($survey_data->citizen_id)}";
                $work_data = $this->db->query($sql)->row();

                // ดึงประเภทองค์กร
                $sql = "SELECT * FROM cse_v2.org_type rt
                    WHERE rt.is_status = '1' AND rt.id <> 0";
                $org_data = $this->db->query($sql)->result();


                // ดึงลำดับตำแหน่ง
                $sql = "SELECT * FROM cse_v2.director_type d
                    WHERE d.is_status = '1' AND d.id <> 0";
                $director_type = $this->db->query($sql)->result();

                //  ดึงคำถาม
                $sql = "SELECT g.id AS qgroup_id, g.title AS qgroup_title,
                                l.id AS qlist_id, l.title AS qlist_title
                        FROM cse_v2.question_list l
                        LEFT JOIN cse_v2.question_group g
                            ON l.question_group_id = g.id
                        WHERE l.is_status = '1' AND g.is_status = '1'
                        ORDER BY g.id";
                $q = $this->db->query($sql);
                $qlist = $q->result_array();
                $qlist_num_rows = $q->num_rows();

                //update read form
                $cond_email = array(
                    'std_id' => $std_id,
                    'token_code' => $token_code
                );
                $data_email = array(
                    'is_read' => '1',
                );
                //update read question
                $this->db->update('cse_v2.send_survey', $data_email, $cond_email);
                $data = array(
                    'work_data' => $work_data,
                    'org_data' => $org_data,
                    'director_type' => $director_type,
                    'qlist' => $qlist,
                    'num_rows' => $qlist_num_rows,
                    'token_code' => $token_code
                );

                //@Plugin & Appjs
                $data['plugin'] = array(
                    'assets/plugins/sweetalert/sweetalert_all.js',
                    'assets/plugins/PNotify/pnotify.min.js',
                    'assets/plugins/PNotify/pnotify.min.css'
                );
                $data['appjs'] = array(
                    'appjs/qform/qform_view.js'
                );

                $data['student_workplace_id'] = $survey_data->student_workplace_id;

                //@VIEW
                $this->load->view('qform/qform_view', $data);
                $this->load->view('theme/footer');

            }else{
                $res_data = array(
    					'is_success' => TRUE,
    					'message' => 'ท่านได้ตอบแบบสำรวจเรียบร้อยแล้ว'
    				);
                $this->load->view('qform/message_view', $res_data);
            }//END IF ELSE

        }else{

            $res_data = array(
					'is_success' => FALSE,
					'message' => 'ไม่พบข้อมูลที่ท่านร้องขอ'
				);
            $this->load->view('qform/message_view', $res_data);

        }//END IF ELSE

    }

    public function question_form_std(){
        $get = $this->input->get(NULL, TRUE);

        if(empty($get['std_id']) || empty($get['token_code'])){
            $res_data = array(
					'is_success' => FALSE,
					'message' => 'ไม่พบข้อมูลที่ท่านร้องขอ'
				);
            $this->load->view('qform/message_view', $res_data);
            return FALSE;
        }//END IF
        $std_id =$get['std_id'];;
        $token_code = $get['token_code'];
        $sql = "SELECT s.*
            FROM cse_v2.send_survey s
            WHERE s.std_id = {$this->db->escape($std_id)}
                AND s.token_code = {$this->db->escape($token_code)}";
        $survey_data = $this->db->query($sql)->row();

        if(COUNT($survey_data) > 0){
            if($survey_data->is_answer == '0'){
                $cond = array('token_code' => $token_code, 'std_id'=>$std_id );
                $data_update = array('date_update'=>'now()', 'is_read'=>'1');
                $this->db->update('cse_v2.send_survey', $data_update, $cond);

                $data['std_data'] = $this->student->get_detail($survey_data->std_id, $survey_data->citizen_id);
                $data['time_range_list'] = $this->time_range->get_list();
                $data['country_list'] = $this->db->get('cse_v2.addr_country')->result();
                $data['provice_list'] = $this->db->get('cse_v2.addr_provinces')->result();
                $data['token_code'] = $token_code;
                //@Plugin & Appjs
                $data['plugin'] = array(
                    'assets/plugins/sweetalert/sweetalert_all.js',
                    'assets/plugins/PNotify/pnotify.min.js',
                    'assets/plugins/PNotify/pnotify.min.css',
                    'assets/plugins/jquery.serializeJSON/jquery.serializejson.min.js'
                );
                $data['appjs'] = array(
                    'appjs/qform_std/app.js'
                );

                //@VIEW
                $this->load->view('qform_std/main_view', $data);
                $this->load->view('theme/footer');
            }else {
                redirect('survey/success_view');
            }

        }


    }

    public function success_view()
    {
        //@Plugin & Appjs
        $data['plugin'] = array();
        $data['appjs'] = array();

        //@VIEW
        $this->load->view('qform/success_view', $data);
    }

    public function save()
    {

        $post = $this->input->post(NULL, TRUE);
        // echo "<pre>";
        // print_r($post);
        // exit();
        $student_data = $this->student->get_detail($post['std_id'], $post['citizen_id']);

        //========= CHECK WORK STATUS ===========//
        if ($post['work_status'] == '0') {

            $cond = array(
                'std_id' => $post['std_id'],
                'citizen_id' => $post['citizen_id']
            );

            $is_success =   $this->db->update('cse_v2.student_data',
                            array(
                                'work_status' => '0'
                            ),
                            $cond);
            $cond = array(
                'std_id' => $post['std_id'],
                'citizen_id' => $post['citizen_id'],
                'id'=> $post['student_workplace_id']
            );
            $is_success =   $this->db->update('cse_v2.student_workplace',
                            array(
                                'date_update'=>'now()',
                                'is_current' => '0',
                                'is_answer' => '1',
                                'suggestion'=>$post['work_cause']
                            ),
                            $cond);

            $cond = array(
                'std_id' => $post['std_id'],
                'citizen_id'=>$post['citizen_id'],
                'token_code' => $post['token_code']
            );
            $data = array(
                'is_answer' => '1',
                'answer_time' => 'now()'
            );
            $is_success = $this->db->update('cse_v2.send_survey', $data, $cond);

            $msg = ($is_success)?'เรียบร้อยแล้ว':'';
            echo json_encode(
                array(
                    'is_success'=>$is_success,
                    'msg'=>$msg
                )
            );
            exit();
        }

        //============= VALIDATE ===================//
        $valid_arr = array(
            'organiz_name'=>'ชื่อหน่วยงาน/สถานประกอบการ',
            'organiz_type'=>'ลักษณะหน่วยงาน',
            'director_type'=>'ระดับตำแหน่งของทานในองค์กร',
            'std_id'=>'ชื่อ - สกุล'
        );
        if($this->validation_form($valid_arr)){
            $data = array(
                'org_name' => $post['organiz_name'],
                'org_type_id' => $post['organiz_type'],
                'org_type_other' => $post['organiz_type_other'],
                'director_type_id' => $post['director_type'],
                'director_type_other' => $post['director_type_other'],
                'std_id' => $post['std_id'],
                'citizen_id' => $post['citizen_id'],
                'comment_note' => $post['comment_note'],
                'from_token_code'=>$post['token_code'],
                'student_workplace_id'=>$post['student_workplace_id']
            );

            $is_success = $this->db->insert('cse_v2.answer_general', $data);

            $answer_general_id = $this->db->insert_id('cse_v2.answer_general_id_seq');

            //========= INSERT ANSWER =========//
            foreach ($post['answer_list'] as $row) {
                $data_answer = array(
                    'std_id' => $post['std_id'],
                    'citizen_id' => $post['citizen_id'],
                    'question_list_id' => $row['question_list_id'],
                    'question_group_id' => $row['question_group_id'],
                    'answer_general_id' => $answer_general_id,
                    'answer_value' => $row['answer']
                );
                $is_success = $this->db->insert('cse_v2.answer_list', $data_answer);
            }

            //========= UPDATE SEND_SURVEY =======//
            $cond = array(
                'std_id' => $post['std_id'],
                'citizen_id'=>$post['citizen_id'],
                'token_code' => $post['token_code']
            );
            $data = array(
                'is_answer' => '1',
                'answer_time' => 'now()'
            );
            $is_success = $this->db->update('cse_v2.send_survey', $data, $cond);

            //========== UPDATE STATUS STUDENT_WORKPLACE =========//
            $cond = array(
                'std_id' => $post['std_id'],
                'citizen_id' => $post['citizen_id'],
                'id'=> $post['student_workplace_id']
            );
            $is_success =   $this->db->update('cse_v2.student_workplace',
                            array(
                                'date_update'=>'now()',
                                'is_current' => '0',
                                'is_answer' => '1',
                                'suggestion'=>$post['work_cause']
                            ),
                            $cond);
            //Check success
            $msg = ($is_success)?'เรียบร้อยแล้ว':'';
            echo json_encode(
                array(
                    'is_success'=>$is_success,
                    'msg'=>$msg
                )
            );
        }//END IF

    }


    public function save_std()
    {
        $post = $this->input->post(NULL, TRUE);

        if($post['work_status'] == 0){
            $cond = array('std_id'=>$post['std_id'], 'citizen_id'=>$post['citizen_id'] );
            $data_update = array(
                'date_update'=>'now()',
                'work_status'=>$post['work_status'],
                'is_answer'=>'1'
            );
            $is_success = $this->db->update('cse_v2.student_data', $data_update, $cond);

            $cond = array(
                'token_code'=>$post['token_code'],
                'std_id'=>$post['std_id']
            );
            $data_update = array(
                'date_update'=>'now()',
                'is_answer'=>'1',
                'answer_time'=>'now()'
            );
            $is_success = $this->db->update('cse_v2.send_survey', $data_update, $cond);

            $msg = ($is_success)?'บันทึกเรียบร้อยแล้ว':'Somyhing wrong, Please try again leter.';

            echo json_encode(array(
                'is_success'=>$is_success,
                'msg'=>$msg
            ));

        }else{

            //============= VALIDATE ===================//
            $valid_arr = array(
                'std_mobile_number'=>'เบอร์โทรศัพท์บัณฑิต',
                'work_match'=>'ลักษณะงานที่ทำ',
                'work_time_range'=>'ระยะเวลาทำงาน',
                'work_position'=>'ตำแหน่งงาน',
                'salary'=>'เงินเดือน',
                'organization_name'=>'ชื่อหน่วยงาน/สถานประกอบกอบ',
                'addr'=>'ที่อยู่',
                'province'=>'จังหวัด',
                'amphures'=>'อำเภอ',
                'tambon'=>'ตำบล',
                'zipcode'=>'รหัสไปรษณีย์',
                'country'=>'ประเทศ',
                'director_name'=>'ชื่อหัวหน้างาน',
                'director_email'=>'E-Mail หัวหน้างาน',
                'tel'=>'เบอร์โทรศัพท์หัวหน้างาน'
            );
            if($this->validation_form($valid_arr)){
                $data = array(
    				'std_id' => $post['std_id'],
    				'citizen_id' => $post['citizen_id'],
    				'work_position' => $post['work_position'],
    				'organiz_name' => $post['organization_name'],
    				'work_time_range_id' => $post['work_time_range'],
    				'work_match' => $post['work_match'],
    				'salary' => $post['salary'],
    				'address' => $post['addr'],
    				'province' => $post['province'],
    				'amphures' => $post['amphures'],
    				'tambon' => $post['tambon'],
    				'zipcode' => $post['zipcode'],
    				'nation' => $post['country'],
    				'tel' => $post['tel'],
    				'fax' => $post['fax'],
    				'is_current' => '1',
    				'director_email' => $post['director_email'],
    				'director_name' => $post['director_name']
    			);
                $is_success = $this->db->insert('cse_v2.student_workplace', $data);

                $cond = array('std_id'=>$post['std_id'], 'citizen_id'=>$post['citizen_id'] );
                $data_update = array(
                    'date_update'=>'now()',
                    'work_status'=>$post['work_status'],
                    'is_answer'=>'1'
                );
                $is_success = $this->db->update('cse_v2.student_data', $data_update, $cond);

                $cond = array(
                    'token_code'=>$post['token_code'],
                    'std_id'=>$post['std_id']
                );
                $data_update = array(
                    'date_update'=>'now()',
                    'is_answer'=>'1',
                    'answer_time'=>'now()'
                );
                $is_success = $this->db->update('cse_v2.send_survey', $data_update, $cond);

                $msg = ($is_success)?'บันทึกเรียบร้อยแล้ว':'Somyhing wrong, Please try again leter.';

                echo json_encode(array(
                    'is_success'=>$is_success,
                    'msg'=>$msg
                ));

            }//END VALIDATION


        }//END IF ELSE

    }
    //====================== PRIVATE FUNCTION ================================//
    private function validation_form($valid_list = array())
    {
        $this->load->library('form_validation');
        $frm = $this->form_validation;

        foreach ($valid_list as $key => $value) {
            $frm->set_rules($key, $value, 'trim|required');
        }

        $frm->set_message('required', 'กรุณากรอก %s');

        if ($frm->run() == FALSE) {
            $message  = '';
            foreach ($valid_list as $key => $value) {
                $message .= form_error($key);
            }
            echo json_encode( array(
                    'is_success' => FALSE,
                    'msg' => $message
            ));
            exit();
        }else{
            return TRUE;
        }
    }

}//End CLASS
