<?php
//------------[Controller File name : Web_link.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Survey_group extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('survey_group_model', 'survey_group');

    }

    public function get_detail()
    {
        $post = $this->input->post(NULL, TRUE);
        $detail = $this->survey_group->get_detail($post['year']);

        echo json_encode($detail);
    }

}//END CLASS
