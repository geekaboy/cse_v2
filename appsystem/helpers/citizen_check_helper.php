<?php

function valid_citizen_id($personID){
  if (strlen($personID) != 13) {
    return false;
  }
  $rev = strrev($personID); // reverse string ขั้นที่ 0 เตรียมตัว
  $total = 0;
  for($i=1;$i<13;$i++) // ขั้นตอนที่ 1 - เอาเลข 12 หลักมา เขียนแยกหลักกันก่อน
  {
    $mul = $i +1;
    $count = $rev[$i]*$mul; // ขั้นตอนที่ 2 - เอาเลข 12 หลักนั้นมา คูณเข้ากับเลขประจำหลักของมัน
    $total = $total + $count; // ขั้นตอนที่ 3 - เอาผลคูณทั้ง 12 ตัวมา บวกกันทั้งหมด
  }
  $mod = $total % 11; //ขั้นตอนที่ 4 - เอาเลขที่ได้จากขั้นตอนที่ 3 มา mod 11 (หารเอาเศษ)
  $sub = 11 - $mod; //ขั้นตอนที่ 5 - เอา 11 ตั้ง ลบออกด้วย เลขที่ได้จากขั้นตอนที่ 4
  $check_digit = $sub % 10; //ถ้าเกิด ลบแล้วได้ออกมาเป็นเลข 2 หลัก ให้เอาเลขในหลักหน่วยมาเป็น Check Digit
  if($rev[0] == $check_digit){  // ตรวจสอบ ค่าที่ได้ กับ เลขตัวสุดท้ายของ บัตรประจำตัวประชาชน
    return true; /// ถ้า ตรงกัน แสดงว่าถูก
  }else{
    return false; // ไม่ตรงกันแสดงว่าผิด
  }
}

function valid_birthday($birthday){
  // 15122534
  $date_ = substr($birthday, 0, 2);
  $month_ = substr($birthday, 2, 2);
  $year_ = substr($birthday, 4, 4);

  $is_valid = FALSE;
  if(intval($date_)){
    if(intval($date_) < 31){
      $is_valid = TRUE;
    }else {
      $is_valid = FALSE;

    }
  }else {
    $is_valid = FALSE;

  }


  if(intval($month_)){
    if(intval($month_) < 12){
      $is_valid = TRUE;
    }else {
      $is_valid = FALSE;

    }
  }else {
    $is_valid = FALSE;

  }

  if(intval($year_)){
    if(intval($year_) > 2411 && intval($year_) <= 2580){
      $is_valid = TRUE;
    }else {
      $is_valid = FALSE;

    }
  }else {
    $is_valid = FALSE;

  }
  // return $date_.'-'.$month_.'-'.$year_;
  return $is_valid;


}

function citizen_id_format($citizen_id){
  $cid_1 = substr($citizen_id, 0, 1);
  $cid_2 = substr($citizen_id, 1, 4);
  $cid_3 = substr($citizen_id, 5, 5);
  $cid_4 = substr($citizen_id, 10, 2);
  $cid_5 = substr($citizen_id, 12, 1);

  return $cid_1.'-'.$cid_2.'-'.$cid_3.'-'.$cid_4.'-'.$cid_5;

}






?>
