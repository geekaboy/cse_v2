<?php
Class Mydate {

    public $ci;

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function get_ci() {
        return $this->ci;
    }

    public function get_date() {
        $sql = "SELECT   current_date as db_date ";
        $row = $this->ci->db->query($sql)->row_array();
        return $row['db_date']; //2012-11-21
    }

}
function thainumDigit($num)
{
    return str_replace(
        array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'),
        array("o", "๑", "๒", "๓", "๔", "๕", "๖", "๗", "๘", "๙"),
        $num
    );
}

function full_month_th($m)
{
    $mm = array(
        'มกราคม',
        'กุมภาพันธ์',
        'มีนาคม',
        'เมษายน',
        'พฤษภาคม',
        'มิถุนายน',
        'กรกฎาคม',
        'สิงหาคม',
        'กันยายน',
        'ตุลาคม',
        'พฤศจิกายน',
        'ธันวาคม'
    );
    return $mm[$m];
}

function full_month_th_array($m)
{
    $mm = array(
        "1" => 'มกราคม',
        "2" => 'กุมภาพันธ์',
        "3" => 'มีนาคม',
        "4" => 'เมษายน',
        "5" => 'พฤษภาคม',
        "6" => 'มิถุนายน',
        "7" => 'กรกฎาคม',
        "8" => 'สิงหาคม',
        "9" => 'กันยายน',
        "10" => 'ตุลาคม',
        "11" => 'พฤศจิกายน',
        "12" => 'ธันวาคม'
    );
    return $mm[$m];
}

function full_month_th_key($m)
{
    $mm = array(
        "01" => 'มกราคม',
        "02" => 'กุมภาพันธ์',
        "03" => 'มีนาคม',
        "04" => 'เมษายน',
        "05" => 'พฤษภาคม',
        "06" => 'มิถุนายน',
        "07" => 'กรกฎาคม',
        "08" => 'สิงหาคม',
        "09" => 'กันยายน',
        "10" => 'ตุลาคม',
        "11" => 'พฤศจิกายน',
        "12" => 'ธันวาคม'
    );
    return $mm[$m];
}

function short_month_th($m)
{
    $mm = array(
        'ม.ค.',
        'ก.พ.',
        'มี.ค.',
        'เม.ย.',
        'พ.ค.',
        'มิ.ย.',
        'ก.ค.',
        'ส.ค.',
        'ก.ย.',
        'ต.ค.',
        'พ.ย.',
        'ธ.ค.'
    );
    return $mm[$m];
}

function short_month_th_array($m)
{
    $mm = array(
        "1" => 'ม.ค.',
        "2" => 'ก.พ.',
        "3" => 'มี.ค.',
        "4" => 'เม.ย.',
        "5" => 'พ.ค.',
        "6" => 'มิ.ย.',
        "7" => 'ก.ค.',
        "8" => 'ส.ค.',
        "9" => 'ก.ย.',
        "10" => 'ต.ค.',
        "11" => 'พ.ย.',
        "12" => 'ธ.ค.'
    );
    return $mm[$m];
}

function short_month_th_key($m)
{
    $mm = array(
        "01" => 'ม.ค.',
        "02" => 'ก.พ.',
        "03" => 'มี.ค.',
        "04" => 'เม.ย.',
        "05" => 'พ.ค.',
        "06" => 'มิ.ย.',
        "07" => 'ก.ค.',
        "08" => 'ส.ค.',
        "09" => 'ก.ย.',
        "10" => 'ต.ค.',
        "11" => 'พ.ย.',
        "12" => 'ธ.ค.'
    );
    return $mm[$m];
}

function full_month_en($m)
{
    $mm = array(
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    );
    return $mm[$m];
}

function full_month_en_array($m)
{
    $mm = array(
        "1" => 'January',
        "2" => 'February',
        "3" => 'March',
        "4" => 'April',
        "5" => 'May',
        "6" => 'June',
        "7" => 'July',
        "8" => 'August',
        "9" => 'September',
        "10" => 'October',
        "11" => 'November',
        "12" => 'December'
    );
    return $mm[$m];
}

function full_month_en_key($m)
{
    $mm = array(
        "01" => 'January',
        "02" => 'February',
        "03" => 'March',
        "04" => 'April',
        "05" => 'May',
        "06" => 'June',
        "07" => 'July',
        "08" => 'August',
        "09" => 'September',
        "10" => 'October',
        "11" => 'November',
        "12" => 'December'
    );
    return $mm[$m];
}

function short_month_en($m)
{
    $mm = array(
        'Jan',
        'Febr',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
    );
    return $mm[$m];
}

function short_month_en_array($m)
{
    $mm = array(
        "1" => 'Jan',
        "2" => 'Feb',
        "3" => 'Mar',
        "4" => 'Apr',
        "5" => 'May',
        "6" => 'Jun',
        "7" => 'Jul',
        "8" => 'Aug',
        "9" => 'Sep',
        "10" => 'Oct',
        "11" => 'Nov',
        "12" => 'Dec'
    );
    return $mm[$m];
}

function short_month_en_key($m)
{
    $mm = array(
        "01" => 'Jan',
        "02" => 'Feb',
        "03" => 'Mar',
        "04" => 'Apr',
        "05" => 'May',
        "06" => 'Jun',
        "07" => 'Jul',
        "08" => 'Aug',
        "09" => 'Sep',
        "10" => 'Oct',
        "11" => 'Nov',
        "12" => 'Dec'
    );
    return $mm[$m];
}

// $date = 2012-11-05


function do_full_thai_date($date)
{
    $year = substr($date, 0, 4) + 543;
    $month = full_month_th_key(substr($date, 5, 2));
    $day = substr($date, 8, 2) + 0;
    return $day . " " . $month . " พ.ศ. " . $year;    // 5  มีนาคม  พ.ศ. 2552
}

//$date = 2012-11-05


function do_full_thai_date_style_1($date)
{
    $year = substr($date, 0, 4) + 543;
    $month = full_month_th_key(substr($date, 5, 2));
    $day = substr($date, 8, 2) + 0;
    return $day . " " . $month . " " . $year;    // 5  มี.ค. 2552
}
function do_full_thai_date_number($date)
{
    $year = substr($date, 0, 4) + 543;
    $month = full_month_th_key(substr($date, 5, 2));
    $day = substr($date, 8, 2) + 0;
    return thainumDigit($day) . " " . $month . " " . thainumDigit($year);    // 5  มีนาคม 2552
}

//$date =  23/01/2555     ต่างกับแบบที่ 1 ตรง input


function do_full_thai_date_style_2($date)
{
    $year = substr($date, 6, 4);
    $month = full_month_th_key(substr($date, 3, 2));
    $day = substr($date, 0, 2) + 0;
    return $day . " " . $month . " " . $year;    // 5  มีนาคม 2552
}

//$date = 2012-11-05


function do_standard_date($date)
{
    $year = substr($date, 0, 4) + 543;
    $month = substr($date, 5, 2);
    $day = substr($date, 8, 2) + 0;
    return sprintf("%02d", intval($day)) . "-" . $month . "-" . $year;    // 19/06/2556
}

/*
    $date = 2012-11-05
    return Array
 */

function do_full_thai_date_array($date)
{
    $year = substr($date, 0, 4) + 543;
    $month = full_month_th_key(substr($date, 5, 2));
    $day = substr($date, 8, 2) + 0;

    return array(
        'day' => $day,
        'month' => $month,
        'year' => $year
    );
}

function do_full_english_date($date)
{
    $year = substr($date, 0, 4);
    $month = full_month_en_key(substr($date, 5, 2));
    $day = substr($date, 8, 2) + 0;

    return $month . " " . $day . " , " . $year;    // April 27 , 2009
}

/*
 $date = 2012-11-05
 */

function do_date_name_st1($date)
{
    if (strlen($date) != 10) {
        return "";
    } else {
        $year = substr($date, 0, 4) + 543;
        $yy = substr($year, 2, 2);
        $month = short_month_th_key(substr($date, 5, 2));
        $day = substr($date, 8, 2) + 0;
        return $day . " " . $month . " " . $yy;    // 5-มี.ค.-56
    }
}

function buddhist_to_ymd($date)
{ //01-10-2559
    if (strlen($date) != 10) {
        return "";
    } else {
        $year = substr($date, 6, 4) - 543;
        $month = substr($date, 3, 2);
        $day = substr($date, 0, 2) + 0;
        return $year . "-" . $month . "-" . $day;    // 2016-10-01
    }
}
function ymd_to_buddhist($date)
{ // 2016-10-01
    if (strlen($date) != 10) {
        return "";
    } else {
        $year = substr($date, 0, 4) + 543;
        $month = substr($date, 5, 2);
        $day = substr($date, 8, 2);
        // return $year . "-" . $month . "-" . $day;    // 01-10-2559
        return $day . "-" . $month . "-" . $year;    // 01-10-2559
    }
}
function date_convert_christian($date)
{ //01-10-2559
    if (strlen($date) != 10) {
        return "";
    } else {
        $year = substr($date, 6, 4) - 543;
        $month = substr($date, 3, 2);
        $day = substr($date, 0, 2) + 0;
        return $day . "-" . $month . "-" . $year;    // 01-10-2016
    }
}

function date_convert_christian_reverse($date)
{ //01-10-2016
    if (strlen($date) != 10) {
        return "";
    } else {
        $year = substr($date, 6, 4) + 543;
        $month = substr($date, 3, 2);
        $day = substr($date, 0, 2);
        return $day . "-" . $month . "-" . $year;    // 01-10-2559
    }
}

function do_dayweek_thai($date)
{   // 2011-01-02
    $THAI_DAYWEEK = array(
        "0" => 'อาทิตย์',
        "1" => 'จันทร์',
        "2" => 'อังคาร',
        "3" => 'พุธ',
        "4" => 'พฤหัสบดี',
        "5" => 'ศุกร์',
        "6" => 'เสาร์'
    );

    $obj = new Mydate();
    $ci = $obj->get_ci();

    $sql = "SELECT EXTRACT(DOW FROM TIMESTAMP '$date') as db_day;";
    $row = $ci->db->query($sql)->row_array();
    $x_day = $row['db_day'];


    return $THAI_DAYWEEK[$x_day];
}

function do_dayweek_sm($date)
{   // 2011-01-02
    $THAI_DAYWEEK = array(
        "0" => 'อา.',
        "1" => 'จ.',
        "2" => 'อ.',
        "3" => 'พ.',
        "4" => 'พฤ.',
        "5" => 'ศ.',
        "6" => 'ส.'
    );

    $obj = new Mydate();
    $ci = $obj->get_ci();

    $sql = "SELECT EXTRACT(DOW FROM TIMESTAMP '$date') as db_day;";
    $row = $ci->db->query($sql)->row_array();
    $x_day = $row['db_day'];


    return $THAI_DAYWEEK[$x_day];
}


function date_full_thai_style1()
{
    $obj = new Mydate();
    $date = $obj->get_date();
    $year = substr($date, 0, 4) + 543;
    $month = short_month_th_key(substr($date, 5, 2));
    $day = substr($date, 8, 2) + 0;
    return $day . " " . $month . " " . $year;    // 5  มี.ค. 2552
}

function date_full_thai_number()
{
    $obj = new Mydate();
    $date = $obj->get_date();
    $year = substr($date, 0, 4) + 543;
    $month = full_month_th_key(substr($date, 5, 2));
    $day = substr($date, 8, 2) + 0;
    return thainumDigit($day) . " " . $month . " " . thainumDigit($year);    // 5  มีนาคม 2552
}

function cal_birthday($birthday)
{ //Y-m-d
    $from = new DateTime($birthday);
    $to   = new DateTime(substr(date('Y-m-d'), 0, 10));
    $age = $from->diff($to)->y;

    return $age;
}

//คำนวณอายุ
function get_age($birthday)
{ // birthday = 'yyyy-mm-dd'  =  '2011-05-02'
    $obj = new Mydate();
    $ci = $obj->get_ci();

    $sql = "SELECT  age(timestamp '$birthday')  AS db_age ";
    $row = $ci->db->query($sql)->row_array();
    $x_data = explode(' ', $row['db_age']);

    $data['years'] = 0;
    $data['mons'] = 0;
    $data['days'] = 0;
    for ($i = 0; $i < count($x_data); $i++) {
        if ($i == 0) {
            $data['years'] = $x_data[0];
        } else if ($i == 2) {
            $data['mons'] = $x_data[2];
        } else if ($i == 4) {
            $data['days'] = $x_data[4];
        }
    }
    return $data;
}

function get_current_datetime_db()
{
    $obj = new Mydate();
    return $obj->get_current_datetime_db();
}

//---------- END Helper ---------
