<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once APPPATH.'/third_party/composer/vendor/autoload.php';

class M_pdf {

    public $param;
    public $pdf;
    // public function __construct($param = "'c', 'A4-L'")
    public function __construct($param = "'c', 'A4-L','','','0','0','0','0'")
    {
        $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];
        $this->param =$param;
        $this->pdf = new \Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                __DIR__ . '/custom/font/directory',
            ]),
            'fontdata' => $fontData + [
                'thsarabun' => [
                    'R' => 'THSarabunNew.ttf',
                    'I' => 'THSarabunNew.ttf',
                ]
            ],
            'default_font' => 'thsarabun'
        ]);

    }
}
