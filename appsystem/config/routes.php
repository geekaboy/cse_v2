<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
// $route['register'] = 'website/register';
$route['404_override'] = 'website/home/content_not_found';
$route['translate_uri_dashes'] = FALSE;
