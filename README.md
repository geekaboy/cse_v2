# CSE Customer Satisfaction Evaluation 
**Folder เก็บ ตัวอย่าง Database อยู่ที่ /assets/database/**  
**Folder เก็บ Plugin อยู่ที่ /assets/plugins/**  
**Folder เก็บ Custom JS อยู่ที่ /appjs/**  

**การติดตั้งเบื้องต้น**
**1. แก้ไฟล์ config.php และ database.php เป็นของตัวเอง**
**2. รันคำสั่งต่อไปนี้เพื่อไม่ให้ git track การเปลี่ยนแปลง (เดี๋ยว code จะ conflict กัน)**
```
git update-index --assume-unchanged appsystem/config/config.php
git update-index --assume-unchanged appsystem/config/database.php
git update-index --assume-unchanged .htaccess

```

========================================================================


**การเพิ่ม plugin และ custom JS เข้าหน้า View ให้ส่งผ่าน Controllers ไปหา View
โดยมีรูปแบบดังนี้**
```
$data['plugin] = array(        
    'assets/plugin/plugin_folder/*.css',    
    'assets/plugin/plugin_folder/*.js',
);

$data['appjs'] = array(        
     'appjs/project_folder/view_folder/*.js',
);
$this->load->view('backend/theme/header', $data);

```
