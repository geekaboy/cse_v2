-- Table: cse_v2.email_list

-- DROP TABLE cse_v2.email_list;

CREATE TABLE cse_v2.email_list
(
    date_record timestamp without time zone DEFAULT now(),
    id NOT NULL DEFAULT nextval('cse_v2.email_list_id_seq'::regclass),
    email character varying COLLATE pg_catalog."default",
    password character varying COLLATE pg_catalog."default",
    status character(1) COLLATE pg_catalog."default" DEFAULT 1,
    datetime_use timestamp without time zone,
    CONSTRAINT email_list_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE cse_v2.email_list
    OWNER to dev_admin;
GRANT ALL ON TABLE cse_v2.survey_group TO dev_admin;

GRANT ALL ON TABLE cse_v2.survey_group TO developer_group;