-- Table: cse_v2.country

DROP TABLE cse_v2.country;

CREATE TABLE cse_v2.addr_country
(
    id character varying(10) COLLATE pg_catalog."default" NOT NULL,
    name_th character varying(64) COLLATE pg_catalog."default",
    name_en character varying(64) COLLATE pg_catalog."default",
    CONSTRAINT addr_country_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE cse_v2.addr_country
    OWNER to admin;

GRANT ALL ON TABLE cse_v2.addr_country TO admin;

GRANT ALL ON TABLE cse_v2.addr_country TO dev_admin;

GRANT ALL ON TABLE cse_v2.addr_country TO developer_group;
