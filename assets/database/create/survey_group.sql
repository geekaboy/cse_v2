-- Table: cse_v2.survey_group

-- DROP TABLE cse_v2.survey_group;

CREATE TABLE cse_v2.survey_group
(
    id integer NOT NULL DEFAULT nextval('cse_v2.survey_group_id_seq'::regclass),
    date_record timestamp without time zone NOT NULL DEFAULT now(),
    date_update timestamp without time zone,
    year_graduated character varying(4) COLLATE pg_catalog."default" NOT NULL,
    start_survey timestamp without time zone,
    end_survey timestamp without time zone,
    is_close character(1) COLLATE pg_catalog."default" DEFAULT 0,
    CONSTRAINT survey_group_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE cse_v2.survey_group
    OWNER to dev_admin;

GRANT ALL ON TABLE cse_v2.survey_group TO dev_admin;

GRANT ALL ON TABLE cse_v2.survey_group TO developer_group;

COMMENT ON COLUMN cse_v2.survey_group.year_graduated
    IS 'ปีการศึกษาที่จบ';
	
COMMENT ON COLUMN cse_v2.survey_group.is_close
    IS 'สถานะการปิดสำรวจ 0 = ยังไม่ปิด 1 = ปิด';