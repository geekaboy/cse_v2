-- Table: cse_v2.send_survey

DROP TABLE cse_v2.send_survey;

CREATE TABLE cse_v2.send_survey
(
    id integer NOT NULL DEFAULT nextval('cse_v2.send_survey_id_seq'::regclass),
    date_record timestamp without time zone DEFAULT now(),
    date_update timestamp without time zone,
    std_id character varying COLLATE pg_catalog."default",
    citizen_id character varying COLLATE pg_catalog."default",
    std_email character varying(255) COLLATE pg_catalog."default",
    director_email character varying(255) COLLATE pg_catalog."default",
    survey_type character(1) COLLATE pg_catalog."default",
    token_code character varying(255) COLLATE pg_catalog."default",
    is_read character(1) COLLATE pg_catalog."default" DEFAULT 0,
    is_answer character(1) COLLATE pg_catalog."default" DEFAULT 0,
    answer_time timestamp without time zone,
    CONSTRAINT send_survey_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE cse_v2.send_survey
    OWNER to dev_admin;

COMMENT ON COLUMN cse_v2.send_survey.survey_type
    IS '1= บัฑิต 2 = นายจ้าง';