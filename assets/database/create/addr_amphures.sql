-- Table: cse.amphures

-- DROP TABLE cse.amphures;

CREATE TABLE cse_v2.amphures
(
    id integer,
    code character varying(255) COLLATE pg_catalog."default",
    name_th character varying(255) COLLATE pg_catalog."default",
    name_en character varying(255) COLLATE pg_catalog."default",
    province_id integer DEFAULT 0
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE cse_v2.amphures
    OWNER to admin;

GRANT ALL ON TABLE cse_v2.amphures TO admin;

GRANT ALL ON TABLE cse_v2.amphures TO developer_group;
