-- Table: cse_v2.sys_user

-- DROP TABLE cse_v2.sys_user;

CREATE TABLE cse_v2.sys_user
(
    date_record timestamp without time zone DEFAULT now(),
    date_update timestamp without time zone,
    add_user_code character varying(20) COLLATE pg_catalog."default",
    edit_user_code character varying(20) COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('cse_v2.sys_user_id_seq'::regclass),
    fullname character varying(255) COLLATE pg_catalog."default" NOT NULL,
    faculty character varying(255) COLLATE pg_catalog."default" NOT NULL,
    username character varying(255) COLLATE pg_catalog."default" NOT NULL,
    password character varying(255) COLLATE pg_catalog."default" NOT NULL,
    user_type character(1) COLLATE pg_catalog."default" NOT NULL,
    is_status character(1) COLLATE pg_catalog."default" NOT NULL DEFAULT 1,
    last_login timestamp without time zone,
    CONSTRAINT sys_user_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE cse_v2.sys_user
    OWNER to admin;

GRANT ALL ON TABLE cse_v2.sys_user TO admin;

GRANT ALL ON TABLE cse_v2.sys_user TO dev_admin;

GRANT ALL ON TABLE cse_v2.sys_user TO developer_group;