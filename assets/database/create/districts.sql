-- Table: cse.districts

-- DROP TABLE cse.districts;

CREATE TABLE cse_v2.districts
(
    id integer,
    zip_code character varying(255) COLLATE pg_catalog."default",
    name_th character varying(255) COLLATE pg_catalog."default",
    name_en character varying(255) COLLATE pg_catalog."default",
    amphure_id integer
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE cse_v2.districts
    OWNER to admin;

GRANT ALL ON TABLE cse_v2.districts TO admin;

GRANT ALL ON TABLE cse_v2.districts TO developer_group;
