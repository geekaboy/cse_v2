-- Table: cse_v2.provinces

-- DROP TABLE cse_v2.provinces;

CREATE TABLE cse_v2.provinces
(
    id integer,
    code character varying(255) COLLATE pg_catalog."default",
    name_th character varying(255) COLLATE pg_catalog."default",
    name_en character varying(255) COLLATE pg_catalog."default",
    geography_id integer
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE cse_v2.provinces
    OWNER to admin;

GRANT ALL ON TABLE cse_v2.provinces TO admin;

GRANT ALL ON TABLE cse_v2.provinces TO developer_group;
