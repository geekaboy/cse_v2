-- Table: cse_v2.answer_general

DROP TABLE cse_v2.answer_general;

CREATE TABLE cse_v2.answer_general
(
    date_record timestamp without time zone DEFAULT now(),
    date_update timestamp without time zone,
    add_user_code character varying(20) COLLATE pg_catalog."default",
    edit_user_code character varying(20) COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('cse_v2.answer_general_id_seq'::regclass),
    std_id character varying(30) COLLATE pg_catalog."default",
    citizen_id character varying(30) COLLATE pg_catalog."default",
    org_name character varying(200) COLLATE pg_catalog."default",
    org_type_id integer,
    org_type_other character varying(255) COLLATE pg_catalog."default",
    director_type_id integer,
    director_type_other character varying(255) COLLATE pg_catalog."default",
    comment_note text COLLATE pg_catalog."default",
    work_status character varying(1) COLLATE pg_catalog."default" NOT NULL DEFAULT 1,
    from_token_code character varying(255) COLLATE pg_catalog."default",
    is_status character varying(1) COLLATE pg_catalog."default" NOT NULL DEFAULT 1,
    CONSTRAINT answer_general_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE cse_v2.answer_general
    OWNER to dev_admin;

GRANT ALL ON TABLE cse_v2.answer_general TO dev_admin WITH GRANT OPTION;

GRANT ALL ON TABLE cse_v2.answer_general TO developer_group;

COMMENT ON COLUMN cse_v2.answer_general.comment_note
    IS 'ข้อเสนอแนะเกี่ยวกับบัณฑิต';